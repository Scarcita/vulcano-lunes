import Layout from "../../components/layout";

import LineChart from "../../components/Admin/Dashboard/LineChart";
import LineChartDos from "../../components/Admin/Dashboard/barChartMarca";
import LineChartUno from "../../components/Admin/Dashboard/pieChart";
import Tabla from "../../components/Admin/Dashboard/Tabla";
import BarChartMarcas from "../../components/Admin/Dashboard/barChartMarca";
import PieChart from "../../components/Admin/Dashboard/pieChart";
import CounterEntregadas from "../../components/Admin/Dashboard/counterEntregadas";
import CounterCerradas from "../../components/Admin/Dashboard/counterCerradas";
import CounterAnuladas from "../../components/Admin/Dashboard/counterAnuladas";


export default function pantallaSeis() {
  return (
    <div className="">
      <div className="mt-[36px] ml-[20px] mr-[20px] ">
        <div className="grid grid-cols-12">
          <div className="col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between">
            <div className="text-[24px] md:text-[32px] lg:text-[32px] text-[#000000] ">
              Nueva Orden
            </div>
            <div className="flex flex-row">
              <div className="text-right">
                <span className="text-[12px] md:text-[16px] lg:text-[16px] text-[#000000]">
                  Juan Camacho
                </span>
                <h1 className="text-[12px] md:text-[16px] lg:text-[16px] text-[#ACB5BD]">
                  ASESOR DE SERVICIO
                </h1>
              </div>
              <div className="w-[52px] h-[52px] md:w-[56px] md:h-[56px] lg:w-[62px] lg:h-[62px] rounded-full bg-[#E4E7EB] ml-[5px]"></div>
            </div>
          </div>
        </div>
        <div className="grid grid-cols-12 gap-4 mt-[20px]">
          <div className="col-span-12 md:col-span-12 lg:col-span-3">
            <div className="grid grid-cols-12 gap-4">
              <div className=" col-span-12 md:col-span-4 lg:col-span-12 bg-[#3682F7] rounded-[10px] self-center text-center py-1">
                <div className="text-[#FFFFFF] text-[43px] font-light">
                  <CounterEntregadas />
                </div>
                <p className="text-[#FFFFFF] text-[12px] font-thin">
                  ORDENES ENTREGADAS
                </p>
              </div>
              <div className="col-span-12 md:col-span-4 lg:col-span-12  bg-[#3682F7] rounded-[10px] self-center text-center py-1">
                <div className="text-[#FFFFFF] text-[43px] font-light">
                  <CounterCerradas />
                </div>
                <p className="text-[#FFFFFF] text-[12px] font-thin">
                  ORDENES CERRADAS
                </p>
              </div>
              <div className="col-span-12 md:col-span-4 lg:col-span-12 bg-[#3682F7] rounded-[10px] self-center text-center py-1">
                <div className="text-[#FFFFFF] text-[43px] font-light">
                  <CounterAnuladas />
                </div>
                <p className="text-[#FFFFFF] text-[12px] font-thin">
                  ORDENES ANULADAS
                </p>
              </div>
            </div>
          </div>

          <div className="col-span-12 md:col-span-12 lg:col-span-3">
            <p className="text-[20px] text-[#000000] font-normal">
              Última OT&apos;s
            </p>

            <div className="mt-[10px]">
              <PieChart />
            </div>
          </div>

          <div className="col-span-12 md:col-span-12 lg:col-span-3">
            <p className="text-[20px] text-[#000000] font-normal">
              Marcas más ingresadas
            </p>
            <div className="mt-[10px]">
              <BarChartMarcas />
            </div>
          </div>

          <div className="col-span-12 md:col-span-12 lg:col-span-3">
            <p className="text-[20px] text-[#000000] font-normal">
              Ingresos a lo largo del tiempo
            </p>
            <div className="mt-2">
              <LineChart />
            </div>
          </div>
        </div>
        <div className="text-[20px]">
          <h1 className="text-[32px] text-[#000000] ">OT&apos;s en curso</h1>
        </div>
        <div className="grid grid-cols-12 mt-[22px]">
          <div className="col-span-12 md:col-span-12">
            <Tabla />
          </div>
        </div>
      </div>
    </div>
  );
}
pantallaSeis.auth = true;
pantallaSeis.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};
