import React, { useEffect, useState } from "react";
import Image from "next/image";



// import BarChartGlobal from './BarChart';
import Layout from "../../components/layout";
import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";

/////////////// IMAGENES//////////////
import ImgItems from "../../public/Desktop8/items.svg";
import ImgCostos from "../../public/Desktop8/costo.svg";
import ImgRepuestos from "../../public/Desktop8/repuestos.svg";
import BarChartProductGlobal from "../../components/Inventory/Dashboard/BarChart";
import ProductsCount from "../../components/Inventory/Dashboard/productsCount";
import ProductsCostSum from "../../components/Inventory/Dashboard/productsCostSum";
import ProductsInsThisMonth from "../../components/Inventory/Dashboard/productsInsThisMonth";
import ProductsOrdersOutsThisMonth from "../../components/Inventory/Dashboard/productsOrdersOutsThisMonth";
import BarChartDesks from "../../components/Inventory/Dashboard/barChartDesks ";
import DesksOutsThisMonth from "../../components/Inventory/Dashboard/desksOutsThisMonth";
import RequestedProducts from "../../components/Inventory/Dashboard/requestedProducts";
import RequestedProductsFromOtNotCompleted from "../../components/Inventory/Dashboard/requestedProductsFromOtNotCompleted";
import BarChartOt from "../../components/Inventory/Dashboard/barChartOt";
import ListLastOuts from "../../components/Inventory/Dashboard/lastOuts";
import ListProducts from "../../components/Inventory/Dashboard/listProductStock";
export default function Almacen() {
  return (
    <div className="mt-[26px] ml-[20px] mr-[20px]">
      <div className="flex flex-row">
        <h2 className="text-[24px] font-semibold">Almacen</h2>
        <h2 className="text-[24px]">/Global</h2>
      </div>
      <div className="mt-[20px] grid grid-cols-12 gap-4 ">
        <div className="col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-[#FFFF] rounded-[16px] shadow-md justify-between px-2 py-2">
          <div className="grid grid-cols-12 gap-4">
            <div className="col-span-6 md:col-span-6 lg:col-span-6">
              <div>
                <div className="text-[14px] text-[#000000] ">
                  Ingresos de productos
                </div>
                <div className="flex flex-row justify-between">
                  <div className="text-[34px] text-[#3682F7]">
                    <ProductsInsThisMonth/>
                  </div>
                  <div className=" flex flex-row h-[20px] bg-[#3682F7] rounded-[5px] items-center self-center justify-center p-2">
                    <div className="w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] items-center justify-center">
                      <p className="text-[#3682F7] text-[12px] font-semibold mt-[-6px] ml-[3px]">
                        ↓
                      </p>
                    </div>
                    <p className="text-[12px] text-[#FFFFFF] font-semibold pl-[3px]">
                      15%
                    </p>
                  </div>
                </div>
                <div className="text-[10px] text-[#A5A5A5]">Este Mes</div>
              </div>

            </div>
            <div className="col-span-6 md:col-span-6 lg:col-span-6 text-center self-center">
              <BarChartProductGlobal />
            </div>
            
          </div>
        </div>
        <div className="col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#FFFF] rounded-[16px] shadow-md justify-between px-2 py-2">
          <div className="grid grid-cols-12 gap-4">
            <div className="col-span-6 md:col-span-6 lg:col-span-6">
              <div>
                <div className="text-[14px] text-[#000000] ">
                  Salidas por OT
                </div>
                <div className="flex flex-row justify-between">
                  <div className="text-[34px] text-[#3682F7]">
                    <ProductsOrdersOutsThisMonth/>
                  </div>
                  <div className=" flex flex-row h-[20px] bg-[#3682F7] rounded-[5px] items-center self-center justify-center p-2">
                    <div className="w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] items-center justify-center">
                      <p className="text-[#3682F7] text-[12px] font-semibold mt-[-6px] ml-[3px]">
                        ↓
                      </p>
                    </div>
                    <p className="text-[12px] text-[#FFFFFF] font-semibold pl-[3px]">
                      15%
                    </p>
                  </div>
                </div>
                <div className="text-[10px] text-[#A5A5A5]">Este Mes</div>
              </div>

            </div>
            <div className="col-span-6 md:col-span-6 lg:col-span-6 text-center self-center">
              <BarChartOt/>
            </div>
            
          </div>
        </div>
        <div className="col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#FFFF] rounded-[16px] shadow-md justify-between px-2 py-2">
          <div className="grid grid-cols-12 gap-4">
            <div className="col-span-6 md:col-span-6 lg:col-span-6">
              <div>
                <div className="text-[14px] text-[#000000] ">
                  Salidas por mostrador
                </div>
                <div className="flex flex-row justify-between">
                  <div className="text-[34px] text-[#3682F7]">
                    <DesksOutsThisMonth/>
                  </div>
                  <div className=" flex flex-row h-[20px] bg-[#3682F7] rounded-[5px] items-center self-center justify-center p-2">
                    <div className="w-[16px] h-[9px] bg-[#FFFFFF] rounded-[2.5px] items-center justify-center">
                      <p className="text-[#3682F7] text-[12px] font-semibold mt-[-6px] ml-[3px]">
                        ↓
                      </p>
                    </div>
                    <p className="text-[12px] text-[#FFFFFF] font-semibold pl-[3px]">
                      15%
                    </p>
                  </div>
                </div>
                <div className="text-[10px] text-[#A5A5A5]">Este Mes</div>
              </div>

            </div>
            <div className="col-span-6 md:col-span-6 lg:col-span-6 text-center self-center">
              <BarChartDesks/>
            </div>
            
          </div>
        </div>

        <div className="col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#FFFF] rounded-[16px] shadow-md  justify-between px-2 py-2">
          <div>
            <div className="text-[14px] text-[#000000] ">#Items</div>
            <div className="text-[40px] text-[#3682F7]">
              <ProductsCount/>
            </div>
            <div className="text-[10px] text-[#A5A5A5]">Actual</div>
          </div>
          <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full pt-[10px] mt-[24px] text-center">
            <Image src={ImgItems} layout="fixed" alt="ImgItems" />
          </div>
        </div>

        <div className="col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-[#FFFF] rounded-[16px] shadow-md justify-between px-2 py-2">
          <div>
            <div className="text-[14px] text-[#000000] ">costo Total</div>
            <div className="text-[40px] text-[#3682F7]">
              <ProductsCostSum/>
            </div>
            <div className="text-[10px] text-[#A5A5A5]">Actual</div>
          </div>
          <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full pt-[10px] mt-[24px] text-center">
            <Image src={ImgCostos} layout="fixed" alt="ImgCostos" />
          </div>
        </div>

        <div className="col-span-12 md:col-span-6 lg:col-span-4 flex flex-row bg-[#FFFF] rounded-[16px] shadow-md justify-between px-2 py-2">
          <div>
            <div className="text-[14px] text-[#000000] ">
              Solicitudes de repuestos
            </div>
            <div className="">
              <RequestedProducts/>
            </div>
            <div className="text-[10px] text-[#A5A5A5]">
              Codificadas vs Solicitudes
            </div>
          </div>
          <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full pt-[10px] mt-[24px] text-center">
            <Image src={ImgRepuestos} layout="fixed" alt="ImgRepuestos" />
          </div>
        </div>
      </div>
      <div className="mt-[20px] grid grid-cols-12 gap-4 ">
        <div className="col-span-12 md:col-span-12 lg:col-span-6 ">
          <div className="flex flex-row">
            <h2 className="text-[24px] font-semibold">Ultimas salidas</h2>
            <h2 className="text-[14px] mt-[12px]">/OTs y Mostrador</h2>
          </div>

          <div className="mt-[14px]">
            <ListLastOuts/>
          </div>
        </div>

        <div className=" col-span-12 md:col-span-12 lg:col-span-6 ">
          <div className="flex flex-row">
            <h2 className="text-[24px] font-semibold">Repuestos solicitados</h2>
            <h2 className="text-[14px] mt-[12px]">
              /En espera de codificacion
            </h2>
          </div>

          <div className="mt-[14px]">
            <RequestedProductsFromOtNotCompleted/>
          </div>
        </div>
      </div>

      <div className="mt-[20px] grid grid-cols-12 g">
        <div className=" col-span-12 md:col-span-12 lg:col-span-12">
          <div className="flex flex-row">
            <h2 className="text-[24px] font-semibold">
              Productos debajo del stock minimo
            </h2>
          </div>
          <div className="mt-[14px] col-span-12 ">
            <ListProducts />
          </div>
        </div>
      </div>
    </div>
  );
}
Almacen.auth = true;
Almacen.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};
