import Image from "next/image";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import Layout from "../../components/layout";

import imagenCaja from "../../public/caja.svg";
import imagenOpciones from "../../public/opciones.svg";
import imagenIngreso from "../../public/ingreso.svg";
import imagenEgreso from "../../public/egreso.svg";
import imagen$ from "../../public/$.svg";
import CounterStockActual from "../../components/Inventory/ProductDetail/counterStockActual";
import CounterStockMin from "../../components/Inventory/ProductDetail/counterStockMin";
import Codigo from "../../components/Inventory/ProductDetail/codigo";
import CounterCostoPro from "../../components/Inventory/ProductDetail/counterCostoPro";
import CounterPrecio from "../../components/Inventory/ProductDetail/counterPrecio";
import TablaKardex from "../../components/Inventory/ProductDetail/tablaKardex";
import ModalNewEntry from "../../components/Inventory/ProductDetail/modalNewIngreso";
import TablaIngresos from "../../components/Inventory/ProductDetail/tablaIngresos";
import TablaSalidasOt from "../../components/Inventory/ProductDetail/tablasalidasOt";
import TablaMostrar from "../../components/Inventory/ProductDetail/tablaSalidasMostrar";

function pantallaAmortiguador() {
  return (
    <div className="mt-[52px] ml-[20px] mr-[20px]">
      <div className="grid grid-cols-12">
        <div className="col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between">
          <div className="text-[14px] md:text-[20px] lg:text-[24px] text-[#000000] font-semibold">
            AMORTIGUADOR DELANTERO IZQUIERDO ECOSPORT
          </div>
          {/* <button className='flex justify-center items-center w-[120px] h-[30px] hover:border-[#3682F7] hover:text-[#3682F7] rounded-lg bg-[#3682F7] shadow-md'>
            <p className='text-[#FFFF] text-[18px] font-light'> Nuevo Ingreso</p>
          </button> */}
          <div>
            <ModalNewEntry />
          </div>
        </div>
      </div>

      <div className="grid grid-cols-9 gap-2 mt-[20px]">
        <div className="col-span-6 md:col-span-2 lg:col-span-1 bg-[#3682F7] rounded-[16px] shadow-md px-2 py-2">
          <div className="text-[14px] text-[#fff] ">Stock Actual</div>
          <div className="text-[40px] text-[#fff]">
            <CounterStockActual />
          </div>
          <div className="text-[10px] text-[#fff]">Unidades/Piezas</div>
        </div>
        <div className="col-span-6 md:col-span-2 lg:col-span-1 bg-[#FFFF] rounded-[16px] shadow-md px-2 py-2">
          <div className="text-[14px] text-[#000000] ">Stock Mínimo</div>
          <div className="text-[40px] text-[#3682F7]">
            <CounterStockMin />
          </div>
          <div className="text-[10px] text-[#A5A5A5]">Unidades/Piezas</div>
        </div>

        <div className="col-span-12 md:col-span-5 lg:col-span-3 flex flex-row  bg-[#FFFF] rounded-[16px] shadow-md  justify-between px-2 py-2">
          <div>
            <div className="text-[14px] text-[#000000] ">Codigo</div>
            <div className="text-[40px] text-[#3682F7]">
              <Codigo />
            </div>
            <div className="text-[10px] text-[#A5A5A5]">Actual</div>
          </div>
          <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full self-center pt-[10px] text-center">
            <Image
              src={imagenCaja}
              layout="fixed"
              alt="media"
              width={30}
              height={30}
            />
          </div>
        </div>

        <div className="col-span-12 md:col-span-4 lg:col-span-2 flex flex-row  bg-[#FFFF] rounded-[16px] shadow-md  justify-between px-2 py-2">
          <div>
            <div className="text-[14px] text-[#000000] ">Costo Promedio</div>
            <div className="text-[40px] text-[#3682F7]">
              <CounterCostoPro />
            </div>
            <div className="text-[10px] text-[#A5A5A5]">Actual</div>
          </div>
          <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full self-center pt-[10px] text-center">
            <Image
              //className='rounded-full text-center items-center self-center pt-[20px]'
              src={imagen$}
              layout="fixed"
              alt="media"
              width={30}
              height={30}
            />
          </div>
        </div>

        <div className="col-span-12 md:col-span-5 lg:col-span-2 flex flex-row  bg-[#FFFF] rounded-[16px] shadow-md  justify-between px-2 py-2 ">
          <div>
            <div className="text-[14px] text-[#000000] ">Precio</div>
            <div className="text-[40px] text-[#3682F7]">
              <CounterPrecio />
            </div>
            <div className="text-[10px] text-[#A5A5A5]">Actual</div>
          </div>
          <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full self-center pt-[10px] text-center">
            <Image
              //className='rounded-full text-center items-center self-center pt-[20px]'
              src={imagen$}
              layout="fixed"
              alt="media"
              width={30}
              height={30}
            />
          </div>
        </div>
      </div>

      <div className="grid grid-cols-12 justify-left mt-10">
        <div className="col-span-12 md:col-span-12 lg:col-span-12">
          <Tabs>
            <TabList className="flex flex-row w-full  md:w-[300px] lg:w-[320px] bg-[#E4E7EB] focus:outline-none rounded-t-lg h-8">
              <Tab className="flex justify-center items-center text-[12px] focus:text-[#fff] focus:bg-[#3682F7] rounded-tl-lg bg-[#E4E7EB] px-2">
                Kardex
              </Tab>
              <Tab className="text-gray-500 text-[12px] flex justify-center items-center focus:bg-[#3682F7] focus:text-[#fff]  px-2 py-2">
                Ingresos
              </Tab>
              <Tab className="text-gray-500 text-[12px] flex justify-center items-center focus:bg-[#3682F7] focus:text-[#fff]  px-2 py-2">
                Salidas por OT
              </Tab>
              <Tab className="text-gray-500 text-[12px] flex justify-center items-center focus:bg-[#3682F7] focus:text-[#fff]  px-2 py-2">
                Salidas por Mostrador
              </Tab>
            </TabList>

            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  <TablaKardex />
                </div>
              </div>
            </TabPanel>

            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  <TablaIngresos />
                </div>
              </div>
            </TabPanel>

            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  <TablaSalidasOt />
                </div>
              </div>
            </TabPanel>
            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  <TablaMostrar />
                </div>
              </div>
            </TabPanel>
          </Tabs>
        </div>
      </div>
    </div>
  );
}
pantallaAmortiguador.auth = true;
pantallaAmortiguador.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

export default pantallaAmortiguador;
