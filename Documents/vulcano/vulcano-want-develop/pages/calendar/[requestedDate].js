import React, { useEffect, useState } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";

import Layout from "../../components/layout";
import Calendario from "../../components/Appointments/Calendar/Calendario";
// import TableCitasConfir from "../../components/Appointments/Calendar/tableConfirmados";
// import TableCitasNext from "../../components/Appointments/Calendar/tableCitasNext";
// import TableCitasAll from "../../components/Appointments/Calendar/tableCitasAll";
// import TableCitasCancel from "../../components/Appointments/Calendar/tableCitasCancel";
// import CounterConfirmadas from "../../components/Appointments/Calendar/counterConfirmadas";
// import CounterReservas from "../../components/Appointments/Calendar/counterReservas";
// import CounterCancel from "../../components/Appointments/Calendar/counterCancel";
import { useRouter } from "next/router";
export default function RequestedDate() {
  const router = useRouter();
  const date = router.query.requestedDate;

  return (
    <div className="overflow-y-auto h-screen">
      <div className="mt-[20px] ml-[20px] mr-[20px]">
        <div className="grid grid-cols-12">
          <div className="col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between">
            <div className="flex flex-row">
              <h2 className="text-[24px] font-semibold">Calendario</h2>
              <h2 className="text-[14px] pt-[10px]">/Citas Hoy</h2>
            </div>
            <div>
              <button className=" w-[101px] h-[30px] bg-[#3682F7] border-[#3682F7] border rounded-[30px] text-[17px] text-[#fff] text-center text-[18px] hover:bg-[#F6F6FA] hover:text-[#3682F7] ">
                NUEVA
              </button>
            </div>
          </div>
        </div>
        <div className="grid grid-cols-12 gap-6">
          <div className="col-span-12 md:col-span-12 lg:col-span-12 mt-[40px] items-center">
            <Calendario date={date} />
          </div>
        </div>
      </div>
    </div>
  );
}
RequestedDate.auth = true;
RequestedDate.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};