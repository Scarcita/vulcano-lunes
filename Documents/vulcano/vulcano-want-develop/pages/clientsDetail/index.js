import React, { useEffect, useState } from "react";
import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";
import ModalInf from "../../components/clientsDetail/modalInfo";
import Image from "next/image";

import Layout from "../../components/layout";
import TabsClientsDetails from "../../components/clientsDetail/tabs/tabs";
import DatosClients from "../../components/clientsDetail/datosClients";
import AccountantsAll from "../../components/clientsDetail/accountantsAll";


function ClientsDetail() {

  const [reloadUsers, setReloadUsers] = useState(false);

  return (
    <div className="mt-[52px] ml-[20px] mr-[20px] ">
      <div className="grid grid-cols-12">
        <div className="col-span-12 md:col-span-12 lg:col-span-12">
            <div className="grid grid-cols-12">
                <div className="col-span-10 md:col-span-10 lg:col-span-10">
                    <DatosClients/>
                </div>
                <div className="col-span-2 md:col-span-2 lg:col-span-2 text-end">
                    <ModalInf/>
                </div>
            </div>
            <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12">
                    <AccountantsAll/>
                </div>
            </div>
            <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12">
                    <TabsClientsDetails/>
                </div>
            </div>
        </div>
      </div>
    </div>
  );
}
ClientsDetail.auth = true;
ClientsDetail.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

export default ClientsDetail;
