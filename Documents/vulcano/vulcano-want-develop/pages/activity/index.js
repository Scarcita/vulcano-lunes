import React from "react";
import Layout from "../../components/layout";
import Image from "next/image";
import imagenCalendario from "../../public/imagenCalendario.svg";
import PieChart from "../../components/activity/pieChart";
import TableActivityClients from "../../components/activity/tableActivityClients";
import TableVisitasPlanificadas from "../../components/activity/tableVisitasPlanificadas";
import CountThisMonth from "../../components/activity/activitiesCreatedThisMonth";
import CountLastMonth from "../../components/activity/activitiesCreatedLastMonth";


export default function Activity() {

    return(
        <div className="mt-[52px] ml-[20px] mr-[20px] ">
            <div className="grid grid-cols-12 gap-5">
                <div className="col-span-12 md:col-span-12 lg:col-span-8">
                    <div className="grid grid-cols-12 gap-6">
                        <div  className="col-span-12 md:col-span-12 lg:col-span-6">
                            <p className="text-[#000] text-[20px] font-semibold">
                                Actividades 
                                <span className="text-[#000] text-[20px] font-normal">/ Este mes</span>
                            </p>
                            <div className="w-full flex flex-row bg-[#FFFF] rounded-[16px] shadow-md  justify-between py-2 px-2 mt-[10px]">
                                <div>
                                    <div className="text-[14px] font-semibold text-[#000000] ">
                                        Actividades realizadas
                                    </div>
                                    <div className="text-[40px] font-semibold text-[#3682F7]">
                                        <CountThisMonth/>
                                    </div>
                                    <div className="text-[10px] text-[#A5A5A5]">Este mes</div>
                                </div>
                                <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full self-center pt-[10px] text-center">
                                    <Image
                                        src={imagenCalendario}
                                        layout="fixed"
                                        alt="media"
                                        width={30}
                                        height={30}
                                    />
                                </div>
                            </div>
                        </div>
                        <div  className="col-span-12 md:col-span-12 lg:col-span-6">
                            <p className="text-[#000] text-[20px] font-semibold">
                                Actividades 
                                <span className="text-[#000] text-[20px] font-normal">/Mes Anterior</span>
                            </p>
                            <div className="w-full flex flex-row bg-[#FFFF] rounded-[16px] shadow-md  justify-between py-2 px-2 mt-[10px]">
                                <div>
                                    <div className="text-[14px] font-semibold text-[#000000] ">
                                        Actividades realizadas
                                    </div>
                                    <div className="text-[40px] font-semibold text-[#3682F7]">
                                        <CountLastMonth/>
                                    </div>
                                    <div className="text-[10px] text-[#A5A5A5]">Mes Anterior</div>
                                </div>
                                <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full self-center pt-[10px] text-center">
                                    <Image
                                        src={imagenCalendario}
                                        layout="fixed"
                                        alt="media"
                                        width={30}
                                        height={30}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="col-span-12 md:col-span-12 lg:col-span-6">
                             <PieChart/>
                        </div>
                        <div className="col-span-12 md:col-span-12 lg:col-span-6 bg-[#FFFF] shadow-md h-[210px] rounded-2xl justify-center items-center text-[#71AD46]">
                            {/* <GaugeChartOrders/> */}
                            <p className=" text-[#000000] text-center self-end">
                            Mes actual vs anterior
                            </p>
                        </div>
                    </div>
                </div>
                <div  className="col-span-12 md:col-span-12 lg:col-span-4 ">
                    <p className="text-[#000] text-[20px] font-semibold">
                        Actividades 
                        <span className="text-[#000] text-[20px] font-normal">/ Visitas planificadas</span>
                    </p>
                    <div className="mt-[10px]">
                    <TableVisitasPlanificadas/>
                    </div>
                </div>

            </div>
            <div className="grid grid-cols-12 mt-[30px]">
                <div className=" md:col-span-4 lg:col-span-4">
                    <p className="text-[#000] text-[30px] font-semibold">
                        Clientes
                    </p>
                </div>
                <div className="col-span-12 md:col-span-8 lg:col-span-8 h-[40px] flex flex-row">
                    <input className="w-full h-full rounded-lg bg-[#fff] border pl-[10px] pr-[10px] " />
                    <div className="flex justify-center items-center">
                        <div className="flex justify-center items-center w-[40px] h-[40px] hover:bg-[#3682F7] flex flex-col rounded-lg border-[#3682F7] border-[1px] ml-[10px] ">
                            <button>
                                <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={1.5}
                                stroke="currentColor"
                                className="w-6 h-6  text-[#3682F7]  hover:text-[#fff]"
                                >
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                                />
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="grid grid-cols-12 mt-[30px]">
                <div className="col-span-12 md:col-span-12 lg:col-span-12">
                    <TableActivityClients/>
                </div>
            </div>

            
        </div>
    )
}

//Activity.auth = true;
Activity.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};