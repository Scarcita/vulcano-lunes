// import GaugeChart from "react-gauge-chart";

import Image from "next/image";

import "react-activity/dist/Spinner.css";
import Layout from "../../components/layout";
// import DatoNumero from "../../components/Orders/Dashboard/DatoNumero";
import imagenCalendario from "../../public/imagenCalendario.svg";
import ImgSmile from "../../public/citas/smile.svg";
import ImgSad from "../../public/citas/sad.svg";
import tablero from "../../public/tablero.svg";
import CitasDos from "../../components/Orders/Dashboard/appointmentsToday";
import BarChartAnual from "../../components/Orders/Dashboard/barChartOrdersCreatedYear";
import BarChartWeek from "../../components/Orders/Dashboard/BarChartWeek";

//////////////////////// Contadores ////////////////////////////
// ESTE MES
import CounterEsteMesRealizadas from "../../components/Orders/Dashboard/counterEsteMesRealizadas";
import CounterEsteMesConfirmadas from "../../components/Orders/Dashboard/counterEsteMesConfirmadas";
import CounterEsteMesCanceladas from "../../components/Orders/Dashboard/counterEsteMesCanceladas";

// MES ANTERIOR
import CounterMesAnteriorRealizadas from "../../components/Orders/Dashboard/counterMesAnteriorRealizadas";
import CounterMesAnteriorConfirmadas from "../../components/Orders/Dashboard/counterMesAnteriorConfirmadas";
import CounterMesAnteriorCanceladas from "../../components/Orders/Dashboard/counterMesAnteriorCanceladas";

// MES ACTUAL vs MES ANTERIOR
import CounterOrdersThisMonth from "../../components/Orders/Dashboard/counterOrdersThisMonth";
import CounterOrdersLastMonth from "../../components/Orders/Dashboard/counterOrdersLastMonth";

// ORDENES DE TRABAJO
import CounterOrdenesAñoReservas from "../../components/Orders/Dashboard/counterOrdernesAñoReservas";
import CounterOrdenesAñoConfirmadas from "../../components/Orders/Dashboard/counterOrdernesAñoConfirmadas";
import BarChartCitasWeek from "../../components/Orders/Dashboard/BarChart";
import BarChartOrdersCreatedYear from "../../components/Orders/Dashboard/barChartOrdersCreatedYear";
import AppointmentsToday from "../../components/Orders/Dashboard/appointmentsToday";
// import GaugeChartOrders from "../../components/Orders/Dashboard/gaugeChartOrders";




function pantallaCitasDos() {
  return (
    <div className="grid grid-cols-12 mt-[20px] ml-[20px] mr-[20px]">
      <div className="col-span-12 md:col-span-12 lg:col-span-8">
        <div className=" pt-8 grid grid-cols-12 gap-6">
          <div className="col-span-12 md:col-span-6 lg:col-span-6">
            <div className="flex flex-row">
              <h2 className="text-[24px] font-semibold">Citas</h2>
              <h2 className="text-[14px] pt-[10px]">/Este mes</h2>
            </div>
            <div className="pt-[20px] grid grid-cols-12 gap-3">
              <div className="col-span-12 md:col-span-12 lg:col-span-12 flex flex-row bg-[#FFFF] rounded-[16px] shadow-md  justify-between py-2 px-2">
                <div>
                  <div className="text-[14px] font-semibold text-[#000000] ">
                    #Reservas realizadas
                  </div>
                  <div className="text-[40px] font-semibold text-[#3682F7]">
                    <CounterEsteMesRealizadas />
                  </div>
                  <div className="text-[10px] text-[#A5A5A5]">Este mes</div>
                </div>
                <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full pt-[10px] self-center text-center">
                  <Image
                    src={imagenCalendario}
                    layout="fixed"
                    alt="media"
                    width={30}
                    height={30}
                  />
                </div>
              </div>

              <div className="col-span-6 md:col-span-6 lg:col-span-6  bg-[#FFFF] rounded-[16px] shadow-md py-2 px-2">
                <div className="w-[30px] h-[30px] bg-[#F6F6FA] rounded-full ">
                  <Image
                    src={ImgSmile}
                    layout="responsive"
                    alt="media"
                    width={30}
                    height={30}
                  />
                </div>
                <p className="text-[14px] text-[#000000] font-semibold">
                  Confirmadas
                </p>
                <div className="text-[40px] font-semibold text-[#3682F7]">
                  <CounterEsteMesConfirmadas />
                </div>
                <p className="text-[10px] text-[#A5A5A5]">Este mes</p>
              </div>

              <div className="col-span-6 md:col-span-6 lg:col-span-6  bg-[#FFFF] rounded-[16px] shadow-md py-2 px-2">
                <div className="w-[30px] h-[30px] bg-[#F6F6FA] rounded-full ">
                  <Image
                    src={ImgSad}
                    layout="responsive"
                    alt="media"
                    width={30}
                    height={30}
                  />
                </div>
                <p className="text-[14px] text-[#000000] font-semibold">
                  Canceladas
                </p>
                <div className="text-[40px] font-semibold text-[#3682F7]">
                  <CounterEsteMesCanceladas />
                </div>
                <p className="text-[10px] text-[#A5A5A5]">Este mes</p>
              </div>
            </div>
          </div>

          <div className="col-span-12 md:col-span-6 lg:col-span-6">
            <div className="flex flex-row">
              <h2 className="text-[24px] font-semibold">Citas</h2>
              <h2 className="text-[14px] pt-[10px]">/Mes anterior</h2>
            </div>
            <div className=" pt-[20px] grid grid-cols-12 gap-3">
              <div className="col-span-12 md:col-span-12 lg:col-span-12 flex flex-row bg-[#FFFF] rounded-[16px] shadow-md  justify-between py-2 px-2">
                <div>
                  <div className="text-[14px] font-semibold text-[#000000] ">
                    #Reservas realizadas
                  </div>
                  <div className="text-[40px] font-semibold text-[#3682F7]">
                    <CounterMesAnteriorRealizadas />
                  </div>
                  <div className="text-[10px] text-[#A5A5A5]">Mes Anterior</div>
                </div>
                <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full pt-[10px] self-center text-center">
                  <Image
                    src={imagenCalendario}
                    layout="fixed"
                    alt="media"
                    width={30}
                    height={30}
                  />
                </div>
              </div>

              <div className="col-span-6 md:col-span-6 lg:col-span-6  bg-[#FFFF] rounded-[16px] shadow-md pl-[15px] pr-[15px]   pt-[10px] pb-[10px]">
                <div className="w-[30px] h-[30px] bg-[#F6F6FA] rounded-full ">
                  <Image
                    src={ImgSmile}
                    layout="responsive"
                    alt="media"
                    width={30}
                    height={30}
                  />
                </div>
                <p className="text-[14px] text-[#000000] font-semibold">
                  Confirmadas
                </p>
                <div className="text-[40px] font-semibold text-[#3682F7]">
                  <CounterMesAnteriorConfirmadas />
                </div>
                <p className="text-[10px] text-[#A5A5A5]">Mes anterior</p>
              </div>

              <div className="col-span-6 md:col-span-6 lg:col-span-6  bg-[#FFFF] rounded-[16px] shadow-md pl-[15px] pr-[15px]  pt-[10px] pb-[10px]">
                <div className="w-[30px] h-[30px] bg-[#F6F6FA] rounded-full ">
                  <Image
                    src={ImgSad}
                    layout="responsive"
                    alt="media"
                    width={30}
                    height={30}
                  />
                </div>
                <p className="text-[14px] text-[#000000] font-semibold">
                  Canceladas
                </p>
                <div className="text-[40px] font-semibold text-[#3682F7]">
                  <CounterMesAnteriorCanceladas />
                </div>
                <p className="text-[10px] text-[#A5A5A5]">Este mes</p>
              </div>
            </div>
          </div>
        </div>

        <div className="flex mt-[20px]">
          <h2 className="text-[24px] font-semibold">Órdenes de Trabajo</h2>
          <h2 className="text-[14px] pt-[10px]">/Mes actual vs anterior</h2>
        </div>
        <div className=" grid grid-cols-12 gap-6 mt-[20px]">
          <div className="col-span-12 md:col-span-12 lg:col-span-6">
            <div className=" grid grid-cols-12 gap-6 ">
              <div className="col-span-12 md:col-span-6 lg:col-span-12 flex flex-row bg-[#FFFF] rounded-[16px] shadow-md justify-between py-2 px-2">
                <div>
                  <div className="text-[40px] font-semibold text-[#3682F7]">
                    <CounterOrdersThisMonth/>
                  </div>
                  <div className="text-[10px] text-[#A5A5A5]">Mes actual</div>
                </div>
                <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full pt-[10px] self-center text-center">
                  <Image
                    src={imagenCalendario}
                    layout="fixed"
                    alt="media"
                    width={30}
                    height={30}
                  />
                </div>
              </div>

              <div className="col-span-12 md:col-span-6 lg:col-span-12 flex flex-row bg-[#FFFF] rounded-[16px] shadow-md justify-between py-2 px-2">
                <div>
                  <div className="text-[40px] font-semibold text-[#3682F7]">
                    <CounterOrdersLastMonth/>
                  </div>
                  <div className="text-[10px] text-[#A5A5A5]">Mes Anterior</div>
                </div>
                <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full pt-[10px] self-center text-center">
                  <Image
                    src={imagenCalendario}
                    layout="fixed"
                    alt="media"
                    width={30}
                    height={30}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="col-span-12 md:col-span-12 lg:col-span-6 bg-[#FFFF] shadow-md h-[210px] rounded-2xl justify-center items-center text-[#71AD46]">
            {/* <GaugeChartOrders/> */}
            <p className=" text-[#000000] text-center self-end">
              Mes actual vs anterior
            </p>
          </div>
        </div>
        <div className="flex lg:mt-[40px] ">
          <h2 className="text-[24px] font-semibold">Órdenes de Trabajo</h2>
          <h2 className="text-[14px] pt-[10px]">/Año</h2>
        </div>

        <div className="pt-[20px] grid grid-cols-12 gap-4">
          <div className="col-span-12 md:col-span-12 lg:col-span-4">
            <div className=" grid grid-cols-12 gap-4 ">
              <div className="col-span-6 md:col-span-6 lg:col-span-12  bg-[#fff] rounded-[15px] shadow-md flex flex-col justify-center items-center px-2 py-2">
                <div className="bg-[#3682F7] shadow-md w-[50px] h-[25px] rounded mt-2 flex justify-center">
                  <div className="bg-[#FFFF] w-[18px] h-[12px] rounded flex justify-center mt-2 ">
                    <p className="text-[#3682F7] text-[12px] font-semibold mt-[-4px]">
                      ↑
                    </p>
                  </div>
                  <p className="text-[14px] text-[#FFFF] mt-[3px] ml-[4px]">
                    15%
                  </p>
                </div>
                <div className="flex flex-col justify-center items-center">
                  <p className="text-[#000000] text-[40px] font-medium ">
                    <CounterOrdenesAñoReservas />
                  </p>
                  <p className="text-[#000000] text-[15px] font-light">
                    Reservas
                  </p>
                </div>
              </div>
              <div className="col-span-6 md:col-span-6 lg:col-span-12 bg-[#fff] shadow-md rounded-[15px] justify-center flex flex-col items-center px-2 py-2">
                <div className="bg-[#EE002B] shadow-md w-[50px] h-[25px] rounded mt-2 flex justify-center">
                  <div className="bg-[#FFFF] w-[18px] h-[12px] rounded flex justify-center mt-2">
                    <p className="text-[#EE002B] text-[12px] font-semibold mt-[-4px]">
                      ↓
                    </p>
                  </div>
                  <p className="text-[14px] text-[#FFFF] mt-[3px] ml-[4px]">
                    15%
                  </p>
                </div>
                <div className="flex flex-col justify-center items-center">
                  <p className="text-[#000000] text-[40px] font-medium ">
                    <CounterOrdenesAñoConfirmadas />
                  </p>
                  <p className="text-[#000000] text-[15px] font-light ">
                    Confirmadas
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div className="col-span-12 md:col-span-12 lg:col-span-8 mt-[20px] md:mt-[20px] lg:mt-[0px]">
            <BarChartOrdersCreatedYear/>
          </div>
        </div>
      </div>
      <div className="col-span-12 md:col-span-12 lg:col-span-4 ml-[20px] mt-[20px]">
        <div className="flex">
          <h2 className="text-[24px] font-semibold">Citas</h2>
          <h2 className="text-[14px] pt-[10px]">/Hoy</h2>
        </div>
        <div className=" grid grid-cols-12">
          <div className="col-span-12 md:col-span-12 lg:col-span-12 mt-[30px]">
            <AppointmentsToday/>
          </div>
        </div>
        <div className="flex mt-[30px]">
          <h2 className="text-[24px] font-semibold">Citas</h2>
          <h2 className="text-[14px] pt-[10px]">/Esta semana</h2>
        </div>

        <div className=" grid grid-cols-12 mt-[30px]">
          <div className="col-span-12 md:col-span-12 lg:col-span-12">
            <BarChartCitasWeek />
          </div>
        </div>
      </div>
    </div>
  );
}
pantallaCitasDos.auth = true;
pantallaCitasDos.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

export default pantallaCitasDos;
