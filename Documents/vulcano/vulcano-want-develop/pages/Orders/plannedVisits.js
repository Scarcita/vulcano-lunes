import React from "react";
import Layout from "../../components/layout";
import Image from "next/image";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import TableUpcomming from "../../components/Orders/plannedVisits/upComming/tableUpcoming";
import TablePast from "../../components/Orders/plannedVisits/past/tablePast";
import TableAll from "../../components/Orders/plannedVisits/all/tableAll";



export default function PlannedVisits() {

    return(
        <div className="mt-[52px] ml-[20px] mr-[20px] ">
            <div className="grid grid-cols-12">
                <div  className="col-span-12 md:col-span-12 lg:col-span-12">
                    <p className="text-[#000] text-[20px] font-semibold">
                         Órdenes 
                        <span className="text-[#000] text-[20px] font-normal">/ Visitas planificadas</span>
                    </p>
                </div>
            </div>
            <Tabs>
                <TabList className="grid grid-cols-12 focus:outline-none focus:text-[#3682F7] mt-[30px]">
                    <Tab className="col-span-4 md:col-span-2 lg:col-span-2 self-center text-[12px] focus:text-[#3682F7] rounded-tl-lg px-2 py-2">
                        Próximas
                    </Tab>
                    <Tab className="col-span-4 md:col-span-2 lg:col-span-2 text-gray-500 text-[14px] self-center text-center focus:text-[#3682F7]  px-2 py-2">
                        Pasadas
                    </Tab>
                    <Tab className="col-span-4 md:col-span-2 lg:col-span-2 text-gray-500 text-[14px] self-center text-center focus:text-[#3682F7]  px-2 py-2">
                        Todas
                    </Tab>
                </TabList>
                <TabPanel>
                    <div className="grid grid-cols-12">
                        <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                            <TableUpcomming/>
                        </div>
                    </div>
                </TabPanel>
                <TabPanel>
                    <div className="grid grid-cols-12">
                        <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                            <TablePast/>
                        </div>
                    </div>
                </TabPanel>
                <TabPanel>
                    <div className="grid grid-cols-12">
                        <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                            <TableAll/>
                        </div>
                    </div>
                </TabPanel>
            </Tabs>
        </div>
    )
}

//PlannedVisits.auth = true;
PlannedVisits.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};