import Image from "next/image";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import Layout from "../../components/layout";

// import imagenIndicadorDrecha from "../../../public/derecha.svg";
import imagenIndicadorDrecha from "../../public/derecha.svg";

import BarraAzul from "../../components/Orders/Detail/BarraAzul";
import DatosDelAuto from "../../components/Orders/Detail/DatosDelAuto";
import DatosClients from "../../components/Orders/Detail/datosClients";
import TabsServicesRequiare from "../../components/Orders/Detail/tabsServiciosRequeridos";
import TabsSolicitudes from "../../components/Orders/Detail/tabsSolicitudes";

function PantallaCinco() {
  return (
    <div className="mt-[52px] ml-[20px] mr-[20px] ">
      <div className="grid grid-cols-12 gap-4">
        <div className="col-span-12 md:col-span-12 lg:col-span-9">
          <div className="grid grid-cols-12">
            <div className="col-span-12 md:col-span-12 lg:col-span-12">
              <div className="flex self-center items-center ">
                <div className="items-center">
                  <Image
                    src={imagenIndicadorDrecha}
                    layout="fixed"
                    alt="imagenIndicadorDrecha"
                  />
                </div>
                <div className="items-center">
                  <p className="text-[24px] ml-[5px]">Orden # 44427</p>
                </div>

              </div>
            </div>
            <div className="col-span-12 md:col-span-12 lg:col-span-12 mt-[25px]">
              <BarraAzul/>
            </div>
            <div className="col-span-12 md:col-span-12 lg:col-span-12 mt-[25px]">
              <DatosClients/>
            </div>
            <div className="col-span-12 md:col-span-12 lg:col-span-12 mt-[25px]">
              <TabsServicesRequiare/>
            </div>
            <div className="col-span-12 md:col-span-12 lg:col-span-12 mt-[25px]">
              <TabsSolicitudes/>
            </div>
          </div>
        </div>
        <div className="col-span-12 md:col-span-12 lg:col-span-3">
          <DatosDelAuto />
        </div>
      </div>
    </div>
  );
}

PantallaCinco.auth = true;
PantallaCinco.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};
export default PantallaCinco;
