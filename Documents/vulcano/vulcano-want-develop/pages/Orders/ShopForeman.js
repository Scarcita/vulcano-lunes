import React, { useEffect, useState } from "react";


// import GaugeCharts from '../../components/Orders/ShopForeman/gaugeChart';
import Image from "next/image";
import ListOTs from "../../components/Orders/ShopForeman/list.OTs";
// import BarChart from './bar';
import BarChartOrdersCreatedFullYear from "../../components/Orders/ShopForeman/barChatOrdersCreatedFullYear";
import Layout from "../../components/layout";
import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";

///////////// IMAGENES //////////
import ImgCheck from "../../public/Desktop5/check.svg";
import ImgCheck1 from "../../public/Desktop5/check1.svg";
import Imgup from "../../public/Desktop5/up.svg";
import ImgStop from "../../public/Desktop5/stop.svg";
import ImgPlay from "../../public/Desktop5/play.svg";
import ImgTiket from "../../public/Desktop5/tiket.svg";
import ImgTwoTiket from "../../public/Desktop5/twoTiket.svg";
import ImgBandera from "../../public/Desktop5/bandera.svg";
import ImgStop1 from "../../public/Desktop5/stop1.svg";
import ImgPlay1 from "../../public/Desktop5/play1.svg";
import ImgTiket1 from "../../public/Desktop5/tiket1.svg";
import ImgTwoTiket1 from "../../public/Desktop5/twoTiket1.svg";
import ImgBandera1 from "../../public/Desktop5/bandera1.svg";
import ImgTable from "../../public/Desktop5/tablero.svg";

// CONTADORES GOBAL
import OrdersOpenCount from "../../components/Orders/ShopForeman/contadores/contadores global/ordersOpenCount";
import OrdersInProgressCount from "../../components/Orders/ShopForeman/contadores/contadores global/ordersInProgressCount";
import OrdersPausedCount from "../../components/Orders/ShopForeman/contadores/contadores global/ordersPausedCount";
import OrdersFinalizedCount from "../../components/Orders/ShopForeman/contadores/contadores global/ordersFinalizedCount";
import OrdersClosedCount from "../../components/Orders/ShopForeman/contadores/contadores global/ordersClosedCount";
import OrdersDeliveredCount from "../../components/Orders/ShopForeman/contadores/contadores global/ordersDeliveredCount";


// CONTADORES HOY
import OrdersCreatedToday from "../../components/Orders/ShopForeman/contadores/contadores hoy/ordersCreatedToday";
import OrdersOpenToday from "../../components/Orders/ShopForeman/contadores/contadores hoy/ordersOpenToday";
import OrdersInProgressToday from "../../components/Orders/ShopForeman/contadores/contadores hoy/ordersInProgressToday";
import OrdersPausedToday from "../../components/Orders/ShopForeman/contadores/contadores hoy/ordersPausedToday";
import OrdersFinishedToday from "../../components/Orders/ShopForeman/contadores/contadores hoy/ordersFinishedToday";
import OrdersClosedToday from "../../components/Orders/ShopForeman/contadores/contadores hoy/ordersClosedToday";
import OrdersDeliveredToday from "../../components/Orders/ShopForeman/contadores/contadores hoy/ordersDeliveredToday";

// CONTADOR MES ACTUAL Vs MES ANTERIOR
import OrdersCreatedThisMonth from "../../components/Orders/ShopForeman/contadores/contadores Vs/ordersCreatedThisMonth";
import OrdersCreatedLastMonth from "../../components/Orders/ShopForeman/contadores/contadores Vs/ordersCreatedLastMonth";


// CONTADOR AÑO
import ContadorCitas from "../../components/Orders/ShopForeman/contadores/contadores año/contadorCitas";
import ContadorCreada from "../../components/Orders/ShopForeman/contadores/contadores año/contadorCreada";
// import GaugeChartOrdersMonth from "../../components/Orders/ShopForeman/gaugeChart";




function WorkOrder() {
  const [showDots, setShowDots] = useState(true);
  const [data, setData] = useState([]);
  useEffect(() => {
    fetch("https://slogan.com.bo/vulcano/orders/total/abierto")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          console.log(data.data);
          setData(data.data);
        } else {
          console.error(data.error);
        }
      })
      .then(setShowDots(false));
  }, []);

  return showDots ? (
    <div className="flex justify-center items-center  mt-[40px]">
      <Spinner
        color="#3682F7"
        size={17}
        speed={1}
        animating={true}
        style={{ marginLeft: "auto", marginRight: "auto" }}
      />
    </div>
  ) : (
    <div>
      <div className="grid grid-cols-12 gap-4 ">
        <div className="col-span-12 md:col-span-12 lg:col-span-8">
          <div className=" mt-[26px] ml-[20px] mr-[20px]">
            <div className="flex flex-row justify-between">
              <div className="flex flex-row">
                <h2 className="text-[24px] font-semibold">
                  Órdenes de trabajo
                </h2>
                <h2 className="text-[24px]">/Global</h2>
              </div>
              <button className="flex justify-center items-center w-[68px] h-[20px]  border-[1px] border-[#3682F7] rounded-[20px] text-[#3682F7] hover:bg-[#3682F7] hover:text-[#FFFF] text-[15px] mt-[10px]">
                Nueva
              </button>
            </div>
            <div className=" grid grid-cols-12 gap-3 mt-[14px]">
              <div className=" col-span-6 md:col-span-6 lg:col-span-4  h-[124px] bg-[#FFFFFF] rounded-[19px] shadow-md ">
                <div className="flex flex-row pt-[20px] justify-between pl-[20px] pr-[20px]">
                  <span className="text-[15px] text-[15px]">Abirtas</span>
                  <Image src={ImgCheck} layout="fixed" alt="ImgCheck" />
                </div>
                <h1 className="text-[38px] md:text-[42px] lg:text-[45px] text-center">
                  <OrdersOpenCount/>
                </h1>
              </div>

              <div className="col-span-6 md:col-span-6 lg:col-span-4  h-[124px] bg-[#FFFFFF] rounded-[19px] shadow-md  ">
                <div className="flex flex-row pt-[20px] justify-between pl-[20px] pr-[20px] ">
                  <span className="text-[15px] text-[15px] ">En curso</span>
                  <Image src={ImgPlay} layout="fixed" alt="ImgPlay" />
                </div>
                <h1 className="text-[38px] md:text-[42px] lg:text-[45px] text-center">
                  <OrdersInProgressCount/>
                </h1>
              </div>
              <div className="col-span-6 md:col-span-6 lg:col-span-4 h-[124px] bg-[#FFFFFF] rounded-[19px] shadow-md ">
                <div className="flex flex-row pt-[20px] justify-between pl-[20px] pr-[20px]">
                  <span className="text-[15px] text-[15px]  ">En pausa</span>
                  <Image src={ImgStop} layout="fixed" alt="ImgStop" />
                </div>
                <h1 className="text-[38px] md:text-[42px] lg:text-[45px] text-center">
                  <OrdersPausedCount/>
                </h1>
              </div>
              <div className="col-span-6 md:col-span-6 lg:col-span-4  h-[124px] bg-[#FFFFFF] rounded-[19px] shadow-md ">
                <div className="flex flex-row pt-[20px] justify-between pl-[20px] pr-[20px] ">
                  <span className="text-[15px] text-[15px]">Finalizadas</span>
                  <Image src={ImgTiket} layout="fixed" alt="ImgTiket" />
                </div>
                <h1 className="text-[38px] md:text-[42px] lg:text-[45px] text-center">
                  <OrdersFinalizedCount/>
                </h1>
              </div>
              <div className="col-span-6 md:col-span-6 lg:col-span-4 h-[124px] bg-[#FFFFFF] rounded-[19px] shadow-md ">
                <div className="flex flex-row pt-[20px] justify-between pl-[20px] pr-[20px]">
                  <span className="text-[15px] text-[15px] ">Cerradas</span>
                  <Image src={ImgTwoTiket} layout="fixed" alt="ImgTwoTiket" />
                </div>
                <h1 className="text-[38px] md:text-[42px] lg:text-[45px] text-center">
                  <OrdersClosedCount/>
                </h1>
              </div>

              <div className="col-span-6 md:col-span-6 lg:col-span-4 h-[124px] bg-[#FFFFFF] rounded-[19px] shadow-md">
                <div className="flex flex-row pt-[20px] justify-between pl-[20px] pr-[20px]">
                  <span className="text-[15px] text-[15px]  ">Entregadas</span>
                  <Image src={ImgBandera} layout="fixed" alt="ImgBandera" />
                </div>
                <h1 className="text-[38px] md:text-[42px] lg:text-[45px] text-center">
                  <OrdersDeliveredCount/>
                </h1>
              </div>
            </div>

            <div className="flex flex-row mt-[40px] ">
              <h2 className="text-[24px] font-semibold">Órdenes de trabajo</h2>
              <h2 className="text-[24px]">/Hoy</h2>
            </div>

            <div className="grid grid-cols-12 gap-3">
              <div className="col-span-12 md:col-span-12 lg:col-span-3 ">
                <div className="text-center justify-center items-center self-center mt-[30px] md: mt-[20px] lg:mt-[60px]">
                  <div className="text-[24px] ">Ingresadas</div>
                  <div className="text-[72px] font-semibold lg:text-[52px] self-center">
                    <OrdersCreatedToday/>
                  </div>
                </div>
              </div>

              <div className="col-span-12 md:col-span-12 lg:col-span-9">
                <div className="grid grid-cols-12 gap-3 mt-[20px] ">
                  <div className="col-span-4 md:col-span-4 lg:col-span-4 h-[85px] bg-[#FFFFFF] rounded-[19px] shadow-md text-center">
                    <div className="flex flex-row pt-[13px] justify-between pl-[20px] pr-[20px] ">
                      <span className="text-[12px] ">Abirtas</span>
                      <Image src={ImgCheck1} layout="fixed" alt="ImgCheck1" />
                    </div>
                    <h1 className="flex justify-center items-center text-[30px] ">
                      <OrdersOpenToday/>
                    </h1>
                  </div>

                  <div className="col-span-4 md:col-span-4 lg:col-span-4 h-[85px] bg-[#FFFFFF] rounded-[19px] shadow-md text-center">
                    <div className="flex flex-row pt-[13px] justify-between pl-[20px] pr-[20px] ">
                      <span className="text-[12px]">En curso</span>
                      <Image src={ImgPlay1} layout="fixed" alt="ImgPlay1" />
                    </div>
                    <h1 className="flex justify-center items-center text-[30px]">
                      <OrdersInProgressToday/>
                    </h1>
                  </div>

                  <div className="col-span-4 md:col-span-4 lg:col-span-4 h-[85px] bg-[#FFFFFF] rounded-[19px] shadow-md text-center">
                    <div className="flex flex-row pt-[13px] justify-between pl-[20px] pr-[20px] ">
                      <span className="text-[12px]">En pausa</span>
                      <Image src={ImgStop1} layout="fixed" alt="ImgStop1" />
                    </div>
                    <h1 className="flex justify-center items-center text-[30px]">
                      <OrdersPausedToday/>
                    </h1>
                  </div>
                  <div className="col-span-4 md:col-span-4 lg:col-span-4 h-[85px] bg-[#FFFFFF] rounded-[19px] shadow-md text-center">
                    <div className="flex flex-row pt-[13px] justify-between pl-[20px] pr-[20px] ">
                      <span className="text-[12px]">Finalizadas</span>
                      <Image src={ImgTiket1} layout="fixed" alt="ImgTiket1" />
                    </div>
                    <h1 className="flex justify-center items-center text-[30px]">
                      <OrdersFinishedToday/>
                    </h1>
                  </div>

                  <div className="col-span-4 md:col-span-4 lg:col-span-4 h-[85px] bg-[#FFFFFF] rounded-[19px] shadow-md text-center">
                    <div className="flex flex-row pt-[13px] justify-between pl-[20px] pr-[20px] ">
                      <span className="text-[12px] ">Cerradas</span>
                      <Image
                        src={ImgTwoTiket1}
                        layout="fixed"
                        alt="ImgTwoTiket1"
                      />
                    </div>
                    <h1 className="flex justify-center items-center text-[30px]">
                      <OrdersClosedToday/>
                    </h1>
                  </div>

                  <div className="col-span-4 md:col-span-4 lg:col-span-4 h-[85px] bg-[#FFFFFF] rounded-[19px] shadow-md text-center">
                    <div className="flex flex-row pt-[13px] justify-between pl-[20px] pr-[20px] ">
                      <span className="text-[12px] ">Entregadas</span>
                      <Image
                        src={ImgBandera1}
                        layout="fixed"
                        alt="ImgBandera1"
                      />
                    </div>
                    <h1 className="flex justify-center items-center text-[30px]">
                      <OrdersDeliveredToday/>
                    </h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="col-span-12 md:col-span-12 lg:col-span-4">
          <div className=" mt-[26px] ml-[20px] mr-[20px]">
            <div className="flex justify-center items">
              <h2 className="text-[24px] font-semibold">OTs Finalizadas</h2>
              <h2 className="text-[14px] mt-[10px]">
                /En espera de control de calidad
              </h2>
            </div>

            <div className=" grid grid-cols-12 gap-3">
              <div className="col-span-12 md:col-span-12 lg:col-span-12">
                <div className="mt-[14px]">
                  <ListOTs />
                </div>
              </div>
            </div>

            <div className="flex mt-[40px]">
              <h2 className="text-[24px] font-semibold">Órdenes de Trabajo</h2>
              <h2 className="text-[14px] mt-[10px]">
                / Mes actual vs anterior
              </h2>
            </div>
            <div className=" grid grid-cols-12 gap-3 mt-[15px] ">
              <div className="col-span-6 md:col-span-6 lg:col-span-12">
                <div className="flex flex-row h-[86px] bg-[#FFFF] rounded-[16px]  justify-between pl-[10px] pr-[10px] text-center shadow-md ">
                  <div>
                    <div className="text-[32px] md:text-[36px] lg:text-[40px] text-[#3682F7]">
                      <OrdersCreatedThisMonth/>
                    </div>
                    <div className="text-[14px] text-[#3682F7]">Mes actual</div>
                  </div>
                  <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full pt-[10px] mt-[15px]">
                    <Image src={ImgTable} layout="fixed" alt="ImgTable" />
                  </div>
                </div>
              </div>
              <div className="col-span-6 md:col-span-6 lg:col-span-12">
                <div className="flex flex-row h-[86px] bg-[#FFFF] rounded-[16px]  justify-between pl-[10px] pr-[10px] text-center shadow-md ">
                  <div>
                    <div className="text-[32px] md:text-[36px] lg:text-[40px] text-[#3682F7]">
                      <OrdersCreatedLastMonth/>
                    </div>
                    <div className="text-[14px] text-[#3682F7]">
                      Mes anterior
                    </div>
                  </div>
                  <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full pt-[10px] mt-[15px]">
                    <Image src={ImgTable} layout="fixed" alt="ImgTable" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="mt-[40px] ml-[20px] mr-[20px] ">
        <div className="flex flex-row">
          <h2 className="text-[24px] font-semibold">Órdenes de trabajo</h2>
          <h2 className="text-[24px]">/Año</h2>
        </div>

        <div>
          {/* <div className="flex flex-row"> */}
          <div className="grid grid-cols-12 gap-4 mt-[20px]">
            <div className="col-span-12 md:col-span-12 lg:col-span-2 flex justify-center items-center">
              <div className="grid grid-cols-12 gap-2">
                <div className="col-span-6 md:col-span-6 lg:col-span-12  rounded-2xl mt-[20px] pl-[15px] flex flex-col justify-center items-center pr-[15px] pb-[5px]">
                  <div className="bg-[#3682F7] w-[50px] h-[25px] rounded mt-2 flex justify-center  shadow-md">
                    <div className="bg-[#FFFF] w-[18px] h-[12px] rounded flex justify-center mt-2 ">
                      <p className="text-[#3682F7] text-[12px] font-semibold mt-[-4px]">
                        ↓
                      </p>
                    </div>
                    <p className="text-[14px] text-[#FFFF] mt-[3px] ml-[4px]">
                      15%
                    </p>
                  </div>
                  <div className="flex flex-col justify-center items-center">
                    <p className="text-[#000000] text-[40px] font-medium ">
                      <ContadorCreada />
                    </p>
                    <p className="text-[#000000] text-[15px] font-light mt-[-12px] ">
                      Entregadas
                    </p>
                  </div>
                </div>
                <div className="col-span-6 md:col-span-6 lg:col-span-12  rounded-2xl mt-[20px] pl-[15px] flex flex-col justify-center items-center pr-[15px] pb-[5px]">
                  <div className="bg-[#EE002B] w-[50px] h-[25px] rounded mt-2 flex justify-center  shadow-md">
                    <div className="bg-[#FFFF] w-[18px] h-[12px] rounded flex justify-center mt-2 ">
                      <p className="text-[#EE002B] text-[12px] font-semibold mt-[-4px]">
                        ↓
                      </p>
                    </div>
                    <p className="text-[14px] text-[#FFFF] mt-[3px] ml-[4px]">
                      15%
                    </p>
                  </div>
                  <div className="flex flex-col justify-center items-center">
                    <p className="text-[#000000] text-[40px] font-medium ">
                      <ContadorCitas />
                    </p>
                    <p className="text-[#000000] text-[15px] font-light mt-[-12px] ">
                      Canceladas
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-6">
              <BarChartOrdersCreatedFullYear/>
            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-4">
              <div className="bg-[#FFFF] md:h-[200px] lg:h-[242px] h-[200px] flex flex-col rounded-[24px] justify-center items-center text-[#000000]  ">
                {/* <GaugeChartOrdersMonth/> */}
                <p className="mt-5 ">Mes actual vs anterior</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
WorkOrder.auth = true;
WorkOrder.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};
export default WorkOrder;
