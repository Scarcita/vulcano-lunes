import React, { useState, useEffect } from "react";
import Image from "next/image";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import Layout from "../../components/layout";
import TableCompanyInf from "../../components/settings/tableCompanyInf";
import CarsCatalog from "../../components/settings/cars_catalog/carsCatalog";
import ColorsCatalog from "../../components/settings/Colors Catalog/colorsCatalog";
import EditColorsCatalog from "../../components/settings/Colors Catalog/modals/editCatalogColors";
import DeleteCatalogColors from "../../components/settings/Colors Catalog/modals/deleteCatalogColors";
import Services from "../../components/settings/services";
import AddNewCatalogColors from "../../components/settings/Colors Catalog/modals/addCatalogColors";


export default function DeskSettings() {
  const [selectedCatalog, setSelectedCatalog] = useState(0);
  const [reloadCarsCatalog, setReloadCarsCatalog] = useState(false);
  const [reloadCarsModels, setReloadCarsModels] = useState(false);
  const [reloadCarsModelsVersions, setReloadCarsModelsVersions] = useState(false);
  const [reloadColorsCatalog, setReloadColorsCatalog] = useState(false);
  const [reloadServicesCategory, setReloadServicesCategory] = useState(false);
  const [reloadServices, setReloadServices] = useState(false);
  const [selectedColorsName, setSelectedColorsName] = useState('');


  return (
    <div className="mt-[52px] ml-[20px] mr-[20px]">
      <div className="grid grid-cols-12 justify-left mt-10">
        <div className="col-span-12 md:col-span-12 lg:col-span-12">
          <Tabs>
            <TabList className="flex flex-row w-full md:w-[330px] lg:w-[440px] bg-[#E4E7EB] rounded-t-lg focus:outline-none">
              <Tab className="flex justify-center lg:w-[110px] md:w-[100px] items-center text-[12px] focus:text-[#fff] focus:bg-[#3682F7] rounded-tl-lg bg-[#E4E7EB] px-2">
                Company Inf.
              </Tab>
              <Tab className="text-gray-500 lg:w-[110px] md:w-[110px] text-[12px] flex justify-center items-center focus:bg-[#3682F7] focus:text-[#fff]  px-2 py-2">
                Cars Catalog
              </Tab>
              <Tab className="lg:w-[110px] text-gray-500 md:w-[110px] text-[12px] flex justify-center items-center focus:bg-[#3682F7] focus:text-[#fff]  px-2 py-2">
                Colors Catalog
              </Tab>
              <Tab className="lg:w-[110px] text-gray-500 md:w-[110px] text-[12px] flex justify-center items-center focus:bg-[#3682F7] focus:text-[#fff]  px-2 py-2">
                Services
              </Tab>
            </TabList>

            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  <TableCompanyInf/>
                </div>
              </div>
            </TabPanel>

            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  <CarsCatalog 
                    reloadCarsCatalog={reloadCarsCatalog} 
                    setReloadCarsCatalog={setReloadCarsCatalog}
                    reloadCarsModels={reloadCarsModels} 
                    setReloadCarsModels={setReloadCarsModels}
                    reloadCarsModelsVersions={reloadCarsModelsVersions}
                    setReloadCarsModelsVersions={setReloadCarsModelsVersions}
                  />
                </div>
              </div>
            </TabPanel>

            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12">
                  <div className="justify-end flex mt-[20px]">
                      <div>
                          <AddNewCatalogColors
                            reloadColorsCatalog={reloadColorsCatalog}
                            setReloadColorsCatalog={setReloadColorsCatalog}
                          />
                      </div>
                      <div>
                          <EditColorsCatalog
                            selectedCatalog={selectedCatalog}
                            selectedColorsName={selectedColorsName}
                            reloadColorsCatalog={reloadColorsCatalog}
                            setReloadColorsCatalog={setReloadColorsCatalog}
                          />
                      </div>
                      <div>
                          <DeleteCatalogColors 
                            selectedCatalog={selectedCatalog}
                            reloadColorsCatalog={reloadColorsCatalog}
                            setReloadColorsCatalog={setReloadColorsCatalog}
                            />
                      </div>
                  </div>
                    <ColorsCatalog 
                      selectedCatalog={selectedCatalog} 
                      setSelectedCatalog={setSelectedCatalog}
                      selectedColorsName={selectedColorsName}
                      setSelectedColorsName={setSelectedColorsName}
                      reloadColorsCatalog={reloadColorsCatalog}
                      setReloadColorsCatalog={setReloadColorsCatalog}
                    />
                </div>
              </div>
            </TabPanel>

            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  <Services
                    reloadServicesCategory={reloadServicesCategory}
                    setReloadServicesCategory={setReloadServicesCategory}
                    reloadServices={reloadServices} 
                    setReloadServices={setReloadServices}

                  />
                </div>
              </div>
            </TabPanel>
          </Tabs>
        </div>
      </div>
    </div>
  );
}
DeskSettings.auth = true;
DeskSettings.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};