import Head from "next/head";
import Image from "next/image";
import styles from "../styles/Home.module.css";
import { signOut, useSession } from "next-auth/react";
import Layout from "../components/layout";

import LoginCheckerButton from "../components/login-btn";
import ImgFondo from '../assets/img/login.jpg'

export default function Home() {
  return (
      <div className="grid grid-cols-12 container">
        <div className=" col-span-12 md:col-span-12 lg:col-span-12 container">
          <Image
              src={ImgFondo}
              layout="fill" 
              objectFit="cover"
              className="opacity-50 md:opacity-100 lg:opacity-100"
              alt='imageninicio'
            />
            <div className="grid grid-cols-12 h-screen self-center items-center">
              <div className="col-span-12 md:col-span-6 lg:col-span-6 relative px-10 ">
                <p className="text-[48px] font-medium leading-10">
                    You are <span className="font-bold text-[48px] italic">in control</span>
                </p>
                <p className="text-[48px] font-medium leading-10">
                    of every step.
                </p>
                <p className="text-[20px] text-[#000] mt-[30px] leading-6">
                    Vulcano makes it easy to administer every stage of the repair of the cars in your workshop
                </p>
                <div className="mt-[60px]">
                  <LoginCheckerButton />
                </div>
              </div>
            </div>
        </div>
    </div>
  );
}
