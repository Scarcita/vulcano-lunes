import React, { useEffect, useState } from "react";
import { Dots } from "react-activity";

import Layout from "../../components/layout";

import DatosVehiculos from "../../components/Appointments/New/DatosVehiculos";
import DatosClientes from "../../components/Appointments/New/DatosClientes";

import DetallesContacto from "../../components/Appointments/New/DetallesContacto";
import DetallesCitas from "../../components/Appointments/New/DetallesCitas";

import Observaciones from "../../components/Appointments/New/Observaciones";

export default function Agregar() {
  const [diaNumeralActual, setDiaActual] = useState(0);
  const [diaTextualActual, setDiaTextualActual] = useState("");
  const [mesNumeralActual, setMesActual] = useState(0);
  const [mesTextualActual, setMesTextualActual] = useState("");
  const [anioActual, setAnioActual] = useState(0);

  const [isDataFilled, setIsDataFilled] = useState(false);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);

  const [datosVehiculo, setDatosVehiculo] = useState({
    id: "",
    placa: "",
    vin: "",
    marca: "",
    modelo: "",
    version: "",
    color: "",
    anio: "",
    transmision: "",
  });
  const [datosCliente, setDatosCliente] = useState({
    id: "",
    telefono: "",
    email: "",
    ciNit: "",
    nombre: "",
  });
  const [detallesContacto, setDetallesContacto] = useState({
    nombre: "",
    telefono: "",
  });
  const [detallesCita, setDetallesCita] = useState({
    fecha: "",
    horaInicio: "",
    horaFin: "",
  });
  const [razon, setRazon] = useState("");
  const [observaciones, setObservaciones] = useState(null);

  useEffect(() => {
    console.log("Datos del vehiculo: ", datosVehiculo);
    console.log("Datos del cliente: ", datosCliente);
    console.log("Detalles de contacto: ", detallesContacto);
    console.log("Detalles de la cita: ", detallesCita);
    console.log("Observaciones: ", observaciones);
    console.log("Razon: ", razon);

    if (
      datosVehiculo.id != "" &&
      datosCliente.id != "" &&
      detallesContacto.nombre != "" &&
      detallesContacto.telefono != "" &&
      detallesCita.fecha != "" &&
      detallesCita.horaInicio != "" &&
      detallesCita.horaFin != "" &&
      razon != ""
    ) {
      setIsDataFilled(true);
    } else {
      setIsDataFilled(false);
    }
  }, [
    datosVehiculo,
    datosCliente,
    detallesContacto,
    detallesCita,
    observaciones,
    razon,
  ]);

  const sendData = () => {
    setLoading(true);
    const data = new FormData();
    data.append("car_id", datosVehiculo.id);
    // data.append("km", "0");
    data.append("client_id", datosCliente.id);
    data.append("contact_name", detallesContacto.nombre);
    data.append("contact_phone", detallesContacto.telefono);
    data.append("date", detallesCita.fecha);
    data.append("time_from", detallesCita.horaInicio);
    data.append("time_to", detallesCita.horaFin);
    data.append("reason", razon);
    observaciones != null
      ? data.append("observations", JSON.stringify(observaciones))
      : null;
    data.append("created_by", "1");

    fetch("https://slogan.com.bo/vulcano/appointments/addMobile", {
      method: "POST",
      body: data,
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.status == true) {
          console.log("Cita creada con exito: ", data);
          setLoading(false);
          setSuccess(true);
          window.location.reload();

          setTimeout(() => {
            setSuccess(false);
          }, 2000);
        } else {
          console.log("Error al crear cita: ", data);
          setLoading(false);
          setError(true);

          setTimeout(() => {
            setError(false);
          }, 2000);
        }
      })
      .catch((error) => {
        console.error("sendData: ", error);
      });
  };

  const getFechaActual = () => {
    const fechaActual = new Date();
    const diaNumeral = fechaActual.getDate();
    const diaTextual = fechaActual.toLocaleString("es-ES", { weekday: "long" });
    const mesNumeral = fechaActual.getMonth() + 1;
    const mesTextual = fechaActual.toLocaleString("es-ES", { month: "long" });
    const anio = fechaActual.getFullYear();

    setDiaActual(diaNumeral);
    setDiaTextualActual(
      diaTextual.charAt(0).toUpperCase() + diaTextual.slice(1)
    );
    setMesActual(mesNumeral);
    setMesTextualActual(
      mesTextual.charAt(0).toUpperCase() + mesTextual.slice(1)
    );
    setAnioActual(anio);
  };

  useEffect(() => {
    getFechaActual();
  }, []);

  return (
    // CONTENEDOR PRINCIPAL
    <div
      className="
    w-full h-screen flex flex-col justify-start items-center bg-[#F6F6FA]
    "
    >
      {/* CABECERA */}
      <div
        className="
      w-full h-24 flex flex-row justifify-center items-center px-8
      {/*DESKTOP*/}
      lg:h-20  
      "
      >
        {/* TITULO */}
        <div
          className="
        w-3/4 h-full flex flex-col justify-center items-start
        {/*DESKTOP*/}
        lg:flex-row lg:justify-start lg:items-center
        "
        >
          <p
            className="
          text-2x1 font-bold font-Dosis text-[#000000]
          {/*DESKTOP*/}
          lg:text-3xl
          "
          >
            Agendar nueva cita
          </p>
          <p
            className="
          text-2x1 font-Dosis hidden
          {/*DESKTOP*/}
          lg:text-3xl lg:flex
          "
          >
            &nbsp;/&nbsp;
          </p>
          <p
            className="
          text-2x1 font-Dosis
          {/*DESKTOP*/}
          lg:text-3xl
          "
          >
            {diaTextualActual} {diaNumeralActual} de {mesTextualActual} del{" "}
            {anioActual}
          </p>
        </div>
        {/* BOTON */}
        <div
          className="
        w-1/4 flex flex-row justify-end items-center 
        "
        >
          <button
            type={"button"}
            className={`
            w-28 h-10 border-[#3682F7] border-2 rounded-full text-sm text-center font-Dosis font-medium bg-[#F6F6FA] text-[#3682F7] focus:
             outline-none z-100
            ${isDataFilled ? "bg-[#3682F7] text-[#fff]" : "cursor-not-allowed"}
            ${loading ? "cursor-not-allowed" : ""}
            ${success ? "cursor-not-allowed bg-green-500 border-green-500" : ""}
            ${error ? "cursor-not-allowed bg-red-500 border-red-500" : ""}
            {/*DESKTOP*/}
            lg:w-32 lg:h-12 lg:text-lg
            {/*HOVER*/}
            transform hover:scale-105
            `}
            onClick={sendData}
            disabled={!isDataFilled || loading || success || error}
          >
            {loading ? (
              <Dots color={"#fff"} size={20} className="mb-1" />
            ) : success ? (
              "Guardado"
            ) : error ? (
              "Error"
            ) : (
              "Guardar"
            )}
          </button>
        </div>
      </div>

      {/* CUERPO */}
      <div
        className="
      w-full h-full flex flex-col justfiy-start items-center overflow-scroll
      {/*DESKTOP*/}
      lg:justify-between
      "
      >
        {/* DATOS DE VIHÍCULOS Y CLIENTES */}
        <div
          className="
          w-full h-fit flex flex-col justify-start items-center
          {/*DESKTOP*/}
          lg:h-2/4 lg:flex-row lg:justify-center lg:px-4
          "
        >
          {/* DATOS DE VEHÍCULOS */}
          <div
            className="
            w-5/6 h-[700px] flex flex-row justify-center items-center mx-4 mb-4
            {/*DESKTOP*/}
            lg:w-1/2 lg:h-full lg:mb-0 lg:py-4
            "
          >
            <DatosVehiculos setDatosVehiculo={setDatosVehiculo} />
          </div>
          {/* DATOS DE CLIENTES */}
          <div
            className="
            w-5/6 h-[600px] flex flex-row justify-center items-center mx-4 mb-4
            {/*DESKTOP*/}
            lg:w-1/2 lg:h-full lg:mb-0 lg:py-4
            "
          >
            <DatosClientes setDatosCliente={setDatosCliente} />
          </div>
        </div>
        {/* DETALLES DE CONTACTOS Y CITAS */}
        <div
          className="
        w-full h-fit flex flex-col justify-start items-center
        {/*DESKTOP*/}
        lg:h-1/5 lg:flex-row lg:justify-center lg:px-4
        "
        >
          {/* DETALLES DE CONTACTO */}
          <div
            className="
          w-5/6 flex flex-row justify-center items-center mx-4 mb-4
          {/*DESKTOP*/}
          lg:w-1/2 lg:h-full lg:mb-0 lg:py-4
          "
          >
            <DetallesContacto setDetallesContacto={setDetallesContacto} />
          </div>
          {/* DETALLES DE CITAS */}
          <div
            className="
          w-5/6 flex flex-row justify-center items-center mx-4 mb-4
          {/*DESKTOP*/}
          lg:w-1/2 lg:h-full lg:mb-0 lg:py-4
          "
          >
            <DetallesCitas setDetallesCita={setDetallesCita} />
          </div>
        </div>
        {/* OBSERVACIONES */}
        <div
          className="
        w-full h-fit flex flex-col justify-start items-center
        {/*DESKTOP*/}
        lg:h-1/3 lg:flex-row lg:justify-center lg:px-4
        "
        >
          <div
            className="
          w-5/6 flex flex-row justify-center items-center mx-4 mb-4
          {/*DESKTOP*/}
          lg:w-full lg:h-full
          "
          >
            <Observaciones
              observaciones={observaciones}
              setObservaciones={setObservaciones}
              setRazon={setRazon}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

Agregar.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

Agregar.auth = true;
