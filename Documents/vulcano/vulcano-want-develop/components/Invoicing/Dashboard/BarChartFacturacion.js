import React, { useEffect, useState } from 'react';
import Chart from "chart.js";

export default function BarChartFacturacion() {
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);

    useEffect(() => {

        setIsLoading(true)

        fetch('https://slogan.com.bo/vulcano/orders/invoicingDashboard')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    var temp = [];
                    Object.values(data.data).map((result) =>{
                        temp.push(result);
                    })
    
                    setData(temp)
                    console.log(temp);
                    
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

    }, [])

    React.useEffect(() => {
        let config = {
          type: "bar",
          data: {
            labels: [
              "Jan",
              "Feb",
              "Mar",
              "Apr",
              "May",
              "Jun",
              "Jul",
              "Ago",
              "Sep",
              "Oct",
              "Nov",
              "Dec",
    
            ],
            datasets: [
              {
                label: new Date().getFullYear(),
                backgroundColor: "#3682F7",
                //borderColor: "#4a5568",
                data: [
                  data[4]
                ],
                fill: false,
                barThickness: 6,
              },
            ],
          },
          options: {
            maintainAspectRatio: false,
            responsive: true,
            title: {
              display: false,
              text: "Orders Chart",
            },
            tooltips: {
              mode: "index",
              intersect: false,
            },
            hover: {
              mode: "nearest",
              intersect: true,
            },
            legend: {
              labels: {
                fontColor: "rgba(0,0,0,.4)",
                color: "#3682F7"
              },
              align: "end",
              position: "bottom",
            },
            scales: {
              xAxes: [
                {
                  ticks: {
                    fontColor: "#000000",
                  },
                  display: false,
                  scaleLabel: {
                    display: true,
                    labelString: "Month",
                  },
                  gridLines: {
                    borderDash: [2],
                    borderDashOffset: [2],
                    color: "rgba(33, 37, 41, 0.3)",
                    zeroLineColor: "rgba(33, 37, 41, 0.3)",
                    zeroLineBorderDash: [2],
                    zeroLineBorderDashOffset: [2],
                  },
                },
              ],
              yAxes: [
                {
                  display: true,
                  scaleLabel: {
                    display: false,
                    labelString: "Value",
                  },
                  gridLines: {
                    borderDash: [2],
                    drawBorder: false,
                    borderDashOffset: [2],
                    color: "rgba(33, 37, 41, 0.2)",
                    zeroLineColor: "rgba(33, 37, 41, 0.15)",
                    zeroLineBorderDash: [2],
                    zeroLineBorderDashOffset: [2],
                  },
                },
              ],
            },
          },
        };
    
        let ctx = document.getElementById("bar-chart-services").getContext("2d");
        window.myBar = new Chart(ctx, config);
      }, []);
      
      return (
        <>
          <div className="">
              <div className=" h-[100px]">
                <canvas id="bar-chart-services"></canvas>
              </div>
          </div>
        </>
      );
    }
    
    
    