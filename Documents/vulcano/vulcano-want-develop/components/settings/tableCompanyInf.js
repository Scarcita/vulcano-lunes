import React, { useEffect, useState, useRef } from "react";
import Image from "next/image";
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Dots } from 'react-activity';
import "react-activity/dist/Dots.css";

import { Bounce, Sentry } from "react-activity";
import "react-activity/dist/library.css";
import CreatableSelect from "react-select/creatable";
import { setLocale } from "yup";


function TableCompanyInf(props) {
  const selectRef = useRef();

  const [isLocked, setIsLocked] = useState(false);
  const [isLoading, setIsLoading] = useState(false)
  const [userDataLoading, setUserDataLoading] = useState(false);
  const [companyName, setCompanyName] = useState("");
  const [address, setAdress] = useState("");
  const [zipNumber, setZipNumber] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [licenseNumber, setLicenseNumber] = useState("");
  const [salesTax, setSalestax] = useState("");

  const clearForm = () => {
    setCompanyName('')
    setAdress('')
    setZipNumber('')
    setPhone('')
    setEmail('')
    setLicenseNumber('')
    setSalestax('')
}

  const getDataOrden = async (formData) => {
    
    setIsLoading(true)
      var data = new FormData();

      data.append("company_name", companyName);
      data.append("company_address", address);
      data.append("company_zip", zipNumber);
      data.append("company_phone", phone);
      data.append("company_email", email);
      data.append("company_license_number", licenseNumber);
      data.append("company_sales_tax", salesTax);

    fetch("https://slogan.com.bo/vulcano/settings/editCompanyInfoMobile", {
      method: 'POST',
      body: data,
    })
      .then(response => response.json())
      .then(data => {
        console.log('VALORES ENVIADOS: ', data);

        setIsLoading(false)
        if(data.status){
          clearForm()
          //setReloadUsers(!reloadUsers)
          //setShowModal(false)
        } else {
          alert(JSON.stringify(data.errors, null, 4))
        }
        
      },
      (error) => {
        console.log(error)
      }
      )
  }
  

  const validationSchema = Yup.object().shape({
    companyName: Yup.string()
    .required('companyName is required'),

    phone: Yup.string()
      .required("phone is required"),
    // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    email: Yup.string()
      .required('email is required'),
    //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    });

    const formOptions = { resolver: yupResolver(validationSchema) };

    // get functions to build form with useForm() hook
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
        console.log('string is NOT empty')
        getDataOrden(data)
        return false;
        
    }


  return (
    // COTENERDOR PRINCIPAL
    <form onSubmit={handleSubmit(onSubmit)} className="grid grid-cols-12">
        <div className="col-span-12 md:col-span-12 lg:col-span-12 justify-center items-center px-4 bg-[#fff] rounded-lg shadow-lg rounded-lg"
        >
          <div className="grid grid-cols-12">
            <div className="col-span-12 md:col-span-12 lg:col-span-12 mt-[20px]">
              <div className="flex justify-between">
                <div>
                  <p className="font-medium text-lg"> Detalles de registro</p>
                </div>
                <div>
                  <div
                    className="w-[19px] h-[18px] m-2"
                    onClick={() => {
                      setIsLocked(!isLocked);
                    }}
                  >
                    {isLocked == false ? (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        fill="currentColor"
                        className="
                      text-[#D3D3D3] w-[19px] h-[18px] cursor-pointer
                      {/*HOVER*/}
                      hover:text-[#000]
                      "
                      >
                        <path d="M18 1.5c2.9 0 5.25 2.35 5.25 5.25v3.75a.75.75 0 01-1.5 0V6.75a3.75 3.75 0 10-7.5 0v3a3 3 0 013 3v6.75a3 3 0 01-3 3H3.75a3 3 0 01-3-3v-6.75a3 3 0 013-3h9v-3c0-2.9 2.35-5.25 5.25-5.25z" />
                      </svg>
                    ) : (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        fill="currentColor"
                        className="
                      text-[#D3D3D3] w-[19px] h-[18px] cursor-pointer
                      {/*HOVER*/}
                      hover:text-[#000] 
                      "
                      >
                        <path
                          fillRule="evenodd"
                          d="M12 1.5a5.25 5.25 0 00-5.25 5.25v3a3 3 0 00-3 3v6.75a3 3 0 003 3h10.5a3 3 0 003-3v-6.75a3 3 0 00-3-3v-3c0-2.9-2.35-5.25-5.25-5.25zm3.75 8.25v-3a3.75 3.75 0 10-7.5 0v3h7.5z"
                          clipRule="evenodd"
                        />
                      </svg>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="grid grid-cols-12 gap-4">
            <div className="col-span-12 md:col-span-12 lg:col-span-12">
              <div className="">
                <label className=" w-full self-start text-s m text-[#A5A5A5] font-light">
                  Company Name
                </label>
                <div className="w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border rounded-lg">
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input className=" w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none"
                      type="text"
                      disabled={isLocked}
                      placeholder="Descripcion"
                      name="companyName"
                      {...register("companyName")}
                      value={companyName}
                      onChange={(e) => {
                        setCompanyName(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>

            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-4">
              <div className="">
                <label className=" w-full self-start text-s m text-[#A5A5A5] font-light">
                  Address
                </label>
                <div className="w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border rounded-lg">
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input className=" w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none"
                      type="text"
                      disabled={isLocked}
                      placeholder="Descripcion"
                      name="address"
                      {...register("address")}
                      value={address}
                      onChange={(e) => {
                        setAdress(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>

            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-4">
              <div className="">
                <label className=" w-full self-start text-s m text-[#A5A5A5] font-light">
                  Zip Number
                </label>
                <div className="w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border rounded-lg">
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input className=" w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none"
                      type="text"
                      disabled={isLocked}
                      placeholder="Descripcion"
                      name="zipNumber"
                      {...register("zipNumber")}
                      value={zipNumber}
                      onChange={(e) => {
                        setZipNumber(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>

            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-4">
              <div className="">
                <label className=" w-full self-start text-s m text-[#A5A5A5] font-light">
                  Phone
                </label>
                <div className="w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border rounded-lg">
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input className=" w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none"
                      type="number"
                      disabled={isLocked}
                      placeholder="Descripcion"
                      name="phone"
                      {...register("phone")}
                      value={phone}
                      onChange={(e) => {
                        setPhone(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>

            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-4">
              <div className="">
                <label className=" w-full self-start text-s m text-[#A5A5A5] font-light">
                  Email
                </label>
                <div className="w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border rounded-lg">
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input className=" w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none"
                      type="email"
                      disabled={isLocked}
                      placeholder="Descripcion"
                      name="email"
                      {...register("email")}
                      value={email}
                      onChange={(e) => {
                        setEmail(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>

            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-4">
              <div className="">
                <label className=" w-full self-start text-s m text-[#A5A5A5] font-light">
                  License Number
                </label>
                <div className="w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border rounded-lg">
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input className=" w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none"
                      type="text"
                      disabled={isLocked}
                      placeholder="Descripcion"
                      name="licenseNumber"
                      {...register("licenseNumber")}
                      value={licenseNumber}
                      onChange={(e) => {
                        setLicenseNumber(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>

            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-4">
              <div className="">
                <label className=" w-full self-start text-s m text-[#A5A5A5] font-light">
                  Sales Tax
                </label>
                <div className="w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border rounded-lg">
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input className=" w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none"
                      type="text"
                      disabled={isLocked}
                      placeholder="Descripcion"
                      name="salesTax"
                      {...register("salesTax")}
                      value={salesTax}
                      onChange={(e) => {
                        setSalestax(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
          
          {/* BOTONES */}
          <div
            className="w-full h-14 flex flex-row justify-end items-center mb-2"
          >
            <button
              type="submit"
              className={`w-24 h-10 border rounded-lg text-[#fff] text-center text-[14px] font-Dosis ml-2 outline-none ${
                isLocked
                  ? "text-[#D9D9D9]"
                  : "bg-[#3682F7] border-[#3682F7] hover:bg-transparent hover:text-[#3682F7]"
              }
              `}
              disabled={isLocked}
            >
              {isLoading ? <Dots className='m-auto' size={9} color={'#fff'}></Dots> : 'Submit'}
              
            </button>
          </div>
        </div>
    </form>
  );
}
export default TableCompanyInf;
