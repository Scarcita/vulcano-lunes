import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Dots } from 'react-activity';
import "react-activity/dist/Dots.css";


export default function EditServices(props) {

    const {
        Services, 
        selectedServices,
        reloadServices,
        setReloadServices,
        selectedServicesName,
        selectedServicesCode,
        selectedServicesTime,
        selectedServicesPrice

    } = props
    console.log('NAME SERVICE: ' + selectedServicesName);
    console.log('CODE SERVICE: ' + selectedServicesCode);
    console.log('TIME SERVICE: ' + selectedServicesTime);
    console.log('PRICE SERVICE: ' + selectedServicesPrice);
    const [showModal, setShowModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false)
    const [name, setName] = useState(selectedServicesName)
    const [code, setCode] = useState(selectedServicesCode)
    const [time, setTime] = useState(selectedServicesTime)
    const [price, setPrice] = useState(selectedServicesPrice)


    const clearForm = () => {
        setCode('')
        setName('')
        setTime('')
        setPrice('')
    }
    
  const getDataOrden = async (formData) => {
    
    setIsLoading(true)
        var data = new FormData();
        data.append("category_id", selectedServices);
        data.append("code", code);
        data.append("name", name);
        data.append("time", time);
        data.append("price", price);
 
    fetch("https://slogan.com.bo/vulcano/services/editMobile/" + Services , {
      method: 'POST',
      body: data,
    })
      .then(response => response.json())
      .then(data => {
        console.log(data);

        setIsLoading(false)
        if(data.status){
          clearForm()
          setReloadServices(!reloadServices)
          setShowModal(false)
        } else {
          alert(JSON.stringify(data.errors, null, 4))
        }
        
      },
      (error) => {
        console.log(error)
      }
      )

  }
 
  ////////////////// VALIDATION ///////////////////

  
  const validationSchema = Yup.object().shape({
    code: Yup.string()
    .required('code is required'),

    name: Yup.string()
    .required('name is required'),

    time: Yup.string()
    .required('time is required'),

    price: Yup.string()
    .required('price is required'),
    });


    const formOptions = { resolver: yupResolver(validationSchema) };

    // get functions to build form with useForm() hook
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
        console.log('string is NOT empty')
        getDataOrden(data)
        return false;
    }

    const MostrarAlert = () => {
        if(selectedBrand === 0){
            alert('No se a alegido ningun item')
        }
    }


    return (
        <>
            <div>
                <button
                    className="w-[52px] h-[25px] text-[#FFFFFF] bg-[#3682F7] text-[12px] rounded-[4px] text-center items-center hover:bg-transparent hover:border-[#3682F7] hover:border hover:text-[#3682F7] justify-center mr-[10px]"
                    type="button"
                    onClick={() => setShowModal(true)}
                    // disabled={!buttonActive}
                >
                    Edit
                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                <div>
                                    <h4 className="text-lg font-medium text-gray-800">
                                        Edit
                                    </h4>

                                </div>
                                <div>
                                    <form onSubmit={handleSubmit(onSubmit)}>

                                        <div className="mt-[20px] grid grid-cols-12 gap-4">

                                            <div className='col-span-12 md:col-span-12 lg:col-span-6'>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Name:</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="text"
                                                    name="name"
                                                    {...register('name')}
                                                    value={name}
                                                    onChange={(e) => {
                                                        setName(e.target.value)
                                                    }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.name?.message}</div>
                                            </div>

                                            <div className='col-span-12 md:col-span-12 lg:col-span-6'>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Code:</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="text"
                                                    name="code"
                                                    {...register('code')}
                                                    value={code}
                                                    onChange={(e) => {
                                                        setCode(e.target.value)
                                                    }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.code?.message}</div>
                                            </div>

                                            <div className='col-span-12 md:col-span-12 lg:col-span-6'>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Time:</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="number"
                                                    name="time"
                                                    {...register('time')}
                                                    value={time}
                                                    onChange={(e) => {
                                                        setTime(e.target.value)
                                                    }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.time?.message}</div>
                                            </div>

                                            <div className='col-span-12 md:col-span-12 lg:col-span-6'>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Price:</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="number"
                                                    name="price"
                                                    {...register('price')}
                                                    value={price}
                                                    onChange={(e) => {
                                                        setPrice(e.target.value)
                                                    }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.price?.message}</div>
                                            </div>
                                        </div>

                                        <div className="flex flex-row justify-between mt-[20px]">

                                            <div>
                                                <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                            </div>

                                            <div>
                                                <button
                                                    className="w-[72px] h-[30px] text-[#FFFFFF] bg-[#000] text-[12px] rounded-[9px] text-center items-center hover:bg-transparent hover:border-[#000] hover:border hover:text-[#000] justify-center mr-[10px]"
                                                    onClick={() =>
                                                        setShowModal(false)
                                                    }
                                                    disabled={isLoading}
                                                >
                                                    Cancel
                                                </button>
                                                <button
                                                    className="w-[72px] h-[30px] text-[#FFFFFF] bg-[#3682F7] text-[12px] rounded-[9px] text-center items-center hover:bg-transparent hover:border-[#3682F7] hover:border hover:text-[#3682F7] justify-center"
                                                   type="submit"
                                                   disabled={isLoading}

                                                >
                                                    {isLoading ? <Dots className='m-auto' size={7} color={'#fff'}></Dots> : 'Update'}
                                                    
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}