import React, { useState, useEffect } from "react";
import Image from "next/image";
import ListCArs from "./ListCarsAllMakes";
import ListCArsModels from "./listCarsModels";
import ListCArsModelsVersions from "./listCarsModelsVersions";
import EditCarsAllMakes from "./modal/editCarsAllMakes";
import AddNewCarsAllMakes from "./modal/addNewCarsAllMakes";
import EditCarsModels from "./modal/editCarsModels";
import AddNewCarsModels from "./modal/addNewCarsModels";
import EditCarsVersions from "./modal/editCarsVersions";
import AddNewCarsVersions from "./modal/addNewCarsVersions";
import DeleteCarsAllMakes from "./modal/deleteCarsAllMakes";
import DeleteCarsModels from "./modal/deleteCarsModels";
import DeleteCarsVersions from "./modal/deleteCarsVersions";

export default function CarsCatalog(props) {
    const [selectedBrand, setSelectedBrand] = useState(0);
    const [selectedBrandVersions, setSelectedBrandVersions] = useState(0);
    const [selectedCarsVersions, setSelectedCarsVersions] = useState(0);
    const [selectedBrandName, setSelectedBrandName] = useState('')
    const [selectedAdditionalInfo, setSelectedAdditionalInfo] = useState('')
    const [selectedModelsName, setSelectedModelsName] = useState('')
    const [selectedVersionssName, setSelectedVersionsName] = useState('')
    const {
        reloadCarsCatalog, 
        setReloadCarsCatalog,
        reloadCarsModels,
        setReloadCarsModels,
        reloadCarsModelsVersions,
        setReloadCarsModelsVersions
    } = props



    return (
        <div className="grid grid-cols-12 gap-4 mt-[30px]">
            <div className="col-span-12 md:col-span-4 lg:col-span-4 ">
                <div className="justify-end flex">
                    <div>
                        <AddNewCarsAllMakes 
                        reloadCarsCatalog={reloadCarsCatalog} 
                        setReloadCarsCatalog={setReloadCarsCatalog}/>
                    </div>
                    <div>
                        <EditCarsAllMakes 
                            selectedBrand={selectedBrand} 
                            selectedBrandName={selectedBrandName}
                            selectedAdditionalInfo={selectedAdditionalInfo}
                            reloadCarsCatalog={reloadCarsCatalog} 
                            setReloadCarsCatalog={setReloadCarsCatalog}
                        />
                    </div>
                    <div>
                        <DeleteCarsAllMakes 
                        selectedBrand={selectedBrand} 
                        reloadCarsCatalog={reloadCarsCatalog} 
                        setReloadCarsCatalog={setReloadCarsCatalog}/>
                    </div>
                </div>
                <div className="w-full h-[620px] bg-[#fff] rounded-[10px] shadow-md p-3 mt-[10px]">
                    <ListCArs 
                        selectedBrand={selectedBrand}
                        setSelectedBrand={setSelectedBrand}
                        selectedBrandName={selectedBrandName} 
                        setSelectedBrandName={setSelectedBrandName}
                        selectedAdditionalInfo={selectedAdditionalInfo}
                        setSelectedAdditionalInfo={setSelectedAdditionalInfo}
                        reloadCarsCatalog={reloadCarsCatalog} 
                        setReloadCarsCatalog={setReloadCarsCatalog}
                    />
                </div>
            </div>
            <div className="col-span-12 md:col-span-4 lg:col-span-4 ">
                <div className="justify-end flex">
                    <div>
                        <AddNewCarsModels 
                            selectedBrand={selectedBrand}
                            reloadCarsModels={reloadCarsModels}
                            setReloadCarsModels={setReloadCarsModels}
                        />
                    </div>
                    <div>
                        <EditCarsModels 
                            selectedBrand={selectedBrand}
                            selectedBrandVersions={selectedBrandVersions}
                            selectedModelsName={selectedModelsName}
                            reloadCarsModels={reloadCarsModels}
                            setReloadCarsModels={setReloadCarsModels}
                        />
                    </div>
                    <div>
                        <DeleteCarsModels 
                            selectedBrandVersions={selectedBrandVersions}
                            reloadCarsModels={reloadCarsModels}
                            setReloadCarsModels={setReloadCarsModels}
                        />
                    </div>
                </div>
                <div className="w-full h-[620px] bg-[#fff] rounded-[10px] shadow-md p-3 mt-[10px]">
                    <ListCArsModels 
                        selectedBrand={selectedBrand} 
                        setSelectedBrandVersions={setSelectedBrandVersions}
                        reloadCarsModels={reloadCarsModels}
                        setReloadCarsModels={setReloadCarsModels}
                        selectedModelsName={selectedModelsName}
                        setSelectedModelsName={setSelectedModelsName}
                        />
                </div>
            </div>
            <div className="col-span-12 md:col-span-4 lg:col-span-4 ">
                <div className="justify-end flex">
                    <div>
                        <AddNewCarsVersions 
                            selectedBrandVersions={selectedBrandVersions}
                            reloadCarsModelsVersions={reloadCarsModelsVersions}
                            setReloadCarsModelsVersions={setReloadCarsModelsVersions}
                        />
                    </div>
                    <div>
                        <EditCarsVersions 
                            selectedCarsVersions={selectedCarsVersions} 
                            selectedBrandVersions={selectedBrandVersions}
                            reloadCarsModelsVersions={reloadCarsModelsVersions}
                            selectedVersionssName={selectedVersionssName}
                            setReloadCarsModelsVersions={setReloadCarsModelsVersions}
                            />
                    </div>
                    <div>
                        <DeleteCarsVersions 
                            selectedCarsVersions={selectedCarsVersions} 
                            selectedBrandVersions={selectedBrandVersions}
                            reloadCarsModelsVersions={reloadCarsModelsVersions}
                            setReloadCarsModelsVersions={setReloadCarsModelsVersions}
                        />
                    </div>
                </div>
                <div className="w-full h-[620px] bg-[#fff] rounded-[10px] shadow-md p-3 mt-[10px]">
                    <ListCArsModelsVersions 
                        selectedBrandVersions={selectedBrandVersions} 
                        setSelectedCarsVersions={setSelectedCarsVersions}
                        selectedVersionssName={selectedVersionssName}
                        setSelectedVersionsName={setSelectedVersionsName}
                        reloadCarsModelsVersions={reloadCarsModelsVersions}

                    />
                </div>
            </div>

        </div>
    )
}