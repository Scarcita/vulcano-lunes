import React from 'react';
import { useEffect, useState } from 'react'
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'
import Image from 'next/image'

const ListCArs = (props) => {
    const { 
        setSelectedBrand, 
        setSelectedBrandName, 
        reloadCarsCatalog,
        setSelectedAdditionalInfo
    } = props;
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);
    

    useEffect(() => {
        setIsLoading(true)
        fetch('http://slogan.com.bo/vulcano/cars/allMakes')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })
    }, [reloadCarsCatalog])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 50 }}>
                    <Spinner color="#3682F7" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <div>
                <ListData 
                    data={data} 
                    setData={setData} 
                    setSelectedBrand={setSelectedBrand}
                    setSelectedBrandName={setSelectedBrandName}
                    setSelectedAdditionalInfo={setSelectedAdditionalInfo}
                    reloadCarsCatalog={reloadCarsCatalog}
                />
            </div>

    )
}

const ListData = (props) => {
    const { 
        data, 
        setSelectedBrand, 
        setSelectedBrandName,
        setSelectedAdditionalInfo
     } = props;
    

    return (
        <div className=''>
            {data.map(row =>
                <div key={row.id} className="flex border-b">
                    <div className='m-[5px] rounded-full bg-[#F3F3F3] w-[25px] h-[25px]'>
                        {row.additional_info !== null ?
                            <Image
                                className='rounded-full bg-[#F3F3F3]'
                                src={row.additional_info}
                                alt="media"
                                height={25}
                                width={25}>

                            </Image>
                            : <></>
                        }
                    </div>
                    <button className='self-center text-[14px] hover:text-[#D9D9D9] focus:text-[#fff] focus:bg-[#3682F7] rounded-[4px] px-[10px]'
                        onClick={() => {
                            setSelectedBrand(row.id)
                            setSelectedBrandName(row.name)
                            setSelectedAdditionalInfo(row.additional_info)
                        }}
                    >
                        {row.name}
                    </button>
                </div>

            )}

        </div>
    )
}

export default ListCArs;