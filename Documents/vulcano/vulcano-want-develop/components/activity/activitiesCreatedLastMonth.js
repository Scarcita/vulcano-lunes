import React, { useEffect, useState } from 'react';
import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";

export default function CountLastMonth() {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);
  console.log(data);

  const getDatos = () => {
    setIsLoading(true);
    fetch("https://slogan.com.bo/vulcano/clients/crmDashboard")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
            var temp = [];
            Object.values(data.data).map((result) =>{
                temp.push(result);
            })
            setData(temp[1])
        } else {
          console.error(data.error);
        }
        setIsLoading(false);
      });
  };
  useEffect(() => {
    getDatos();
  }, []);

  return(
      isLoading ?

      <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
          <Spinner color="#3682F7" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
      </div>
      :
      <div>
        {data}
      </div>
  )
}