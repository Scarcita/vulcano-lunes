import React, { useEffect, useState } from 'react';
import GaugeChart from "react-gauge-chart"

export default function GaugeChartOrders() {
    const [isLoading, setIsLoading] = useState(true)
    const [partsThisMonth, setPartsThisMonth] = useState([]);
    useEffect(() => {

        setIsLoading(true)

        fetch('https://slogan.com.bo/vulcano/orders/ordersDashboard')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    var temp = [];
                    Object.values(data.data).map((result) =>{
                        temp.push(result);
                    })
    
                    setPartsThisMonth(temp)
                    
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

    }, [])

    return(
       <>
            <div>
                <GaugeChart id="gauge-chart6"
                    nrOfLevels={420}
                    arcsLength={[
                        partsThisMonth[6],
                        partsThisMonth[7]
                    ]}
                    colors={['#71AD46', '#F6F6FA',]}
                    percent={
                        partsThisMonth[8]
                    }
                    needleBaseColor={'#71AD46'}
                    needleColor={'#71AD46'}
                    arcPadding={0.02}
                    textColor={'#000'}
                    />
            </div>
       </>
    )
}