import React, { useEffect, useState } from 'react';
import Image from 'next/image'
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'
import TooltipPlannedVisits from './tooltipPlannedVisits';


export default function TableVisitasPlanificadas() {
    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState([]);
    console.log(data);
  
    const getDatos = () => {
      setIsLoading(true);
      fetch("https://slogan.com.bo/vulcano/clients/crmDashboard")
        .then((response) => response.json())
        .then((data) => {
          if (data.status) {
              var temp = [];
              Object.values(data.data).map((result) =>{
                  temp.push(result);
              })
              setData(temp[3])
          } else {
            console.error(data.error);
          }
          setIsLoading(false);
        });
    };
    useEffect(() => {
      getDatos();
    }, []);


    return (
        isLoading ?
            <div className='flex justify-center items-center self-center mt-[30px]'  >
                <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div >
                <div style={{maxHeight: '300px', overflowY: 'auto'}}>
                    <Table2 data={data} />
                </div>
                <div className="grid grid-cols-12 ">
                    <button
                    style={{position: 'sticky', top: 0}} className="col-span-12 md:col-span-12 lg:col-span-12 h-[35px] bg-[#3682F7] text-[#FFFFFF] rounded-b-[24px] hover:bg-[#FFFFFF] hover:text-[#3682F7]  hover:border-2 hover:border-[#3682F7]">Ver todo</button>
                </div>  
            </div>
    )
}


///////////////// TABLE DOS //////////

const Table2 = (props) => {

    const { data } = props;

    return (
    <div className="grid grid-cols-12">
        <table className={`col-span-12 md:col-span-12 lg:col-span-12 h-[300px] bg-[#FFFFFF] rounded-t-[24px]`}>
            <tbody>
                <div>
                    {data.map(row => <TableRow2 key={data.id} row={row} />)}
                </div>
            </tbody>
        </table>
    </div>

    );
};




class TableRow2 extends React.Component {

    render() {

        let row = this.props.row;

        if(row !== undefined){
            if (row !== null){
              if(row.delivery_date !== null && row.delivery_date !== undefined){
                var eventdate = row.delivery_date;
                var splitdatatime = eventdate.split('T')
                //var splitdate = eventdate.split('-');
                console.log(splitdatatime);
                var day = splitdatatime[0].split('-');
                console.log(day);
                var d = day[2];
              }
            }
          }
          if(row !== undefined){
            if (row !== null){
              if(row.delivery_date !== null && row.delivery_date !== undefined){
                var days = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
                var d = new Date(row.delivery_date);
                var dayName = days[d.getDay()];
              }
            }
          }
        
          if(row !== undefined){
            if (row !== null){
              if(row.delivery_date !== null && row.delivery_date !== undefined){
                var months = ['JAN', 'FEB ', 'MAR', 'APR', 'MAY', 'JUN', 'JUL','AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
                var m = new Date(row.delivery_date);
                var monthName = months[m.getMonth()];
              }
            }
          }
        

        return (
            <div className="grid grid-cols-12 gap-3">
                <div className='col-span-2 md:col-span-2 lg:col-span-2'>
                    <div className=" bg-[#FFFF] rounded-2xl justify-center shadow-md justify-center rows-center">
                        <div className="flex bg-[#FF0000] w-full h-[26px] rounded-t-2xl  justify-center ">
                            <div className="flex-col flex justify-center rows-center">
                                <p className="text-[#FFFF] text-[13px]">
                                    {dayName}
                                </p>
                            </div>
                        </div>
                        <div className="flex-col flex justify-center rows-center ">
                            <p className="font-bold text-[30px] mt-[-8px] text-center">
                                {/* {d} */}
                                14
                            </p>
                            <p className="font-bold text-[14px] mt-[-12px] text-center">
                                {monthName}
                            </p>
                        </div>
                    </div>
                </div>
                <div className="col-span-9 md:col-span-9 lg:col-span-9 self-center items-center">
                    <div className=" rounded-[13px] bg-[#3682F7] w-[75px] h-[20px] self-center items-center text-center">
                        <div className="text-[12px] text-[#fff] text-center">
                            {row.status}
                        </div>
                    </div>
                    <tr className="text-[14px] text-[#000000] font-bold">
                        {row.contact_name}
                    </tr>
                    <tr className="text-[14px] text-[#000000]">
                        {row.car.cars_models_version.cars_model.catalogues_record.name} • {row.car.cars_models_version.cars_model.name} • {row.car.plate}
                    </tr>
                </div>

                <div className='col-span-1 md:col-span-1 lg:col-span-1'>
                    <TooltipPlannedVisits/>
                </div>

            </div>
        )

    }
}


