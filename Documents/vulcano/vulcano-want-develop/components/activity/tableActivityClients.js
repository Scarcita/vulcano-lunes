import React, { useEffect, useState } from "react";
import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";
import Image from "next/image";
import TooltipClients from "./tooltipClients";

export default function TableActivityClients() {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);
  console.log(data);
  const headlist = ["", "Nombre", "Número de contacto", "E-mail", ""];

  const getDatos = () => {
    setIsLoading(true);
    fetch("https://slogan.com.bo/vulcano/clients/crmDashboard")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
            var temp = [];
            Object.values(data.data).map((result) =>{
                temp.push(result);
            })

            setData(temp[4])
        } else {
          console.error(data.error);
        }
        setIsLoading(false);
      });
  };
  useEffect(() => {
    getDatos();
  }, []);

  return isLoading ? (
    <div className="flex justify-center items-center self-center mt-[30px]">
      <Spinner
        color="#3682F7"
        size={17}
        speed={1}
        animating={true}
        style={{ marginLeft: "auto", marginRight: "auto" }}
      />
    </div>
  ) : (
    <div>
      <Table headlist={headlist} data={data} />
      <TableResponsive headlist={headlist} data={data} />
    </div>
  );
}

const Table = (props) => {
  const { headlist, data } = props;

  return (
    <div className="hidden lg:block md:block mt-[20px]">
      <table className="w-full bg-[#fff] rounded-[20px] shadow-[25px]">
        <thead className="">
          <tr>
            {headlist.map((header) => (
              <th
                key={headlist.id}
                className="text-[14px] border-b font-medium text-start text-[#A5A5A5] py-2"
              >
                {header}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {data.map((row) => {

            //let spliteado = row.created.split("T");
            //let created = spliteado[0];
            //let spliteado1 = row.created.split("T");
            //let delivery_promise= spliteado1[0];
            // let hora = spliteado[1].split("+")[0];

            return (
              <tr 
              key={row.id}
              >
                <td className="self-center">
                  <div className="w-[54px] h-[54px] bg-[#F6F6FA] rounded-full m-[5px] self-center text-center items-center justify-center">
                    
                  </div>
                </td>
                <td>
                  <p className="text-[14px] text-[#000] self-center">
                    {row.name}
                  </p>
                </td>
                <td>
                  <p className="text-[14px] text-[#000] self-center font-semibold">
                    {row.phone}
                  </p>
                </td>
                <td>
                  <p className="text-[14px] text-[#000] self-center font-semibold">
                    {row.email}
                  </p>
                </td>
                <td className="text-center">
                  <TooltipClients/>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

const TableResponsive = (props) => {
  const { data } = props;

  return (
    <div className="grid grid-cols-12 mt-[20px] ">
      <div className="col-span-12 md:col-span-12 lg:col-span-12 rounded-[10px] md:hidden">
        {data.map((row) => {
          //let spliteado = row.created.split("T");
          //let created = spliteado[0];
          //let spliteado1 = row.created.split("T");
          //let delivery_promise= spliteado1[0];

          return (
            <div
              key={row.id}
              className="grid grid-cols-12 bg-[#fff] rounded-[15px] shadow-md py-3 px-3 mb-[15px]"
            >
              <div className="col-span-11 md:col-span-12">
                <div className="grid grid-cols-12">
                  <div className="col-span-12 md:col-span-12 flex justify-between">
                    <div className="w-[44px] h-[44px] bg-[#fff] border-2 border-solid border-[#3682F7] rounded-full m-[5px] self-center text-center items-center justify-center">
                      <p className="text-[12px] text-[#3682F7]">
                        OT
                      </p>
                      <p className="font-semibold text-[14px] text-[#3682F7]">
                        {/* {row.id} */}
                      </p>
                    </div>
                    <div className="self-center items-center text-center text-[14px] text-[#000000] font-normal whitespace-nowrap ">
                      <p>
                        {/* {created}  */}
                      </p>
                      <p>
                      {/* {delivery_promise} */}
                      </p>
                    </div>
                      {row.status === "EN PAUSA" ?
                      <div className="w-[68px] h-[20px] bg-[#D3D3D3] rounded-[7px]">
                        <p className="text-[12px] text-[#fff] text-center self-center">
                          {/* {row.status} */}
                        </p>
                      </div>
                      : <></>
                      
                    }
                    {row.status === "FINALIZADO" ?
                      <div className="w-[68px] h-[20px] bg-[#3682F7] rounded-[7px]">
                        <p className="text-[12px] text-[#fff] text-center self-center">
                          {/* {row.status} */}
                        </p>
                      </div>
                      : <></>
                      
                    }
                    {row.status === "ABIERTO" ?
                      <div className="w-[68px] h-[20px] bg-[#0099ff] rounded-[7px]">
                        <p className="text-[12px] text-[#fff] text-center self-center">
                          {/* {row.status} */}
                        </p>
                      </div>
                      : <></>
                      
                    }
                    {row.status === "CERRADO" ?
                      <div className="w-[68px] h-[20px] bg-[#ff3300] rounded-[7px]">
                        <p className="text-[12px] text-[#fff] text-center self-center">
                          {/* {row.status} */}
                        </p>
                      </div>
                      : <></>
                      
                    }
                    {row.status === "EN CURSO" ?
                      <div className="w-[68px] h-[20px] bg-[#009900] rounded-[7px]">
                        <p className="text-[12px] text-[#fff] text-center self-center">
                          {/* {row.status} */}
                        </p>
                      </div>
                      : <></>
                      
                    }
                    {row.status === "ENTREGADO" ?
                      <div className="w-[68px] h-[20px] bg-[#ff9900] rounded-[7px]">
                        <p className="text-[12px] text-[#fff] text-center self-center">
                          {/* {row.status} */}
                        </p>
                      </div>
                      : <></>
                    }
                  </div>
                  <div className="col-span-12 md:col-span-12 mt-[5px]">
                    <div className="flex">
                      <div className="w-[48px] h-[48px] bg-[#D3D3D3] rounded-full self-center text-center pt-[7px] items-center ">
                        {/* <Image
                            className="items-center self-center text-center"
                            src={row.car.cars_models_version.cars_model.catalogues_record.additional_info}
                            layout="fixed"
                            alt="media"
                            width={30}
                            height={30}
                        /> */}
                      </div>
                      <div className="ml-[8px] self-center">
                        <p className="font-semibold text-[14px]">
                          {/* {row.car.cars_models_version.cars_model.catalogues_record.name} */}
                        </p>
                        <p className="text-[12px] font-light">
                        {/* {row.car.plate} */}
                          <span className="ml-[5px]">
                          {/* {row.car.vin} */}
                          </span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-span-12 md:col-span-12 flex justify-between mt-[5px]">
                    <div>
                      <p className="text-[14px] text-[#000] self-center">
                        {/* {row.contact_name} */}
                      </p>
                    </div>
                    <div>
                      <p className="text-[14px] text-[#000] self-center">
                        {/* {delivery_promise} */}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-span-1 md:col-span-12 ">
                {/* <TooltipOrders/> */}
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};
