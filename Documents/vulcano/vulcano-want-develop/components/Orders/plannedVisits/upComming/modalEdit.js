import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import ImgLike from '../../../../assets/img/like.svg'
import ImgDisLike from '../../../../assets/img/dislike.svg'


export default function ModalEditUpcomming(props) {

  const {
    id, 

  } = props;
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);


//   const actualizarDatos = async (formData) => {

//       setIsLoading(true)
//       var data = new FormData();

//       //console.log(formData.mediaUrl);
  
//       data.append("client_plan_id", client_plan_id);
//       data.append("social_network", formData.social_network);
//       data.append("type", formData.type);
//       data.append("planned_datetime", plannedDatetimeForm);
//       data.append("title", titleForm);
//       data.append("subtitle", subtitleForm);
//       data.append("instructions", instructionsForm);
//       data.append("post_copy", postCopyForm);
      
//       if(formData.mediaUrl.length !== 0){
//           data.append("media_url", formData.mediaUrl[0]);
//       }
  
//       fetch("http://slogan.com.bo/roadie/clientsPlansExtras/editMobile/" + id, {
//         method: 'POST',
//         body: data,
//       })
//         .then(response => response.json())
//         .then(data => {
//           console.log('VALOR ENDPOINTS EDIT: ', data);
//           setIsLoading(false)
//           if (data.status) { 
//               //setReloadPosts(!reloadPosts)
//               setShowModal(false)
//               console.log('edit endpoint: ' + data.status);
//           } else {
//               console.error(data.error)
//           }
//       })
  
//     }

  
  const validationSchema = Yup.object().shape({
  title: Yup.string()
  .required('title is required'),

  socialNetwork: Yup.string()
      .required('socialNetwork is required')
      .oneOf(['Facebook', 'Instagram', 'Mailing', 'YouTube', 'TikTok', 'LinkedIn', 'Twitter']),


  plannedDateTime: Yup.string()
      .required('plannedDateTime is required'),

  });
  const formOptions = { resolver: yupResolver(validationSchema) };

  // get functions to build form with useForm() hook
  const { register, handleSubmit, reset, formState } = useForm(formOptions);
  const { errors } = formState;

  function onSubmit(data) {
      alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
      console.log('string is NOT empty')
      actualizarDatos(data)
      return false;
      
  }

    return (
        <>
            <div className="flex items-center min-h-screen px-4 py-8">
                <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#F6F6FA] rounded-md shadow-lg">
                    <div>
                        <h4 className="text-lg font-medium text-gray-800 text-left">
                            EDIT POST
                        </h4>
                    </div>

                    <div>
                        <form onSubmit={handleSubmit(onSubmit)} >

                            <div className="mt-[20px] grid grid-cols-12 gap-4">
                              <div className='col-span-12 md:col-span-5 lg:col-span-5'>
                                <div className='text-[14px] font-semibold text-left'>
                                  Cliente
                                </div>
                                <div className='bg-[#FFFF] rounded-[8px] p-3 mt-[10px]'>
                                  <div className='flex flex row'>
                                    <div className="bg-[#E4E7EB] w-[48px] h-[48px] rounded-full justify-center">

                                    </div>
                                    <div className="ml-[10px]">
                                        <p className='text-[10px] font-normal text-left'>
                                            Register Into
                                        </p>
                                        <p className='text-[15px] font-semibold text-left'>
                                            CLIENTE NAME
                                        </p>
                                        <p className='text-[12px] font-normal text-left'>
                                            Created On: 
                                        </p>
                                    </div>
                                  </div>
                                  <div>
                                    <p className='text-[12px] text-left font-semibold'>
                                      Phone: <span className='text-[12px] font-normal text-left'>70738077</span>
                                    </p>
                                    <p className='text-[12px] text-left font-semibold'>
                                      Email: <span className='text-[12px] font-normal text-left'>wantdigitalagency@gmail.com</span>
                                    </p>
                                  </div>
                                </div>
                              </div>

                              <div className='col-span-12 md:col-span-7 lg:col-span-7'>
                                <div className='text-[14px] font-semibold text-left'>
                                  Order
                                </div>
                                <div className='bg-[#FFFF] rounded-[8px] p-3 mt-[10px]'>
                                  <div className="flex flex-row">
                                    <div className="w-[48px] h-[48px] bg-[#F6F6FA] rounded-full m-[5px] self-center text-center items-center justify-center">
                                      {/* <Image
                                      src={row.brand_logo}
                                      alt="brand"
                                      layout="fixed"
                                      width={30}
                                      height={30}/> */}

                                    </div>
                                    <div className="self-center items-center ml-[5px]">
                                        <div className=" rounded-[13px] bg-[#3682F7] w-[70px] h-[20px] self-center items-center text-center">
                                            <div className="text-[10px] text-[#fff] self-center items-center text-center pt-1">
                                                ABIERTO
                                            </div>
                                        </div>
                                        <p className="text-[13px] text-[#000000] font-bold text-left">
                                            FORD
                                        </p>
                                        <p className="text-[11px] text-[#000000] text-left">
                                            FJJ333 • DHDJ • JDJJDK
                                        </p>
                                    </div>
                                  </div>
                                  
                                  <div>
                                    <div className='flex flex-row justify-between'>
                                      <p className='text-[12px] text-left font-semibold'>
                                        Created on: <span className='text-[12px] font-normal text-left'>22/12/2022 17:77</span>
                                      </p>
                                      <p className='text-[12px] text-left font-semibold'>
                                        Delivered on: 7 <span className='text-[12px] font-normal text-left'> 22/12/2022 17:7</span>
                                      </p>
                                    </div>
                                    <p className='text-[12px] text-left font-semibold'>
                                      Advisor: <span className='text-[12px] font-normal text-left'> Juan C.</span>
                                    </p>
                                    
                                  </div>
                                </div>
                              </div>

                              <div className='col-span-12 md:col-span-5 lg:col-span-12'>
                                <div className='text-[14px] font-semibold text-left'>
                                  Activity details
                                </div>
                                <div className='bg-[#FFFF] rounded-[8px] p-3 mt-[10px]'>      
                                  <div className='grid grid-cols-12 gap-4'>
                                    <div className='col-span-2 md:col-span-2 lg:col-span-2'>
                                      <p className='text-[12px] text-left font-semibold'>
                                        Result
                                      </p>
                                      <input
                                        className="
                                      w-full h-[30px] text-sm text-left font-normal rounded-[11px] px-2 outline-none border border-[#D3D3D3]
                                      "
                                        type="date"
                                        //name="ciNit"
                                        // {...register("ciNit")}
                                        //value={ciNit}
                                        //onChange={(e) => {
                                          //setCiNit(e.target.value);
                                        //}}
                                      />
                                    </div>
                                    <div className='col-span-2 md:col-span-2 lg:col-span-2'>
                                      <p className='text-[12px] text-left font-semibold'>
                                        Result
                                      </p>
                                      <input
                                          className="
                                        w-full h-[30px] text-sm text-left font-normal rounded-[11px] px-2 outline-none border border-[#D3D3D3]
                                        "
                                          type="time"
                                          //name="ciNit"
                                          // {...register("ciNit")}
                                          //value={ciNit}
                                          //onChange={(e) => {
                                            //setCiNit(e.target.value);
                                          //}}
                                        />

                                    </div>
                                    <div className='col-span-4 md:col-span-4 lg:col-span-4'>
                                      <p className='text-[12px] text-left font-semibold'>
                                        Method
                                      </p>
                                      <div className='flex flex row self-center'>
                                        <div>
                                        • Call
                                        </div>
                                        <div>
                                        • E-Mail
                                        </div>
                                        <div>
                                        • Visit
                                        </div>

                                      </div>
                                    </div>
                                    <div className='col-span-4 md:col-span-4 lg:col-span-4'>
                                      <p className='text-[12px] text-left font-semibold'>
                                        Result
                                      </p>
                                      <div className='flex flex-row justify-between'>
                                        <div className='w-[75px] h-[48px] bg-[#71AD46] rounded-[17px] pt-[10px]'>
                                          <Image
                                          src={ImgLike}
                                          alt="brand"
                                          layout="fixed"
                                          width={30}
                                          height={30}/>

                                        </div>
                                        <div className='w-[75px] h-[48px] bg-[#A5A5A5] rounded-[17px] pt-[10px]'>
                                          <Image
                                          src={ImgDisLike}
                                          alt="brand"
                                          layout="fixed"
                                          width={30}
                                          height={30}/>

                                        </div>

                                      </div>

                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div className='col-span-12 md:col-span-5 lg:col-span-12'>
                                <p className='text-[12px] text-left font-semibold'>
                                  Additional info
                                </p>
                                <input
                                  className="
                                w-full h-[30px] text-sm text-left font-normal rounded-[11px] px-2 outline-none border border-[#D3D3D3]
                                "
                                  type="text"
                                  //name="ciNit"
                                  // {...register("ciNit")}
                                  //value={ciNit}
                                  //onChange={(e) => {
                                    //setCiNit(e.target.value);
                                  //}}
                                />
                              </div>

                            </div>

                            <div className="flex flex-row justify-between mt-[20px]">

                                <div>
                                    <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                </div>

                                <div>
                                <button
                                    className="w-[72px] h-[30px] text-[#FFFFFF] bg-[#000] text-[12px] rounded-[9px] text-center items-center hover:bg-transparent hover:border-[#000] hover:border hover:text-[#000] justify-center mr-[10px]"
                                    onClick={() =>
                                        setShowModal(false)
                                    }
                                    disabled={isLoading}
                                >
                                    Cancel
                                </button>
                                <button
                                    className="w-[72px] h-[30px] text-[#FFFFFF] bg-[#3682F7] text-[12px] rounded-[9px] text-center items-center hover:bg-transparent hover:border-[#3682F7] hover:border hover:text-[#3682F7] justify-center"
                                    type="submit"
                                    disabled={isLoading}

                                >
                                    {isLoading ? <Dots className='m-auto' size={7} color={'#fff'}></Dots> : 'Update'}
                                    
                                </button>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}