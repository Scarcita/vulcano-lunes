import React, { useEffect, useState } from "react";
import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";
import Image from "next/image";
import ImgContacted from '../../../../assets/img/contacted.svg'
import TooltipPast from "./tooltipPast";

export default function TablePast() {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);
  console.log(data);
  const headlist = ["Recommended Service on", "Car", "Service Advisor", "Created on", "Delivered on", ""];

  const getDatos = () => {
    setIsLoading(true);
    fetch("https://slogan.com.bo/vulcano/orders/withScheduledService/past")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
            var temp = [];
            Object.values(data.data).map((result) =>{
                temp.push(result);
            })

            setData(temp)
        } else {
          console.error(data.error);
        }
        setIsLoading(false);
      });
  };
  useEffect(() => {
    getDatos();
  }, []);

  return isLoading ? (
    <div className="flex justify-center items-center self-center mt-[30px]">
      <Spinner
        color="#3682F7"
        size={17}
        speed={1}
        animating={true}
        style={{ marginLeft: "auto", marginRight: "auto" }}
      />
    </div>
  ) : (
    <div>
      <Table headlist={headlist} data={data} />
      <TableResponsive headlist={headlist} data={data} />
    </div>
  );
}

const Table = (props) => {
  const { headlist, data } = props;

  return (
    <div className="hidden lg:block md:block mt-[20px]">
      <table className="w-full bg-[#fff] rounded-[20px] shadow-[25px]">
        <thead className="">
          <tr>
            {headlist.map((header) => (
              <th
                key={headlist.id}
                className="text-[14px] border-b font-medium text-start text-[#A5A5A5] py-2"
              >
                {header}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {data.map((row) => {

              if(row !== undefined){
                if (row !== null){
                  if(row.next_service_date !== null && row.next_service_date !== undefined){
                    var eventdate = row.next_service_date;
                    var splitdatatime = eventdate.split('T')
                    //var splitdate = eventdate.split('-');
                    console.log(splitdatatime);
                    var day = splitdatatime[0].split('-');
                    console.log(day);
                    var d = day[2];
                  }
                }
              }
              if(row !== undefined){
                if (row !== null){
                  if(row.next_service_date !== null && row.next_service_date !== undefined){
                    var days = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
                    var d = new Date(row.next_service_date);
                    var dayName = days[d.getDay()];
                  }
                }
              }

              if(row !== undefined){
                if (row !== null){
                  if(row.next_service_date !== null && row.next_service_date !== undefined){
                    var months = ['JAN', 'FEB ', 'MAR', 'APR', 'MAY', 'JUN', 'JUL','AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
                    var m = new Date(row.next_service_date);
                    var monthName = months[m.getMonth()];
                  }
                }
              }

            let spliteado = row.created.split("T");
            let created = spliteado[0];
            let createdHr = spliteado[1];
            let Hr = createdHr.split("+");
            let Hora =Hr[0]

            let spliteadoDate = row.delivery_date.split("T");
            let deliveryDate = spliteadoDate[0];
            let createdDeliveryDate = spliteado[1];
            let DeliveryHr = createdDeliveryDate.split("+");
            let DeliveryHora = DeliveryHr[0]

            let splitLastActivity = row.last_activity_date_time.split("T");
            let LastActivityDate = splitLastActivity[0];
            let LastActivityTime = spliteado[1];
            let LastActivityHr = LastActivityTime.split("+");
            let LastActivityHora = LastActivityHr[0]
            


            return (
              <tr key={row.id}>
                <td >
                  <div className="flex flex-row">
                    <div className=" w-[44px] h-[46px] bg-[#FFFF] rounded-[7px] justify-center shadow-md justify-center items-center">
                      <div className="flex bg-[#FF0000] w-full h-[13px] rounded-t-[7px]  justify-center ">
                          <div className="flex-col flex justify-center items-center">
                              <p className="text-[#FFFF] text-[10px]">
                                  {dayName}
                              </p>
                          </div>
                      </div>
                      <div className="flex-col flex justify-center self-center items-center">
                          <p className="font-bold text-[14px] text-center">
                              {/* {d} */}
                              14
                          </p>
                          <p className="font-bold text-[10px] text-center">
                              {monthName}
                          </p>
                      </div>
                    </div>
                    <div className="ml-[5px] self-center items-center">
                      <div className=" rounded-[8px] bg-[#71AD46] w-[85px] h-[25px] self-center items-center px-1 text-center flex flex-row">
                          <Image
                            src={ImgContacted}
                            alt="brand"
                            layout="fixed"
                            width={15}
                            height={15}/>
                          <div className="text-[10px] text-[#fff] text-center">
                              {row.has_been_contacted}
                          </div>
                      </div>
                      <p className="text-[14px] text-[#000] self-center font-semibold text-center">
                        {LastActivityDate} {LastActivityHora}
                      </p>
                    </div>
                  </div>
                </td>
                <td className="flex flex-row">
                  <div className="w-[48px] h-[48px] bg-[#F6F6FA] rounded-full m-[5px] self-center text-center items-center justify-center">
                    <Image
                     src={row.brand_logo}
                     alt="brand"
                     layout="fixed"
                     width={30}
                     height={30}/>

                  </div>
                  <div className="self-center items-center ml-[5px]">
                      <div className=" rounded-[13px] bg-[#3682F7] w-[70px] h-[20px] self-center items-center text-center">
                          <div className="text-[10px] text-[#fff] self-center items-center text-center pt-1">
                              {row.status}
                          </div>
                      </div>
                      <p className="text-[13px] text-[#000000] font-bold">
                          {row.brand}
                      </p>
                      <p className="text-[11px] text-[#000000]">
                          {row.model} • {row.version} • {row.color}
                      </p>
                  </div>
                </td>
                <td>
                  <p className="text-[14px] text-[#000] self-center font-semibold">
                  {row.service_advisor}
                  </p>
                </td>
                <td>
                  <p className="text-[14px] text-[#000] self-center font-semibold">
                    {created} {Hora}
                  </p>
                </td>
                <td>
                  <p className="text-[14px] text-[#000] self-center font-semibold">
                   {deliveryDate} {DeliveryHora}
                  </p>
                </td>
                <td className="text-center">
                  <TooltipPast/>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

const TableResponsive = (props) => {
  const { data } = props;

  return (
    <div className="grid grid-cols-12 mt-[20px] ">
      <div className="col-span-12 md:col-span-12 lg:col-span-12 rounded-[10px] md:hidden">
        {data.map((row) => {
          //let spliteado = row.created.split("T");
          //let created = spliteado[0];
          ///let spliteado1 = row.created.split("T");
          //let delivery_promise= spliteado1[0];

          return (
            <div
              key={row.id}
              className="grid grid-cols-12 bg-[#fff] rounded-[15px] shadow-md py-3 px-3 mb-[15px]"
            >
              {/* <div className="col-span-11 md:col-span-12">
                <div className="grid grid-cols-12">
                  <div className="col-span-12 md:col-span-12 flex justify-between">
                    <div className="w-[44px] h-[44px] bg-[#fff] border-2 border-solid border-[#3682F7] rounded-full m-[5px] self-center text-center items-center justify-center">
                      <p className="text-[12px] text-[#3682F7]">
                        OT
                      </p>
                      <p className="font-semibold text-[14px] text-[#3682F7]">
                        {row.id}
                      </p>
                    </div>
                    <div className="self-center items-center text-center text-[14px] text-[#000000] font-normal whitespace-nowrap ">
                      <p>
                        {created} 
                      </p>
                      <p>
                      {delivery_promise}
                      </p>
                    </div>
                      {row.status === "EN PAUSA" ?
                      <div className="w-[68px] h-[20px] bg-[#D3D3D3] rounded-[7px]">
                        <p className="text-[12px] text-[#fff] text-center self-center">
                          {row.status}
                        </p>
                      </div>
                      : <></>
                      
                    }
                    {row.status === "FINALIZADO" ?
                      <div className="w-[68px] h-[20px] bg-[#3682F7] rounded-[7px]">
                        <p className="text-[12px] text-[#fff] text-center self-center">
                          {row.status}
                        </p>
                      </div>
                      : <></>
                      
                    }
                    {row.status === "ABIERTO" ?
                      <div className="w-[68px] h-[20px] bg-[#0099ff] rounded-[7px]">
                        <p className="text-[12px] text-[#fff] text-center self-center">
                          {row.status}
                        </p>
                      </div>
                      : <></>
                      
                    }
                    {row.status === "CERRADO" ?
                      <div className="w-[68px] h-[20px] bg-[#ff3300] rounded-[7px]">
                        <p className="text-[12px] text-[#fff] text-center self-center">
                          {row.status}
                        </p>
                      </div>
                      : <></>
                      
                    }
                    {row.status === "EN CURSO" ?
                      <div className="w-[68px] h-[20px] bg-[#009900] rounded-[7px]">
                        <p className="text-[12px] text-[#fff] text-center self-center">
                          {row.status}
                        </p>
                      </div>
                      : <></>
                      
                    }
                    {row.status === "ENTREGADO" ?
                      <div className="w-[68px] h-[20px] bg-[#ff9900] rounded-[7px]">
                        <p className="text-[12px] text-[#fff] text-center self-center">
                          {row.status}
                        </p>
                      </div>
                      : <></>
                    }
                  </div>
                  <div className="col-span-12 md:col-span-12 mt-[5px]">
                    <div className="flex">
                      <div className="w-[48px] h-[48px] bg-[#D3D3D3] rounded-full self-center text-center pt-[7px] items-center ">
                        <Image
                            className="items-center self-center text-center"
                            src={row.car.cars_models_version.cars_model.catalogues_record.additional_info}
                            layout="fixed"
                            alt="media"
                            width={30}
                            height={30}
                        />
                      </div>
                      <div className="ml-[8px] self-center">
                        <p className="font-semibold text-[14px]">
                          {row.car.cars_models_version.cars_model.catalogues_record.name}
                        </p>
                        <p className="text-[12px] font-light">
                        {row.car.plate}
                          <span className="ml-[5px]">
                          {row.car.vin}
                          </span>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-span-12 md:col-span-12 flex justify-between mt-[5px]">
                    <div>
                      <p className="text-[14px] text-[#000] self-center">
                        {row.contact_name}
                      </p>
                    </div>
                    <div>
                      <p className="text-[14px] text-[#000] self-center">
                        {delivery_promise}
                      </p>
                    </div>
                  </div>
                </div>
              </div> */}
              <div className="col-span-1 md:col-span-12 ">
                {/* <TooltipOrders/> */}
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};
