import React, { useState } from "react";
import Tippy from "@tippyjs/react";
import "tippy.js/dist/tippy.css";
import "tippy.js/animations/scale.css";
import "tippy.js/themes/light.css";

function ModalDelete(props) {
  const { showDots, row } = props;
  const [showModal, setShowModal] = useState(false);
  return (
    <>
      <form className="flex">
        <div>
          <Tippy
            trigger="click"
            placement={"left"}
            animation="scale-extreme"
            theme="light"
            interactive={true}
            content={
              <Tippy>
                <>
                  <button
                    type="button"
                    className="pt-1 pb-1 items-center flex"
                    onClick={() => setShowModal(true)}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth="1.5"
                      stroke="currentColor"
                      className="w-6 h-6"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M19.5 14.25v-2.625a3.375 3.375 0 00-3.375-3.375h-1.5A1.125 1.125 0 0113.5 7.125v-1.5a3.375 3.375 0 00-3.375-3.375H8.25m5.231 13.481L15 17.25m-4.5-15H5.625c-.621 0-1.125.504-1.125 1.125v16.5c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125V11.25a9 9 0 00-9-9zm3.75 11.625a2.625 2.625 0 11-5.25 0 2.625 2.625 0 015.25 0z"
                      />
                    </svg>

                    <p>Ver detalle</p>
                  </button>
                </>
              </Tippy>
            }
          >
            <button type="button">
              <svg
                width="8"
                height="28"
                viewBox="0 0 8 28"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                className="w-[8px] h-[24px] "
              >
                <path
                  d="M4 9.93548C6.21111 9.93548 8 11.7532 8 14C8 16.2468 6.21111 18.0645 4 18.0645C1.78889 18.0645 0 16.2468 0 14C0 11.7532 1.78889 9.93548 4 9.93548ZM0 4.06452C0 6.31129 1.78889 8.12903 4 8.12903C6.21111 8.12903 8 6.31129 8 4.06452C8 1.81774 6.21111 0 4 0C1.78889 0 0 1.81774 0 4.06452ZM0 23.9355C0 26.1823 1.78889 28 4 28C6.21111 28 8 26.1823 8 23.9355C8 21.6887 6.21111 19.871 4 19.871C1.78889 19.871 0 21.6887 0 23.9355Z"
                  fill="#D9D9D9"
                />
              </svg>
            </button>
          </Tippy>
        </div>

        {showModal ? (
          <>
            <div className="fixed inset-0 z-10 overflow-y-auto">
              <div
                className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                onClick={() => setShowModal(false)}
              />

              <div className="flex items-center min-h-screen px-4 py-8">
                <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-xl shadow-2xl justify-center items-center ">
                  <div>
                    <div>
                      <div className="grid grid-cols-12 gap-4 flex">
                        <p className="flex col-span-11 md:col-span-11 lg:col-span-11 font-bold text-[20px] text-[#3682F7]">
                          Detalles de registro
                        </p>
                        <div className="flex col-span-1 md:col-span-1 lg:col-span-1">
                          <button onClick={() => setShowModal(false)}>
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              fill="none"
                              viewBox="0 0 24 24"
                              strokeWidth="1.9"
                              stroke="currentColor"
                              className="w-6 h-6 text-[#ff0000]"
                            >
                              <path
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                d="M6 18L18 6M6 6l12 12"
                              />
                            </svg>
                          </button>
                        </div>
                      </div>
                      <div className="grid grid-cols-12 gap-4 flex ">
                        <div className="col-span-6 md:col-span-6 lg:col-span-6">
                          <div>
                            <p className="font-light text-[#A5A5A5] text-[20px] mt-[5px]">
                              Placa
                            </p>
                            <p className="text-black text-[16px] ">
                              {row.car.plate}
                            </p>
                          </div>
                          <div>
                            <div>
                              <p className="font-light text-[#A5A5A5] text-[20px] mt-[5px]">
                                Marca
                              </p>
                              <p className="text-black">
                                {row.car.cars_models_version.cars_model.name}
                              </p>
                            </div>
                          </div>
                          <div className="flex grid grid-cols-12">
                            <div className="col-span-12 md:col-span-6 lg:col-span-6">
                              <p className="font-light text-[#A5A5A5] text-[20px] mt-[5px]">
                                Version
                              </p>
                              <p className="text-black">{row.car.version_id}</p>
                            </div>
                            <div className="col-span-12 md:col-span-6 lg:col-span-6">
                              <p className="font-light text-[#A5A5A5] text-[20px] mt-[5px]">
                                Color
                              </p>
                              <p className="text-black">{row.car.color_id}</p>
                            </div>
                          </div>
                        </div>
                        <div className="col-span-6 md:col-span-6 lg:col-span-6">
                          <div>
                            <p className="font-light text-[#A5A5A5] text-[20px] mt-[5px]">
                              VIN
                            </p>
                            <p className="text-black">{row.car.vin}</p>
                          </div>
                          <div>
                            <p className="font-light text-[#A5A5A5] text-[20px] mt-[5px]">
                              Modelo
                            </p>
                            <p className="text-black">
                              {row.car.cars_models_version.name}
                            </p>
                          </div>
                          <div className="flex grid grid-cols-12">
                            <div className="col-span-12 md:col-span-6 lg:col-span-6">
                              <p className="font-light text-[#A5A5A5] text-[20px] mt-[5px]">
                                Año
                              </p>
                              <p className="text-black">{row.car.year}</p>
                            </div>
                            <div className="col-span-12 md:col-span-6 lg:col-span-6">
                              <p className="font-light text-[#A5A5A5] text-[20px] mt-[5px]">
                                Transmisión
                              </p>
                              <p className="text-black">
                                {row.car.transmission}
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="flex grid grid-cols-12">
                      <div className="col-span-12 md:col-span-6 lg:col-span-6">
                        <p className="font-bold text-[20px] text-[#3682F7] mt-[10px]">
                          Cliente
                        </p>
                        <div className="grid grid-cols-12 gap-4 flex">
                          <div className="col-span-12 md:col-span-12 lg:col-span-12">
                            <p className="font-light text-[#A5A5A5] text-[20px] mt-[5px]">
                              Nombre/Razón Social
                            </p>
                            <p className="text-black">
                              {row.client.invoice_name}
                            </p>
                            <p className="font-light text-[#A5A5A5] text-[20px] mt-[5px]">
                              E-Mail
                            </p>
                            <p className="text-black">{row.client.email}</p>
                            <p className="font-light text-[#A5A5A5] text-[20px] mt-[5px]">
                              Teléfono
                            </p>
                            <p className="text-black">{row.client.phone}</p>
                          </div>
                        </div>
                      </div>
                      <div className="col-span-12 md:col-span-6 lg:col-span-6">
                        <p className="font-bold text-[20px] text-[#3682F7] mt-[10px]">
                          Detalles de la facturación
                        </p>
                        <div className="col-span-6 md:col-span-6 lg:col-span-6">
                          <p className="font-light text-[#A5A5A5] text-[20px] mt-[5px]">
                            Nombre Factura
                          </p>
                          <p className="text-black">
                            {row.client.invoice_name}
                          </p>

                          <p className="font-light text-[#A5A5A5] text-[20px] mt-[5px]">
                            CI/NIT
                          </p>
                          <p className="text-black">
                            {row.client.invoice_cinit}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        ) : null}
      </form>
    </>
  );
}
export default ModalDelete;
