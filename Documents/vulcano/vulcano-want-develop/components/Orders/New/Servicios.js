import React, { useState, useEffect, useRef } from "react";

import Select from "react-select";

function Servicios(props) {
  const selectInputRef = useRef();

  const { servicios, setServicios } = props;
  const [servicesInput, setServicesInput] = useState("");

  const [servicesList, setServicesList] = useState(null);
  const [loadingData, setLoadingData] = useState(false);

  const addObservation = () => {
    if (servicios === null) {
      setServicios([
        {
          id: 1,
          name: servicesInput,
        },
      ]);
    }
    if (servicios !== null) {
      setServicios([
        ...servicios,
        {
          id: servicios.length + 1,
          name: servicesInput,
        },
      ]);
    }
    selectInputRef.current.clearValue();
  };

  const fetchServices = async () => {
    setLoadingData(true);
    const response = await fetch("http://slogan.com.bo/vulcano/services/all");
    const data = await response.json();
    if (data.status === true) {
      let services = [];
      data.data.map((service) => {
        services.push({
          value: service.id,
          label: service.name,
        });
      });
      setServicesList(services);
    } else {
      console.log("Error al obtener los servicios: ", data);
    }
    setLoadingData(false);
  };

  useEffect(() => {
    fetchServices();
  }, []);

  return (
    // CONTENEDOR PRINCIPAL
    <div
      className="
    w-full h-full flex flex-col justify-start items-center
    "
    >
      {/* CABECERA */}
      <div
        className="
      w-full flex flex-col justify-center items-center
      {/*DESKTOP*/}
      lg:flex-row lg:justify-start lg:items-center
      "
      >
        {/* TITULO */}
        <div
          className="
            w-full flex flex-row justify-start items-center my-2
            {/*DESKTOP*/}
            lg:w-1/2
            "
        >
          <p
            className="
          text-lg font-bold
        "
          >
            Servicios
          </p>
        </div>
      </div>

      {/* CUERPO */}
      <div
        className="
        w-full h-[400px] flex flex-col justify-start items-center bg-[#fff] rounded-lg shadow-lg rounded-lg
        {/*DESKTOP*/}
        lg:h-full
        "
      >
        {/* SELECCIONAR SERVICIO */}
        <div
          className="
          w-full flex flex-row justify-center items-center p-4
        "
        >
          <div
            className="
            w-full h-8 flex flex-row justify-center items-center bg-[#F6F6FA] rounded-lg shadow-inner rounded-lg outline-none
            "
          >
            <Select
              className="w-full"
              ref={selectInputRef}
              options={servicesList}
              onChange={(value) => {
                value !== null
                  ? setServicesInput(value.label)
                  : setServicesInput("");
              }}
              placeholder="Seleccionar servicio"
              isClearable={true}
              isDisabled={loadingData}
              isLoading={loadingData}
              required
              noOptionsMessage={() => "No hay servicios disponibles"}
              components={{
                IndicatorSeparator: () => null,
              }}
              styles={{
                control: (provided, state) => ({
                  ...provided,
                  height: "32px",
                  border: "none",
                  backgroundColor: "transparent",
                  boxShadow: "none",
                  "&:hover": {
                    border: "none",
                  },
                }),
                placeholder: (provided, state) => ({
                  ...provided,
                  color: "#8e8e8e",
                }),
                menu: (provided, state) => ({
                  ...provided,
                  borderRadius: "10px",
                  marginTop: "0px",
                  boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.1)",
                  border: "0px",
                }),
                menuList: (provided, state) => ({
                  ...provided,
                  padding: "0px",
                }),
                option: (provided, state) => ({
                  ...provided,
                  display: "flex",
                  justifyContent: "flex-start",
                  alignItems: "center",
                  height: "32px",
                  padding: "2px 10px",
                  color: "#000",
                  borderRadius: "10px",
                  backgroundColor: "#F6F6FA",
                  "&:hover": {
                    backgroundColor: "#3682F7",
                    color: "#fff",
                  },
                }),
              }}
            />
          </div>

          <button
            className={`
                w-10 h-8 flex flex-row justify-center items-center bg-[#3682F7] ml-2 text-[#fff] font-Dosis text-lg shadow-lg rounded-lg outline-none
                {/*DESKTOP*/}
                lg:w-24 lg:text-xl
                ${servicesInput === "" ? "bg-[#E1E1E1] cursor-not-allowed" : ""}
                `}
            disabled={servicesInput === "" ? true : false}
            onClick={addObservation}
          >
            {window.innerWidth > 1024 ? (
              "Agregar"
            ) : (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-6 h-6 text-white text-center"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M19.5 5.25l-7.5 7.5-7.5-7.5m15 6l-7.5 7.5-7.5-7.5"
                />
              </svg>
            )}
          </button>
        </div>

        {/* SERVICIOS */}
        <div
          className="
        w-full h-full flex flex-col justify-start items-center overflow-y-scroll
        "
        >
          {servicios !== null ? (
            servicios.map((servicio) => (
              <div
                key={servicio.id}
                className="
        w-full flex flex-row justify-start items-center px-4 border-b-2 border-[#E1E1E1] py-2
        "
              >
                <p
                  className="
          text-lg font-base
          "
                >
                  {servicio.name}
                </p>
              </div>
            ))
          ) : (
            <div
              className="
                    w-full h-full flex flex-row justify-center items-center
                    "
            >
              <p
                className="
                        text-lg font-base
                        "
              >
                No hay servicios
              </p>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
export default Servicios;
