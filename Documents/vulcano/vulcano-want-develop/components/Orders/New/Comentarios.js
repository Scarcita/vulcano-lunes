import React, { useEffect, useState } from "react";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";

function Comentarios(props) {
  const { comentarios, setComentarios } = props;

  return (
    // CONTENEDOR PRINCIPAL
    <div
      className="
    w-full h-full flex flex-col justify-around items-center
    "
    >
      {/* CABECERA */}
      <div
        className="
        w-full h-14 flex flex-row justify-start items-center
      "
      >
        <p
          className="
          text-lg font-bold
        "
        >
          Comentarios
        </p>
      </div>

      {/* CUERPO */}
      <div
        className="
        w-full h-52 flex flex-col justify-center items-center p-4 bg-[#fff] rounded-lg shadow-lg rounded-lg
        {/*DESKTOP*/}
        lg:h-full
      "
      >
        <textarea
          className="
            w-full h-full text-md text-left font-normal border-[#D3D3D3] border-2 rounded-lg p-2 outline-none resize-none
            "
          placeholder="Escribe aquí tus comentarios"
          type="text"
          value={comentarios}
          onChange={(e) => setComentarios(e.target.value)}
        />
      </div>
    </div>
  );
}
export default Comentarios;
