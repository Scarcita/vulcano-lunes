import React from "react";
import Chart from "chart.js";

export default function BarChartCitasYear() {
    React.useEffect(() => {
        let config = {
          type: "bar",
          data: {
            labels: [
              "Jan",
              "Feb",
              "Mar",
              "Apr",
              "May",
              "Jun",
              "Jul",
              "Ago",
              "Sep",
              "Oct",
              "Nov",
              "Dec",
    
            ],
            datasets: [
              {
                label: new Date().getFullYear(),
                backgroundColor: "#3682F7",
                //borderColor: "#4a5568",
                data: [30, 78, 56, 34, 50, 45, 13, 20, 48, 32, 55, 80],
                fill: false,
                barThickness: 12,
              },
              {
                label: new Date().getFullYear() - 1,
                fill: false,
                backgroundColor: "#318",
                //borderColor: "#3182ce",
                data: [27, 68, 80, 74, 10, 4, 77, 56, 34, 50, 45, 13],
                barThickness: 12,
              },
            ],
          },
          options: {
            maintainAspectRatio: false,
            responsive: true,
            title: {
              display: false,
              text: "Orders Chart",
            },
            tooltips: {
              mode: "index",
              intersect: false,
            },
            hover: {
              mode: "nearest",
              intersect: true,
            },
            legend: {
              labels: {
                fontColor: "rgba(0,0,0,.4)",
                color: "#3682F7"
              },
              align: "end",
              position: "bottom",
            },
            scales: {
              xAxes: [
                {
                  display: false,
                  scaleLabel: {
                    display: true,
                    labelString: "Month",
                  },
                  gridLines: {
                    borderDash: [2],
                    borderDashOffset: [2],
                    color: "rgba(33, 37, 41, 0.3)",
                    zeroLineColor: "rgba(33, 37, 41, 0.3)",
                    zeroLineBorderDash: [2],
                    zeroLineBorderDashOffset: [2],
                  },
                },
              ],
              yAxes: [
                {
                  display: true,
                  scaleLabel: {
                    display: false,
                    labelString: "Value",
                  },
                  gridLines: {
                    borderDash: [2],
                    drawBorder: false,
                    borderDashOffset: [2],
                    color: "rgba(33, 37, 41, 0.2)",
                    zeroLineColor: "rgba(33, 37, 41, 0.15)",
                    zeroLineBorderDash: [2],
                    zeroLineBorderDashOffset: [2],
                  },
                },
              ],
            },
          },
        };
    
        let ctx = document.getElementById("bar-chart").getContext("2d");
        window.myBar = new Chart(ctx, config);
      }, []);
      
      return (
        <>
          <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-md rounded-md pt-[15px]">
              <div className="relative h-[260px]">
                <canvas id="bar-chart"></canvas>
              </div>
          </div>
        </>
      );
    }
    
    
    