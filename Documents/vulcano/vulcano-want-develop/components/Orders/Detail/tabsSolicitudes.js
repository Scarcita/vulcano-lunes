import React from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import TablaServiciosRequeridos from "./TablaServiciosRequeridos";
import TableSolicitudes from "./TableSolicitudes";


export default function TabsSolicitudes() {

    return(
            <div>
                <Tabs>
                 
                        <TabList className="grid grid-cols-12 focus:outline-none  focus:text-[#3682F7] ">
                            <Tab className="col-span-2 md:col-span-2 lg:col-span-2 self-center text-[12px] focus:text-[#3682F7] rounded-tl-lg px-2 py-2 text-ellipsis">
                            {" "}
                                Reptos. Solicitados
                            </Tab>
                            <Tab className="col-span-2 md:col-span-2 lg:col-span-2 self-center text-[12px] focus:text-[#3682F7] rounded-tl-lg px-2 py-2 text-ellipsis">
                                Trab. Externos
                            </Tab>
                            <Tab className="col-span-3 md:col-span-2 lg:col-span-2 self-center text-[12px] focus:text-[#3682F7] rounded-tl-lg px-2 py-2 text-ellipsis">
                                Registro de Trabajo
                            </Tab>
                            <Tab className="col-span-3 md:col-span-2 lg:col-span-3 self-center text-[12px] focus:text-[#3682F7] rounded-tl-lg px-2 py-2 text-ellipsis">
                                Obs. / Recomends.
                            </Tab>
                            <div className="col-span-2 md:col-span-2 lg:col-span-3 text-end justify-end self-center items-center">
                                <p className="lg:w-[62px] md:w-[62px] h-[25px] border-[1px] border-[#3682F7] rounded-[7px] text-[#3682F7] hover:bg-[#3682F7] hover:text-[#FFFF] text-[14px] text-center"
                                > 
                                Añadir
                                </p>
                            </div>
                        </TabList>
                        


                    <TabPanel>
                        <div className="grid grid-cols-12">
                            <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                                <TableSolicitudes/>
                            </div>
                        </div>
                    </TabPanel>

                    <TabPanel>
                        <div className="grid grid-cols-12">
                            <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                                HOLA
                            </div>
                        </div>
                    </TabPanel>

                    <TabPanel>
                        <div className="grid grid-cols-12">
                            <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                                HOLA
                            </div>
                        </div>
                    </TabPanel>

                    <TabPanel>
                        <div className="grid grid-cols-12">
                            <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                                HOLA
                            </div>
                        </div>
                    </TabPanel>

                </Tabs>
            </div>
    )
}