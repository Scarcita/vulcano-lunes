import React, { useState } from "react";
import Tippy from '@tippyjs/react'
import 'tippy.js/dist/tippy.css'
import 'tippy.js/animations/scale.css'
import 'tippy.js/themes/light.css';

function ModalDeleteServicesRequire(props) {
    const {handleDelete, setListBody, row} = props;
    console.log("setListBody");
    console.log(setListBody);
    const [showModal, setShowModal] = useState(false);
    return (
        <>
            <form className='flex'>
                <div>
                    <Tippy
                        trigger='click'
                        placement={'left'}
                        animation='scale-extreme'
                        theme='light'
                        interactive={true}
                        content={
                            <Tippy>
                                <>
                                    <button
                                        type="button"
                                        className="pt-1 pb-1 items-center flex"
                                        onClick={() => setShowModal(true)}
                                    >
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            strokeWidth="1.5"
                                            stroke="currentColor"
                                            className="w-6 h-6 text-[#FF0000]"
                                        >
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                                        </svg>
                                        <p className="text-[14px] hover:text-[#FF0000]">Eliminar</p>
                                    </button>
                                </>
                            </Tippy>
                        }>
                        <button type='button' className="flex justify-center items-center w-[20px] h-[20px] border-[1px] hover:bg-red-600 border-red-600 rounded-[6px] flex justify-center self-center items-center text-center ml-4">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor"
                                className="w-5 h-5 text-red-600 hover:text-[#FFFF]  self-center text-center">
                                <path strokeLinecap="round" strokeLinejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                            </svg>
                        </button>
                    </Tippy>
                </div>

                {showModal ? (
                    <>
                        <div className="fixed inset-0 z-10 overflow-y-auto">
                            <div
                                className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                                onClick={() => setShowModal(false)}
                            />
                            <div className="flex items-center min-h-screen px-4 py-8">
                                <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg justify-center items-center flex flex-col ">
                                    <div className="border-[#FF0000] w-[85px] h-[85px] flex justify-center items-center border-2 rounded-full mt-[30px]">
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            strokeWidth="1.5"
                                            stroke="currentColor"
                                            className="text-[#FF0000] w-[54px]  h-[6 54px] "
                                        >
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                                        </svg>
                                    </div>
                                    <div>
                                        <h1 className="text-[32px] mt-[30px]">
                                            ¿Esta seguro?
                                        </h1>
                                    </div>

                                    <div className="flex flex-col justify-center items-center mt-[22px] ">
                                        <p className="text-[18px] font-light">
                                            Una vez eliminado no se podra recuperar los datos.
                                        </p>
                                    </div>
                                    <div className="flex flex-row justify-between mt-[45px]">
                                        <div>
                                            <button
                                                className="w-[85px] h-[45px] border-[1px] border-[#3682F7] rounded-[15px] text-[#3682F7] hover:bg-[#3682F7] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                onClick={() =>
                                                    setShowModal(false)
                                                }
                                            >
                                                Cancelar
                                            </button>
                                            <button
                                                className="w-[85px] h-[45px] border-[1px] bg-[#FF0000] rounded-[15px] text-[#FFFF] hover:border-[#FF0000] hover:bg-[#FFFF] hover:text-[#FF0000] text-[14px] mt-[3px]"
                                                onClick={() => {
                                                    // setShowModal(false)
                                                    handleDelete (row.id)
                                                }}
                                            >
                                                Eliminar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                ) : null}
            </form>
        </>
    )
}
export default ModalDeleteServicesRequire;
