import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";

export default function ModalEditServiceRequire(props) {

    const [showModal, setShowModal] = useState(false);

    const { id, body } = props;

    const [isLoading, setIsLoading] = useState(false)
    const [nameForm, setNameForm] = useState()
    const [precioRefForm, setPrecioRefForm] = useState()
    const [precioHrForm, setPrecioHrForm] = useState()
    const [hrsHombreForm, setHrsHombreForm] = useState('')
    const [clienteForm, setClienteForm] = useState('')
    const [empresaForm, setEmpresaForm] = useState('')
    const [garantiaForm, setGarantiaForm] = useState('')
    const [mecanicoForm, setMecanicoForm] = useState('')

    const validationSchema = Yup.object().shape({
        name: Yup.string()
        .required('name is required'),

        precioref: Yup.string()
        .required("precioref is required"),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

        precioHr: Yup.string()
        .required('precioHr is required'),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

        hrsHombre: Yup.string()
        .required('hrsHombre is required'),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

        cliente: Yup.string()
        .required('cliente is required'),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

        empresa: Yup.string()
        .required('empresa is required'),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

        garantia: Yup.string()
        .required('garantia is required'),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

        mecanico: Yup.string()
        .required('mecanico is required'),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

        });

    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        console.log('onSubmit data clients:');
        console.log(data);
        actualizarDatos(data);
    }


    return (
        <>
            <div>
            <button
                    className="w-[20px] h-[20px] border-[1px] border-[#3682F7] rounded-[6px] hover:bg-[#3682F7] text-[#3682F7] hover:text-white self-center flex justify-center items-center text-center items-center"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    <svg xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        strokeWidth="2"
                        stroke="currentColor"
                        className="w-5 h-5 self-center text-center">
                        <path strokeLinecap="round" strokeLinejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10"
                        />
                    </svg>

                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                <div>
                                    <h4 className="text-lg font-medium text-gray-800">
                                        Edit
                                    </h4>
                                </div>
                                <form onSubmit={handleSubmit(onSubmit)}>

                                    <div className="mt-[20px] grid grid-cols-12 gap-4">

                                        <div className='col-span-12 md:col-span-12 lg:col-span-6 '>
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-left'>Name</p>
                                            <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                type="text"
                                                name="name"
                                                {...register('name')}
                                                value={nameForm}
                                                onChange={(e) => {
                                                    setNameForm(e.target.value)
                                                }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.name?.message}</div>
                                        </div>

                                        <div className='col-span-12 md:col-span-12 lg:col-span-6'>
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-left'>Precio ref.</p>
                                            <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                type="text"
                                                name="precioref"
                                                {...register('precioref')}
                                                value={precioRefForm}
                                                onChange={(e) => setPrecioRefForm(e.target.value)}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.precioref?.message}</div>
                                        </div>
                                        <div className='col-span-12 md:col-span-6 lg:col-span-6'>
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-left'>Precio hr.</p>
                                            <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px]"
                                                type="text"
                                                name="precioHr"
                                                {...register('precioHr')}
                                                value={precioHrForm}
                                                onChange={(e) => setPrecioHrForm(e.target.value)}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.precioHr?.message}</div>
                                        </div>
                                        <div className='col-span-12 md:col-span-6 lg:col-span-6'>
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-left'>Hrs. hombre</p>
                                            <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px]"
                                                type="text"
                                                name="hrsHombre"
                                                {...register('hrsHombre')}
                                                value={hrsHombreForm}
                                                onChange={(e) => setHrsHombreForm(e.target.value)}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.hrsHombre?.message}</div>
                                        </div>
                                        <div className="col-span-12 md:col-span-6 lg:col-span-6">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-left'>Cliente</p>
                                            <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                type="text"
                                                name="cliente"
                                                {...register('cliente')}
                                                value={clienteForm}
                                                onChange={(e) => setClienteForm(e.target.value)}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.cliente?.message}</div>
                                        </div>
                                        <div className="col-span-12 md:col-span-6 lg:col-span-6">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-left'>Empresa</p>
                                            <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                type="text"
                                                name="empresa"
                                                {...register('empresa')}
                                                value={empresaForm}
                                                onChange={(e) => setEmpresaForm(e.target.value)}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.empresa?.message}</div>
                                        </div>

                                        <div className="col-span-12 md:col-span-6 lg:col-span-6">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-left'>Garantía</p>
                                            <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                type="text"
                                                name="garantia"
                                                {...register('garantia')}
                                                value={garantiaForm}
                                                onChange={(e) => setGarantiaForm(e.target.value)}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.garantia?.message}</div>
                                        </div>
                                        <div className="col-span-12 md:col-span-6 lg:col-span-6">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-left'>Mecánico</p>
                                            <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                type="text"
                                                name="mecanico"
                                                {...register('mecanico')}
                                                value={mecanicoForm}
                                                onChange={(e) => setMecanicoForm(e.target.value)}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.mecanico?.message}</div>
                                        </div>
                                    </div>
                                    <div className="flex flex-row justify-between mt-[20px]">

                                        <div>
                                            <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                        </div>

                                        <div>
                                            <button
                                                className="w-[72px] h-[30px] text-[#FFFFFF] bg-[#000] text-[12px] rounded-[9px] text-center items-center hover:bg-transparent hover:border-[#000] hover:border hover:text-[#000] justify-center mr-[10px]"
                                                onClick={() =>
                                                    setShowModal(false)
                                                }
                                                disabled={isLoading}
                                            >
                                                Cancel
                                            </button>
                                            <button
                                                className="w-[72px] h-[30px] text-[#FFFFFF] bg-[#3682F7] text-[12px] rounded-[9px] text-center items-center hover:bg-transparent hover:border-[#3682F7] hover:border hover:text-[#3682F7] justify-center"
                                                type="submit"
                                                disabled={isLoading}
                                            >
                                                {isLoading ? <Dots className='m-auto' size={7} color={'#fff'}></Dots> : 'Update'}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}
