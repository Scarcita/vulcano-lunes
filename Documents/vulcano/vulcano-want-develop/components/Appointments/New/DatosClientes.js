import React, { useEffect, useState, useRef } from "react";
import Image from "next/image";

import { Bounce, Sentry } from "react-activity";
import "react-activity/dist/library.css";

import imagenOpciones from "../../../public/opciones.svg";

import CreatableSelect from "react-select/creatable";
import { setLocale } from "yup";

function DatosCliente(props) {
  const { setDatosCliente } = props;

  const selectRef = useRef();

  const [selectOptions, setSelectOptions] = useState("");

  const [isLocked, setIsLocked] = useState(false);
  const [userDataLoading, setUserDataLoading] = useState(false);
  const [selectLoading, setSelectLoading] = useState(false);
  const [newClientLoading, setNewClientLoading] = useState(false);

  const [sendingData, setSendingData] = useState(false);
  const [doneMessage, setDoneMessage] = useState(false);
  const [reload, setReload] = useState(false);

  const [selectedClientId, setSelectedClientId] = useState(0);
  const [selectId, setSelectId] = useState(null);

  const [clientName, setClientName] = useState("");
  const [telefono, setTelefono] = useState("");
  const [email, setEmail] = useState("");
  const [ciNit, setCiNit] = useState("");
  const [nombre, setNombre] = useState("");

  useEffect(() => {
    setDatosCliente({
      id: selectedClientId,
      nombre: nombre,
      telefono: telefono,
      email: email,
      ciNit: ciNit,
    });
  }, [selectedClientId, nombre, telefono, email, ciNit, setDatosCliente]);

  useEffect(() => {
    getDatos();
  }, [reload]);

  useEffect(() => {
    getDatos();
  }, []);

  const getDatos = () => {
    setSelectLoading(true);
    fetch("https://slogan.com.bo/vulcano/clients/allBy/name")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          const dataKeys = Object.keys(data.data);

          let indexedData = [];

          for (let i = 0; i < dataKeys.length; i++) {
            let index = dataKeys[i];
            if (data.data[index] !== null) {
              indexedData.push({
                value: index,
                label: data.data[index],
              });
            }
          }
          setSelectOptions(indexedData);
        } else {
          console.error("getDatos: " + data.error);
        }
        setSelectLoading(false);
      });
  };

  const handleOnSelect = (id) => {
    setIsLocked(true);
    setUserDataLoading(true);
    fetch("https://slogan.com.bo/vulcano/clients/getById/" + id)
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          setIsLocked(false);
          setClientName(data.data.name);
          setTelefono(data.data.phone);
          setEmail(data.data.email);
          setCiNit(data.data.invoice_cinit);
          setNombre(data.data.invoice_name);

          setSelectedClientId(data.data.id);

          let tempSelectOptions = [
            {
              value: data.data.id,
              label: data.data.name,
            },
          ];
          for (let i = 0; i < selectOptions.length; i++) {
            if (selectOptions[i].label !== data.data.name) {
              tempSelectOptions.push(selectOptions[i]);
            }
          }
          setSelectOptions(tempSelectOptions);
          setSelectId(0);

          setUserDataLoading(false);
          setIsLocked(true);
        } else {
          console.error("HandleOnSelectUser: " + data.error);
        }
      });
  };

  const handleOnCreate = (item) => {
    setNewClientLoading(true);
    setSelectedClientId(0);
    let tempSelectOptions = [
      {
        value: 0,
        label: item,
      },
    ];
    for (let i = 0; i < selectOptions.length; i++) {
      tempSelectOptions.push(selectOptions[i]);
    }
    setSelectOptions(tempSelectOptions);
    setSelectId(0);

    setClientName(item);
    setTimeout(() => {
      setNewClientLoading(false);
    }, 500);
  };

  const addNewClient = () => {
    if (
      // show red button
      clientName === "" ||
      telefono === "" ||
      email === "" ||
      ciNit === "" ||
      nombre === ""
    ) {
      return;
    }
    setSendingData(true);
    const data = new FormData();
    data.append("name", clientName);
    data.append("phone", telefono);
    data.append("email", email);
    data.append("invoice_cinit", ciNit);
    data.append("invoice_name", nombre);

    fetch("https://slogan.com.bo/vulcano/clients/addMobile", {
      method: "POST",
      body: data,
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          console.log("Cliente agregado con exito");
          console.log("New client: " + Object.entries(data.data));

          setReload(!reload);
          handleOnSelect(data.data.id);

          setSendingData(false);
          setDoneMessage(true);

          setTimeout(() => {
            setDoneMessage(false);
          }, 2000);
        } else {
          console.error("addNewClient: " + data.error);
        }
      });
  };

  const editClientData = () => {
    if (
      clientName === "" ||
      telefono === "" ||
      email === "" ||
      ciNit === "" ||
      nombre === ""
    ) {
      return;
    }
    setSendingData(true);
    const data = new FormData();
    data.append("name", clientName);
    data.append("phone", telefono);
    data.append("email", email);
    data.append("invoice_cinit", ciNit);
    data.append("invoice_name", nombre);

    fetch(
      "https://slogan.com.bo/vulcano/clients/editMobile/" + selectedClientId,
      {
        method: "POST",
        body: data,
      }
    )
      .then((response) => response.json())
      .then((data) => {
        console.log("editClientData Data: " + data.status);
        if (data.status) {
          console.log("Cliente editado con exito");
          console.log("Updated Client: " + Object.entries(data.data));

          handleOnSelect(selectedClientId);

          setSendingData(false);
          setDoneMessage(true);

          setTimeout(() => {
            setDoneMessage(false);
          }, 2000);
        } else {
          console.error("editClientData: " + data.error);
        }
      });
  };

  const formatResult = (item) => {
    return (
      <div className="flex flex-row ">
        <p className="ml-2">{item.name}</p>
      </div>
    );
  };

  return (
    // COTENERDOR PRINCIPAL
    <form
      className="
      w-full h-full flex flex-col justify-around items-center
      "
    >
      {/* CABECERA */}
      <div
        className="
        w-full h-14 flex flex-row justify-start items-center
      "
      >
        <p
          className="
          text-lg font-bold
        "
        >
          Cliente
        </p>
      </div>

      {/* CUERPO */}
      {sendingData ? (
        <div
          className="
          w-full h-full flex flex-col justify-center items-center px-4 bg-[#fff] rounded-lg shadow-lg rounded-lg
    "
        >
          <Sentry size={100} color="#3b82f6" />
        </div>
      ) : doneMessage ? (
        <div
          className="
          w-full h-full flex flex-col justify-center items-center px-4 bg-[#fff] rounded-lg shadow-lg rounded-lg p-4
    "
        >
          <div
            className="
      w-full flex flex-col justify-center items-center
      "
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth="1.5"
              stroke="#3b82f6"
              className="w-24 h-24"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M4.5 12.75l6 6 9-13.5"
              />
            </svg>

            <p
              className="
              text-[#3b82f6] font-bold text-3xl mt-4 mb-4
              "
            >
              ¡Listo!
            </p>
          </div>
        </div>
      ) : (
        <div
          className="
          w-full h-full flex flex-col justify-center items-center px-4 bg-[#fff] rounded-lg shadow-lg rounded-lg
      "
        >
          {/* BUSQUEDA USUARIO */}
          <div
            className="
            w-full h-fit flex flex-col justify-between items-center py-2
            {/*DESKTOP*/}
            lg:h-2/6
        "
          >
            {/* BARRA DE BUSQUEDA */}
            <div
              className="
          w-full
          "
            >
              <div
                className="
            w-full flex flex-row justify-start items-center
            "
              >
                <label
                  className="
              font-light text-[#A5A5A5] text-lg
              "
                >
                  Buscar cliente
                </label>
              </div>
              <div
                className="
                w-full h-12 flex flex-row justify-center items-center bg-[#D9D9D9] rounded-lg
            "
              >
                {userDataLoading == true || newClientLoading == true ? (
                  <Bounce
                    size={20}
                    color="#000"
                    speed={1}
                    animating={true}
                    style={{
                      marginBottom: "0.1rem",
                    }}
                  />
                ) : (
                  <CreatableSelect
                    className="w-full h-full"
                    ref={selectRef}
                    defaultValue={selectOptions[selectId]}
                    options={selectOptions}
                    onChange={(e) => {
                      e == null
                        ? setSelectedClientId(0)
                        : (setSelectedClientId(e.value),
                          handleOnSelect(e.value));
                    }}
                    onCreateOption={(e) => {
                      handleOnCreate(e);
                    }}
                    isLoading={selectLoading}
                    isDisabled={isLocked || selectLoading}
                    isClearable={true}
                    required
                    placeholder="Buscar cliente"
                    noOptionsMessage={() => "No hay resultados"}
                    components={{
                      DropdownIndicator: () => null,
                      IndicatorSeparator: () => null,
                    }}
                    styles={{
                      control: (provided, state) => ({
                        ...provided,
                        height: "100%",
                        border: "none",
                        borderRadius: "10px",
                        backgroundColor: "#D9D9D9",
                        boxShadow: "none",
                        "&:hover": {
                          border: "none",
                          boxShadow: "none",
                        },
                      }),
                      option: (provided, state) => ({
                        ...provided,
                        color: "#000",
                        backgroundColor: state.isSelected
                          ? "#3b82f6"
                          : state.isFocused
                          ? "#e2e8f0"
                          : "#fff",
                        "&:hover": {
                          backgroundColor: "#e2e8f0",
                        },
                      }),
                      menu: (provided, state) => ({
                        ...provided,
                        zIndex: "9999",
                      }),
                      singleValue: (provided, state) => ({
                        ...provided,
                        color: "#000",
                      }),
                      placeholder: (provided, state) => ({
                        ...provided,
                        color: "#000",
                      }),
                      input: (provided, state) => ({
                        ...provided,
                        color: "#000",
                      }),
                    }}
                  />
                )}
              </div>
            </div>
          </div>

          {/* DATOS USUARIO */}
          <div
            className="
            w-full h-full flex flex-col justify-evenly items-center
        "
          >
            {/* TITULO E ICONOS */}
            <div
              className="
        w-full h-12 flex flex-row justify-between items-center
        {/*DESKTOP*/}
        lg:h-4
        "
            >
              {/* TITULO */}
              <div className="w-full flex flex-row justify-start items-center">
                <p className="font-medium text-lg"> Detalles de registro</p>
              </div>
              {/* ICONOS */}
              <div className="flex flex-row justify-center items-center">
                <div
                  className="w-[19px] h-[18px] m-2"
                  onClick={() => {
                    setIsLocked(!isLocked);
                  }}
                >
                  {isLocked == false ? (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      fill="currentColor"
                      className="
                    text-[#D3D3D3] w-[19px] h-[18px] cursor-pointer
                    {/*HOVER*/}
                    hover:text-[#000]
                    "
                    >
                      <path d="M18 1.5c2.9 0 5.25 2.35 5.25 5.25v3.75a.75.75 0 01-1.5 0V6.75a3.75 3.75 0 10-7.5 0v3a3 3 0 013 3v6.75a3 3 0 01-3 3H3.75a3 3 0 01-3-3v-6.75a3 3 0 013-3h9v-3c0-2.9 2.35-5.25 5.25-5.25z" />
                    </svg>
                  ) : (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      fill="currentColor"
                      className="
                    text-[#D3D3D3] w-[19px] h-[18px] cursor-pointer
                    {/*HOVER*/}
                    hover:text-[#000] 
                    "
                    >
                      <path
                        fillRule="evenodd"
                        d="M12 1.5a5.25 5.25 0 00-5.25 5.25v3a3 3 0 00-3 3v6.75a3 3 0 003 3h10.5a3 3 0 003-3v-6.75a3 3 0 00-3-3v-3c0-2.9-2.35-5.25-5.25-5.25zm3.75 8.25v-3a3.75 3.75 0 10-7.5 0v3h7.5z"
                        clipRule="evenodd"
                      />
                    </svg>
                  )}
                </div>
                <div className="w-[5px] h-[19px] m-2">
                  <Image
                    src={imagenOpciones}
                    layout="responsive"
                    alt="imagenOpciones"
                  />
                </div>
              </div>
            </div>

            {/* TELEFONO E EMAIL */}
            <div
              className="
              w-full h-fit flex flex-col justify-around items-center
              {/*DESKTOP*/}
              lg:flex-row
              {/*TABLET*/}
              md:flex-row
          "
            >
              <div
                className="
            w-full flex flex-col justify-center items-center
            {/*DESKTOP*/}
            lg:mr-4
            {/*TABLET*/}
            md:w-1/2 
            "
              >
                <label
                  className="
                w-full self-start text-s m text-[#A5A5A5] font-light
                "
                >
                  Telefono
                </label>

                <div
                  className="
              w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border-2 rounded-lg
              "
                >
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input
                      className="
                    w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none
                    "
                      type="number"
                      disabled={isLocked}
                      placeholder="Introduce el telefono"
                      name="telefono"
                      // {...register("telefono")}
                      value={telefono}
                      onChange={(e) => {
                        setTelefono(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>

              <div
                className="
              w-full flex flex-col justify-center items-center
              {/*DESKTOP*/}
              lg:ml-4
              {/*TABLET*/}
              md:w-1/2 
            "
              >
                <label
                  className="
              w-full self-start text-s m text-[#A5A5A5] font-light
              "
                >
                  Email
                </label>

                <div className="w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border-2 rounded-lg">
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input
                      className="
                    w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none
                    "
                      name="email"
                      type="email"
                      placeholder="Introduce el email"
                      // {...register("email")}
                      disabled={isLocked}
                      value={email}
                      onChange={(e) => {
                        setEmail(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>
            </div>

            {/* TITULO */}
            <div
              className="
            w-full h-12 flex flex-row justify-start items-center
            {/*DESKTOP*/}
            lg:h-8
            "
            >
              {/* TITULO */}
              <div className="w-full flex flex-row justify-start items-center">
                <p className="font-medium text-lg"> Detalles de facturacion</p>
              </div>
            </div>

            {/* CI/NIT NOMBRE */}
            <div
              className="
              w-full h-fit flex flex-col justify-around items-center
              {/*DESKTOP*/}
              lg:flex-row
              {/*TABLET*/}
              md:flex-row
          "
            >
              <div
                className="
            w-full flex flex-col justify-center items-center
            {/*DESKTOP*/}
            lg:mr-4
            {/*TABLET*/}
            md:w-1/2 
            "
              >
                <label
                  className="
                w-full self-start text-s m text-[#A5A5A5] font-light
                "
                >
                  CI/NIT
                </label>

                <div
                  className="
              w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border-2 rounded-lg
              "
                >
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input
                      className="
                    w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none
                    "
                      type="number"
                      disabled={isLocked}
                      placeholder="Introduce el CI/NIT"
                      name="ciNit"
                      // {...register("ciNit")}
                      value={ciNit}
                      onChange={(e) => {
                        setCiNit(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>

              <div
                className="
              w-full flex flex-col justify-center items-center
              {/*DESKTOP*/}
              lg:ml-4
              {/*TABLET*/}
              md:w-1/2 
            "
              >
                <label
                  className="
              w-full self-start text-s m text-[#A5A5A5] font-light
              "
                >
                  Nombre/Razon Social
                </label>

                <div className="w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border-2 rounded-lg">
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input
                      className="
                    w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none
                    "
                      name="nombre"
                      type="text"
                      // {...register("nombre")}
                      disabled={isLocked}
                      placeholder="Introduce el nombre/razon social"
                      value={nombre}
                      onChange={(e) => {
                        setNombre(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>

          {/* BOTONES */}
          <div
            className="
            w-full h-14 flex flex-row justify-end items-center mb-2
          "
          >
            <button
              type="button"
              className={`
              w-16 h-6 border-2 rounded-lg text-[#fff] text-center text-[14px] font-Dosis ml-2 outline-none bg-[#000] border-[#000] hover:bg-[#fff] hover:text-[#000]
              `}
              onClick={() => {
                selectRef.current.clearValue();
                setSelectedClientId(0);
                setClientName("");
                setTelefono("");
                setEmail("");
                setCiNit("");
                setNombre("");
                setIsLocked(false);
              }}
            >
              Cancelar
            </button>

            <button
              type="button"
              className={`
              w-16 h-6 border-2 rounded-lg text-[#fff] text-center text-[14px] font-Dosis ml-2 outline-none ${
                isLocked
                  ? "text-[#D9D9D9]"
                  : "bg-[#3682F7] border-[#3682F7] hover:bg-[#fff] hover:text-[#3682F7]"
              }
              `}
              disabled={isLocked}
              onClick={() => {
                selectedClientId !== 0 ? editClientData() : addNewClient();
              }}
            >
              {selectedClientId !== 0 ? "Editar" : "Guardar"}
            </button>
          </div>
        </div>
      )}
    </form>
  );
}
export default DatosCliente;
