import React, { useState, useEffect } from "react";

function Observaciones(props) {
  const { observaciones, setObservaciones, setRazon } = props;
  const [observationsInput, setObservationsInput] = useState("");

  const [reason, setReason] = useState("");

  useEffect(() => {
    setRazon(reason);
  }, [reason, setRazon]);

  const addObservation = () => {
    if (observaciones === null) {
      setObservaciones([
        {
          id: 1,
          name: observationsInput,
        },
      ]);
    }
    if (observaciones !== null) {
      setObservaciones([
        ...observaciones,
        {
          id: observaciones.length + 1,
          name: observationsInput,
        },
      ]);
    }
    setObservationsInput("");
  };

  return (
    // CONTENEDOR PRINCIPAL
    <div
      className="
    w-full h-full flex flex-col justify-start items-center
    "
    >
      {/* CABECERA */}
      <div
        className="
      w-full flex flex-col justify-center items-center
      {/*DESKTOP*/}
      lg:flex-row lg:justify-start lg:items-center
      "
      >
        {/* TITULO */}
        <div
          className="
            w-full flex flex-row justify-start items-center my-2
            {/*DESKTOP*/}
            lg:w-1/2
            "
        >
          <p
            className="
          text-lg font-bold
        "
          >
            Observaciones
          </p>
        </div>
        {/* NUEVA OBSERVACION */}
        <div
          className="
            w-full flex flex-row justify-end items-center my-2
            {/*DESKTOP*/}
            lg:w-1/2
            "
        >
          <input
            className="
                w-full h-8 px-4 py-2 bg-[#fff] rounded-lg shadow-lg rounded-lg outline-none
                "
            type="text"
            value={reason}
            onChange={(e) => setReason(e.target.value)}
            placeholder="Razón de la cita"
          />
        </div>
      </div>

      {/* CUERPO */}
      <div
        className="
        w-full h-[400px] flex flex-col justify-start items-center bg-[#fff] rounded-lg shadow-lg rounded-lg
        {/*DESKTOP*/}
        lg:h-full
        "
      >
        {/* RAZON */}
        <div
          className="
          w-full flex flex-row justify-center items-center p-4
        "
        >
          <input
            className="
                w-full h-8 px-2 py-2 bg-[#F6F6FA] rounded-lg shadow-inner rounded-lg outline-none
                "
            type="text"
            value={observationsInput}
            onChange={(e) => setObservationsInput(e.target.value)}
            placeholder={`${
              observaciones == null
                ? "Agregar observación"
                : "Nueva observación"
            }`}
          />
          <button
            className={`
                w-10 h-8 flex flex-row justify-center items-center bg-[#3682F7] ml-2 text-[#fff] font-Dosis text-lg shadow-lg rounded-lg outline-none
                {/*DESKTOP*/}
                lg:w-24 lg:text-xl
                ${
                  observationsInput === ""
                    ? "bg-[#E1E1E1] cursor-not-allowed"
                    : ""
                }
                `}
            disabled={observationsInput === "" ? true : false}
            onClick={addObservation}
          >
            {window.innerWidth > 1024 ? (
              "Agregar"
            ) : (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-6 h-6 text-white text-center"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M19.5 5.25l-7.5 7.5-7.5-7.5m15 6l-7.5 7.5-7.5-7.5"
                />
              </svg>
            )}
          </button>
        </div>

        {/* OBSERVACIONES */}
        <div
          className="
        w-full h-full flex flex-col justify-start items-center overflow-y-scroll
        "
        >
          {observaciones !== null ? (
            observaciones.map((observacion) => (
              <div
                key={observacion.id}
                className="
        w-full flex flex-row justify-start items-center px-4 border-b-2 border-[#E1E1E1] py-2
        "
              >
                <p
                  className="
          text-lg font-base
          "
                >
                  {observacion.name}
                </p>
              </div>
            ))
          ) : (
            <div
              className="
                    w-full h-full flex flex-row justify-center items-center
                    "
            >
              <p
                className="
                        text-lg font-base
                        "
              >
                No hay observaciones
              </p>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
export default Observaciones;
