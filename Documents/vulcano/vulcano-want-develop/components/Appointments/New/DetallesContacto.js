import React, { useEffect, useState } from "react";

function DetallesContacto(props) {
  const { setDetallesContacto } = props;

  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");

  useEffect(() => {
    setDetallesContacto({
      nombre: name,
      telefono: phone,
    });
  }, [name, phone, setDetallesContacto]);

  return (
    // CONTENEDOR PRINCIPAL
    <div
      className="
    w-full h-full flex flex-col justify-around items-center
    "
    >
      {/* CABECERA */}
      <div
        className="
        w-full h-14 flex flex-row justify-start items-center
      "
      >
        <p
          className="
          text-lg font-bold
        "
        >
          Detalles de contacto
        </p>
      </div>

      {/* CUERPO */}
      <div
        className="
        w-full h-full flex flex-col justify-center items-center px-4 py-2 bg-[#fff] rounded-lg shadow-lg rounded-lg
      "
      >
        {/* NOMBRE Y TELEFONO */}
        <div
          className="
          w-full h-fit flex flex-col justify-around items-center
          {/*DESKTOP*/}
          lg:flex-row
          {/*TABLET*/}
          md:flex-row
          "
        >
          {/* NOMBRE */}
          <div
            className="
            w-full h-full flex flex-col justify-around items-center
            {/*DESKTOP*/}
            lg:mr-4
            {/*TABLET*/}
            md:w-1/2
            "
          >
            <label
              className="
                w-full self-start text-s m text-[#A5A5A5] font-light mb-2
                "
            >
              Nombre del responsable
            </label>

            <div
              className="
              w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border-2 rounded-lg
              "
            >
              <input
                className="
                    w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none
                    "
                type="text"
                value={name}
                placeholder="Ingrese el nombre"
                onChange={(e) => {
                  setName(e.target.value);
                }}
              />
            </div>
          </div>

          {/* TELEFONO */}
          <div
            className="
              w-full h-full flex flex-col justify-around items-center
              {/*DESKTOP*/}
              lg:ml-4
              {/*TABLET*/}
              md:w-1/2
            "
          >
            <label
              className="
              w-full self-start text-s m text-[#A5A5A5] font-light mb-2
              "
            >
              Telefono del responsable
            </label>

            <div className="w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border-2 rounded-lg">
              <input
                className="
                    w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none
                    "
                type="number"
                value={phone}
                placeholder="Ingrese el teléfono"
                onChange={(e) => {
                  setPhone(e.target.value);
                }}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default DetallesContacto;
