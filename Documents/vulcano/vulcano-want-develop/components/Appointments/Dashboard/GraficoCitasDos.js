import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import React, { useEffect, useState } from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export default function GraficoCitasDos() {
  const options = {
    responsive: true,
    plugins: {
      legend: {
        display: false,
        position: "top",
      },
      title: {
        display: false,
        text: 'Chart.js Bar Chart',
      },
    },
  };

  const [showDots, setShowDots] = useState(true);

  const [data, setData] = useState([]); ([]);
  const [dataGraph, setDataGraph] = useState(null);

  const getDatos = () => {

    fetch('https://slogan.com.bo/vulcano/orders/yearlyGraph')
      .then(response => response.json())
      .then(data => {
        if (data) {
          console.log(data.data);
          setData(data.data)
          console.log(data.data.labels);

          const labels = data.data.labels;

          setDataGraph(
            {
              labels,
              datasets: [
                {
                  label: 'Dataset 1',
                  data: data.data.data,
                  backgroundColor: '#3682F7',
                },
              ],
            }
          );
        } else {
          console.error(data.error)
        }
      })
      .then(setShowDots(false))
  }

  useEffect(() => {
    getDatos();
  }, []);

  return (
    showDots ?
      <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
        <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
      </div>
      :
      <>
        <div>
          {dataGraph != null ?
            <Bar options={options} data={dataGraph} width={286} height={203} /> : <></>
          }
        </div>
      </>
  )
};