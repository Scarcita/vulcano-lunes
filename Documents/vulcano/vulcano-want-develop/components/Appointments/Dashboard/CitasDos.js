import React, { useEffect, useState } from 'react';
import Image from 'next/image'
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'


export default function ListRepuestos() {
    const [isLoading, setIsLoading] = useState(true)

    const [data, setData] = useState([]);

    const getDatos = () => {
        setIsLoading(true)
        fetch('https://slogan.com.bo/vulcano/appointments/today')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    setData(data.data)
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })
    }

    useEffect(() => {
        getDatos();
    }, []);

    return (
        isLoading ?
            <div className='flex justify-center items-center self-center mt-[30px]'  >
                <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div >
                <div style={{maxHeight: '520px', overflowY: 'auto'}}>
                    <Table2 data={data} />
                </div>
                <div className="grid grid-cols-12 ">
                    <button
                    style={{position: 'sticky', top: 0}} className="col-span-12 md:col-span-12 lg:col-span-12 h-[35px] bg-[#3682F7] text-[#FFFFFF] rounded-b-[24px] hover:bg-[#FFFFFF] hover:text-[#3682F7]  hover:border-2 hover:border-[#3682F7]">Ver todo</button>
                </div>  
            </div>
    )
}


///////////////// TABLE DOS //////////

const Table2 = (props) => {

    const { data } = props;

    return (
    <div className="grid grid-cols-12">
        <table className={`col-span-12 md:col-span-12 lg:col-span-12  h-[520px] bg-[#FFFFFF] rounded-t-[24px]`}>
            <tbody>
                <div className="grid grid-cols-1 divide-y pl-3">
                    {data.map(row => <TableRow2 key={data.id} row={row} />)}
                </div>
            </tbody>
        </table>
    </div>

    );
};




class TableRow2 extends React.Component {

    render() {

        let row = this.props.row;

        return (
            <div className="flex flex-row">
                <div className=''>
                    <div className="text-[#000000] font-semibold text-[24px] ">
                        {row.time_from}
                    </div>
                    <div className=" rounded-[13px] border border-[#3682F7] w-[75px] h-[20px] self-center items-center text-center">
                        <div className="text-[12px] text-[#3682F7] text-center">{row.status}</div>
                    </div>
                </div>
                <div className=" self-center items-center ml-[10px]">
                    <tr className="text-[14px] text-[#000000] font-bold">{row.client.name}</tr>
                    <tr className="text-[14px] text-[#000000]"> {row.car.cars_models_version.cars_model.catalogues_record.name} • {row.car.cars_models_version.cars_model.name} • {row.car.plate}</tr>
                </div>

            </div>
        )

    }
}


