import React, { useState, useEffect } from "react";
import Tippy from '@tippyjs/react'
import 'tippy.js/dist/tippy.css'
import 'tippy.js/animations/scale.css'
import 'tippy.js/themes/light.css';
import { ObjectSchema } from "yup";
import ModalEditPedido from "./modalEditPedido";



export default function ModalProducto(props) {
    const [showModal, setShowModal] = useState(false);
    const [showEditModal, setShowEditModal] = useState(false);
    console.log("props derecha: " + Object.values(props))
    const {
        removeFromCart,
        index,
        row,
        id,
        code,
        name,
        min_stock,
    } = props;
    const { setPrecioTotal } = props;
    // console.log("dataColumnaDerecha: " + Object.values(dataColumnaDerecha))

    return (
        <>
            <div
                id='formulario'
                className='flex flex-col m-4 w-full h-full'
            //onSubmit={eliminarDatos}
            >
                <div>
                    <Tippy
                        trigger='click'
                        placement={'left'}
                        animation='scale'
                        theme='light'
                        interactive={true}
                        content={
                            <Tippy>
                                <>
                                    <div className="flex flex-col justify-left items-left ">
                                        <button
                                            className="pt-1 pb-1 items-center flex"
                                            onClick={() => setShowEditModal(true)}
                                        >
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="none"
                                                viewBox="0 0 24 24"
                                                strokeWidth="1.5"
                                                stroke="currentColor"
                                                className="w-5 h-5 text-[#3682F7]"
                                            >
                                                <path strokeLinecap="round" strokeLinejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" />
                                            </svg>

                                            <p className="text-[14px] hover:text-[#3682F7]">Editar</p>

                                        </button>

                                        <button
                                            type="button"
                                            className="pt-1 pb-1 items-center flex"
                                            onClick={() => setShowModal(true)}
                                        >
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="none"
                                                viewBox="0 0 24 24"
                                                strokeWidth="1.5"
                                                stroke="currentColor"
                                                className="w-6 h-6 text-[#FF0000]"
                                            >
                                                <path strokeLinecap="round" strokeLinejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                                            </svg>
                                            <p className="text-[14px] hover:text-[#FF0000]">Eliminar</p>
                                        </button>
                                    </div>
                                </>
                            </Tippy>
                        }>
                        <button type="button">
                            <svg
                                width="8"
                                height="28"
                                viewBox="0 0 8 28"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className='w-[10px] h-[23px] '
                            >
                                <path d="M4 9.93548C6.21111 9.93548 8 11.7532 8 14C8 16.2468 6.21111 18.0645 4 18.0645C1.78889 18.0645 0 16.2468 0 14C0 11.7532 1.78889 9.93548 4 9.93548ZM0 4.06452C0 6.31129 1.78889 8.12903 4 8.12903C6.21111 8.12903 8 6.31129 8 4.06452C8 1.81774 6.21111 0 4 0C1.78889 0 0 1.81774 0 4.06452ZM0 23.9355C0 26.1823 1.78889 28 4 28C6.21111 28 8 26.1823 8 23.9355C8 21.6887 6.21111 19.871 4 19.871C1.78889 19.871 0 21.6887 0 23.9355Z" fill="#D9D9D9" />
                            </svg>
                        </button>
                    </Tippy>
                </div>
                {showModal ? (
                    <>
                        <div className="fixed inset-0 z-10 overflow-y-auto">
                            <div
                                className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                                onClick={() => setShowModal(false)}
                            />
                            <div className="flex items-center min-h-screen px-4 py-8">
                                <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg justify-center items-center flex flex-col ">
                                    <div className="border-[#FF0000] w-[85px] h-[85px] flex justify-center items-center border-2 rounded-full mt-[30px]">
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            strokeWidth="1.5"
                                            stroke="currentColor"
                                            className="text-[#FF0000] w-[54px]  h-[6 54px] "
                                        >
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                                        </svg>
                                    </div>
                                    <div>
                                        <h1 className="text-[32px] mt-[30px]">
                                            ¿Esta seguro?
                                        </h1>
                                    </div>

                                    <div className="flex flex-col justify-center items-center mt-[22px] ">
                                        <p className="text-[18px] mt-[10px] font-light">
                                            ¿Realmente desea eliminar estos registros?
                                        </p>
                                        <p className="text-[18px] font-light">
                                            este proceso no se puede deshacer
                                        </p>
                                    </div>
                                    <div className="flex flex-row justify-between mt-[45px]">
                                        <div>
                                            <button
                                                className="w-[85px] h-[45px] border-[1px] border-[#3682F7] rounded-[15px] text-[#3682F7] hover:bg-[#3682F7] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                onClick={() =>
                                                    setShowModal(false)
                                                }
                                            >
                                                Cancel
                                            </button>
                                            <button
                                                className="w-[85px] h-[45px] border-[1px] bg-[#FF0000] rounded-[15px] text-[#FFFF] hover:border-[#FF0000] hover:bg-[#FFFF] hover:text-[#FF0000] text-[14px] mt-[3px]"
                                                onClick={() => {
                                                    //removeFromCart(index)
                                                    setShowModal(false)
                                                }}
                                            >
                                                Update
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                ) : null}

                {showEditModal ? (
                    <>
                        <div className="fixed inset-0 z-10 overflow-y-auto">
                            <div
                                className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                                onClick={() => setShowEditModal(false)}
                            ></div>
                            <div className="flex items-center min-h-screen px-4 py-8">
                                <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                    <div>
                                        <h4 className="text-lg font-medium text-gray-800 text-start">
                                            EDITAR PEDIDO
                                        </h4>

                                    </div>
                                    <div>
                                        <ModalEditPedido
                                            row={row}
                                            id={id}
                                            code={code}
                                            name={name}
                                            min_stock={min_stock}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                ) : null}
            </div>
        </>
    )
}