import React, { useState, useEffect } from "react";
import Tippy from '@tippyjs/react'
import 'tippy.js/dist/tippy.css'
import 'tippy.js/animations/scale.css'
import 'tippy.js/themes/light.css';
import Link from "next/link";
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';

export default function Modal(props) {
    const {setDataColumnaDerecha,
        row,
        id,
        code,
        name,
        created_by,
        min_stock
         } = props;
    const agregarColumnaDerecha = (item) => {
        item['price'] = cantidadReq;
        setDataColumnaDerecha(oldArray => [...oldArray, item]);
    }
    console.log('modal: ' + row );
    
    const [showSecondModal, setShowSecondModal] = useState(false);

    const [producto, setProducto] = useState(row.name);
    const [codigo, setCodigo] = useState(row.code);
    const [cantidad, setCantidad] = useState(row.min_stock);

    const [cantidadReq, setCantidadReq] = useState();
    // console.log(cantidadReq)
    

    const validationSchema = Yup.object().shape({

        producto: Yup.string()
            .required('Is required')
            .matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        codigo: Yup.string()
            .required('Is required')
            .matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        cantidad: Yup.string()
            .required('Is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),
        cantidadReq: Yup.string()
            .required('Is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),
    });

    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
       // console.log('onSubmit data: ' + data);
    }

    return (
        <>
            <form
                id='formulario'
                className='flex flex-col m-4 w-full h-full'
                onSubmit={handleSubmit(onSubmit)}
            >
                <div>
                    <Tippy
                        trigger='click'
                        placement={'left'}
                        animation='scale'
                        theme='light'
                        interactive={true}
                        content={
                            <Tippy>
                                <>
                                    <div className="flex flex-col justify-left items-left ">
                                        <button
                                            type="button"
                                            className="pt-1 pb-1 items-center flex"
                                            onClick={() => setShowSecondModal(true)}
                                        >
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="none"
                                                viewBox="0 0 24 24"
                                                strokeWidth="1.5"
                                                stroke="currentColor"
                                                className="w-6 h-6"
                                            >
                                                 <path strokeLinecap="round" strokeLinejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                            </svg>
                                            <p>Añadir</p>
                                        </button>
                                    </div>
                                </>
                            </Tippy>
                        }>
                        <button type="button">
                            <svg
                                width="8"
                                height="28"
                                viewBox="0 0 8 28"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className='w-[10px] h-[23px] '
                            >
                                <path d="M4 9.93548C6.21111 9.93548 8 11.7532 8 14C8 16.2468 6.21111 18.0645 4 18.0645C1.78889 18.0645 0 16.2468 0 14C0 11.7532 1.78889 9.93548 4 9.93548ZM0 4.06452C0 6.31129 1.78889 8.12903 4 8.12903C6.21111 8.12903 8 6.31129 8 4.06452C8 1.81774 6.21111 0 4 0C1.78889 0 0 1.81774 0 4.06452ZM0 23.9355C0 26.1823 1.78889 28 4 28C6.21111 28 8 26.1823 8 23.9355C8 21.6887 6.21111 19.871 4 19.871C1.78889 19.871 0 21.6887 0 23.9355Z" fill="#D9D9D9" />
                            </svg>
                        </button>
                    </Tippy>
                </div>
                {showSecondModal ? (
                    <>
                        <div className="fixed inset-0 z-10 overflow-y-auto">
                            <div
                                className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                                onClick={() => setShowSecondModal(false)}
                            />
                            <div className="flex items-center min-h-screen px-4 py-8">
                                <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">
                                    <div>
                                        <h4 className="text-[14PX] font-medium text-[#0000000] text-start">
                                            AÑADIR PRODUCTO
                                        </h4>
                                    </div>
                                    <div
                                        className="mt-[20px] grid grid-cols-12 gap-4 mt-[20px]"
                                        onSubmit={handleSubmit(onSubmit)}
                                    >
                                        <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Nombre producto</p>
                                            <input name="producto"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                                            {...register('producto')}
                                            value={producto}
                                            onChange={(e) => {
                                                setProducto(e.target.value)
                                            }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.producto?.message}</div>
                                        </div>
                                        <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Codigo</p>
                                            <input name="codigo"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                                            {...register('codigo')}
                                            value={codigo}
                                            onChange={(e) => {
                                                setCodigo(e.target.value)
                                            }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.codigo?.message}</div>
                                        </div>
                                        <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Cantidad en Stock</p>
                                            <input name="cantidad"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                                            {...register('cantidad')}
                                            value={cantidad}
                                            onChange={(e) => {
                                                setCantidad(e.target.value)
                                            }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.cantidad?.message}</div>
                                        </div>

                                        <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>
                                                Cantidad requerida
                                            </p>
                                            <input
                                                placeholder='Introdusca la cantidad que desea adquirir '
                                                name="cantidadReq"
                                                type="text" {...register('cantidadReq')}
                                                value={cantidadReq}
                                                onChange={(e) => setCantidadReq(e.target.value)}
                                                className={`w-full outline-none h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.cantidadReq ? 'is-invalid' : ''}`}
                                            />
                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.cantidadReq?.message}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="flex flex-row justify-between mt-[20px]">
                                        <div>
                                            <h1 className="text-[12px] mt-[10px]">
                                                This field is mandatory
                                            </h1>
                                        </div>
                                        <div>
                                            <button
                                                className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                onClick={() => setShowSecondModal(false)}>
                                                Cancel
                                            </button>
                                            <button
                                                className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                                type="button"
                                                onClick={() => {
                                                    agregarColumnaDerecha(row),
                                                        setShowSecondModal(false)
                                                }}
                                            >
                                                Update
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                ) : null}
            </form>
        </>
    )
}