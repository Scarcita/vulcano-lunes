import React, { useState, useEffect } from "react";
import Tippy from '@tippyjs/react'
import 'tippy.js/dist/tippy.css'
import 'tippy.js/animations/scale.css'
import 'tippy.js/themes/light.css';

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';

export default function ModalAñadir(props) {
    const { getDatos } = props;
    //console.log('Get Datos : ' + typeof getDatos);

    const añadirDatos = () => {
        let data = new FormData();

        data.append("name", producto);
        data.append("code", codigo);
        data.append("stock_qty", stockMax);
        data.append("min_stock", stockMin);
        data.append("price", precio);
        data.append("avg_cost", costoPromedio);
        data.append("created_by", 1);

        fetch('https://slogan.com.bo/vulcano/products/addMobile', {
            method: "POST",
            body: data,
        })
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    getDatos();
                } else {
                    console.error(data.error)
                }
            })
    }

    function onSubmit(data) {
        console.log('onSubmit data: ' + data);
        añadirDatos();
    }
    const [showModal, setShowModal] = useState(false);

    const [codigo, setCodigo] = useState("");
    const [producto, setProducto] = useState("");
    const [precio, setPrecio] = useState("");
    const [costoPromedio, setCostoPromedio] = useState("");
    const [stockMin, setStockMin] = useState("");
    const [stockMax, setStockMax] = useState("");

    const validationSchema = Yup.object().shape({
        codigo: Yup.string()
            .required('Is required')
            .matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        producto: Yup.string()
            .required('Is required')
            .matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        precio: Yup.string()
            .required('Is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),
        costoPromedio: Yup.string()
            .required('Is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),
        stockMin: Yup.string()
            .required('Is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),

        stockMax: Yup.string()
            .required('Is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),
    });

    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;


    return (
        <>
            <form id='formulario' onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <Tippy
                        trigger='click'
                        placement={'left'}
                        animation='scale'
                        theme='light'
                        interactive={true}
                    >
                        <div>
                            <button
                                className="w-[94px] h-[30px] rounded-[7px] text-[#FFFFFF] bg-[#3682F7]"
                                type="submit"
                                onClick={() => setShowModal(true)}
                            >
                                Nuevo
                            </button>
                        </div>
                    </Tippy>
                </div>
                {showModal ? (
                    <>
                        <div className="fixed inset-0 z-10 overflow-y-auto">
                            <div
                                className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                                onClick={() => setShowModal(false)}
                            />
                            <div className="flex items-center min-h-screen px-4 py-8">
                                <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">
                                    <div>
                                        <h4 className="text-lg font-medium text-gray-800">
                                            Añadir Productos
                                        </h4>
                                    </div>

                                    <div
                                        className="mt-[20px] grid grid-cols-12 gap-4 mt-[20px]"
                                        onSubmit={handleSubmit(onSubmit)}
                                    >
                                        <div className='col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]'>
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>
                                                Código
                                            </p>
                                            <input
                                                name="codigo"
                                                type="text" {...register('codigo')}
                                                // value={fill Data ? codigo : ''}
                                                onChange={(e) => setCodigo(e.target.value)}
                                                className={`w-full h-[40px] outline-none bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.codigo ? 'is-invalid' : ''}`}
                                            />

                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.codigo?.message}
                                                </p>
                                            </div>
                                        </div>

                                        <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>
                                                Producto
                                            </p>
                                            <input
                                                name="producto"
                                                type="text" {...register('producto')}
                                                onChange={(e) => setProducto(e.target.value)}
                                                className={`w-full outline-none h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.producto ? 'is-invalid' : ''}`}
                                            />

                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.producto?.message}
                                                </p>
                                            </div>
                                        </div>

                                        <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>
                                                Precio
                                            </p>
                                            <input
                                                name="precio"
                                                type="text" {...register('precio')}
                                                onChange={(e) => setPrecio(e.target.value)}
                                                className={`w-full outline-none h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.precio ? 'is-invalid' : ''}`}
                                            />

                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.precio?.message}
                                                </p>
                                            </div>
                                        </div>

                                        <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>
                                                Costo Promedio
                                            </p>
                                            <input
                                                name="costoPromedio"
                                                type="text" {...register('costoPromedio')}
                                                onChange={(e) => setCostoPromedio(e.target.value)}
                                                className={`w-full outline-none h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.costoPromedio ? 'is-invalid' : ''}`}
                                            />

                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.costoPromedio?.message}
                                                </p>
                                            </div>
                                        </div>

                                        <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>
                                                Stock Min.
                                            </p>
                                            <input
                                                name="stockMin"
                                                type="text" {...register('stockMin')}
                                                onChange={(e) => setStockMin(e.target.value)}
                                                className={`w-full outline-none h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.stockMin ? 'is-invalid' : ''}`}
                                            />

                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.stockMin?.message}
                                                </p>
                                            </div>
                                        </div>

                                        <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>
                                                Stock Max.
                                            </p>
                                            <input
                                                name="stockMax"
                                                type="text" {...register('stockMax')}
                                                onChange={(e) => setStockMax(e.target.value)}
                                                className={`w-full h-[40px] outline-none bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.stockMax ? 'is-invalid' : ''}`}
                                            />

                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.stockMax?.message}
                                                </p>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="flex flex-row justify-between mt-[20px]">
                                        <div>
                                            <h1 className="text-[12px] mt-[10px]">
                                                This field is mandatory
                                            </h1>
                                        </div>

                                        <div>
                                            <button
                                                className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                onClick={() =>
                                                    setShowModal(false)
                                                }
                                            >
                                                Cancelar
                                            </button>

                                            <button
                                                className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                                type="button"
                                                // id="refresh"
                                                onClick={() => {
                                                    añadirDatos(),
                                                    setShowModal(false)
                                                    // "/pantallaProductos",
                                                    // window.location.href = window.location.href;
                                                    // location.reload()
                                                }}
                                            >
                                                Añadir
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                ) : null}
            </form>
        </>
    )
}