import React, { useState, useEffect } from "react";
import Tippy from '@tippyjs/react'
import 'tippy.js/dist/tippy.css'
import 'tippy.js/animations/scale.css'
import 'tippy.js/themes/light.css';
import Link from "next/link";

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';

export default function Modal(props) {

    const { id } = props;
    const { getDatos } = props;
    console.log('Props Modal: ' + getDatos);

    const actualizarDatos = () => {
        let data = new FormData();

        data.append("name", producto);
        data.append("code", codigo);
        data.append("stock_qty", stockMax);
        data.append("min_stock", stockMin);
        data.append("price", precio);
        data.append("avg_cost", costoPromedio);

        fetch('https://slogan.com.bo/vulcano/products/editMobile/' + id, {
            method: "POST",
            body: data,
        })
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    getDatos();
                    console.log(data.data);
                } else {
                    console.error(data.error)
                }
            })
    }



    const eliminarDatos = ( ) => {
        let data = new FormData();

        data.append("name", producto);
        data.append("code", codigo);
        data.append("stock_qty", stockMax);
        data.append("min_stock", stockMin);
        data.append("price", precio);
        data.append("avg_cost", costoPromedio);

        fetch('https://slogan.com.bo/vulcano/products/deleteMobile/' + id, {
            method: "POST",
            body: data,
        })
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    getDatos();
                    console.log(data.data);
                } else {
                    console.error(data.error)
                }
            })
    }




    const [showModal, setShowModal] = useState(false);
    const [showSecondModal, setShowSecondModal] = useState(false);

    const [codigo, setCodigo] = useState("");
    const [producto, setProducto] = useState("");
    const [precio, setPrecio] = useState("");
    const [costoPromedio, setCostoPromedio] = useState("");
    const [stockMin, setStockMin] = useState("");
    const [stockMax, setStockMax] = useState("");

    const validationSchema = Yup.object().shape({
        codigo: Yup.string()
            .required('Is required')
            .matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        producto: Yup.string()
            .required('Is required')
            .matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        precio: Yup.string()
            .required('Is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),
        costoPromedio: Yup.string()
            .required('Is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),
        stockMin: Yup.string()
            .required('Is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),

        stockMax: Yup.string()
            .required('Is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),
    });

    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        console.log('onSubmit data: ' + data);
        actualizarDatos();
        eliminarDatos();
    }

    // let refresh = document.getElementById('refresh');
    // refresh.addEventListener('click', _ => {
    //             location.reload();
    // })

    var DatosId = 123;

    return (
        <>
            <form id='formulario' className='flex flex-col m-4 w-full h-full' onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <Tippy
                        trigger='click'
                        placement={'left'}
                        animation='scale'
                        theme='light'
                        interactive={true}
                        content={
                            <Tippy>
                                <>
                                    <div
                                        className="flex flex-col justify-left items-left "
                                    >
                                        <Link
                                            href={'/product/' + id}
                                        >
                                            <button
                                                className="pt-1 pb-1 items-center flex"
                                            >
                                                <svg
                                                    xmlns="http://www.w3.org/2000/svg"
                                                    fill="none"
                                                    viewBox="0 0 24 24"
                                                    strokeWidth="1.5"
                                                    stroke="currentColor"
                                                    className="w-6 h-6"
                                                >
                                                    <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 14.25v-2.625a3.375 3.375 0 00-3.375-3.375h-1.5A1.125 1.125 0 0113.5 7.125v-1.5a3.375 3.375 0 00-3.375-3.375H8.25m5.231 13.481L15 17.25m-4.5-15H5.625c-.621 0-1.125.504-1.125 1.125v16.5c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125V11.25a9 9 0 00-9-9zm3.75 11.625a2.625 2.625 0 11-5.25 0 2.625 2.625 0 015.25 0z" />
                                                </svg>

                                                <p>Ver detalle</p>
                                            </button>
                                        </Link>

                                        <button
                                            className="pt-1 pb-1 items-center flex"
                                            onClick={() => setShowSecondModal(true)}
                                        >
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="none"
                                                viewBox="0 0 24 24"
                                                strokeWidth="1.5"
                                                stroke="currentColor"
                                                className="w-6 h-6"
                                            >
                                                <path strokeLinecap="round" strokeLinejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" />
                                            </svg>

                                            <p>Editar</p>

                                        </button>

                                        <button
                                            className="pt-1 pb-1 items-center flex"
                                            onClick={() => setShowModal(true)}
                                        >
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="none"
                                                viewBox="0 0 24 24"
                                                strokeWidth="1.5"
                                                stroke="currentColor"
                                                className="w-6 h-6"
                                            >
                                                <path strokeLinecap="round" strokeLinejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                                            </svg>
                                            <p>
                                                Eliminar
                                            </p>
                                        </button>
                                    </div>
                                </>
                            </Tippy>
                        }>
                        <button>
                            <svg
                                width="8"
                                height="28"
                                viewBox="0 0 8 28"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className='w-[10px] h-[23px] '
                            >
                                <path d="M4 9.93548C6.21111 9.93548 8 11.7532 8 14C8 16.2468 6.21111 18.0645 4 18.0645C1.78889 18.0645 0 16.2468 0 14C0 11.7532 1.78889 9.93548 4 9.93548ZM0 4.06452C0 6.31129 1.78889 8.12903 4 8.12903C6.21111 8.12903 8 6.31129 8 4.06452C8 1.81774 6.21111 0 4 0C1.78889 0 0 1.81774 0 4.06452ZM0 23.9355C0 26.1823 1.78889 28 4 28C6.21111 28 8 26.1823 8 23.9355C8 21.6887 6.21111 19.871 4 19.871C1.78889 19.871 0 21.6887 0 23.9355Z" fill="#D9D9D9" />
                            </svg>
                        </button>
                    </Tippy>
                </div>
                {showModal ? (
                    <>
                        <div className="fixed inset-0 z-10 overflow-y-auto">
                            <div
                                className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                                onClick={() => setShowModal(false)}
                            />
                            <div className="flex items-center min-h-screen px-4 py-8">
                                <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg justify-center items-center flex flex-col ">
                                    <div
                                        className="border-[#FF0000] w-[85px] h-[85px] flex justify-center items-center border-2 rounded-full mt-[30px] "
                                    >
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            fill="none"
                                            viewBox="0 0 24 24"
                                            strokeWidth="1.5"
                                            stroke="currentColor"
                                            className="text-[#FF0000] w-[54px]  h-[6 54px] "
                                        >
                                            <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
                                        </svg>

                                    </div>

                                    <div>
                                        <h1
                                            className="text-[32px] mt-[30px]"
                                        >
                                            ¿Esta seguro?
                                        </h1>
                                    </div>

                                    <div
                                        className="flex flex-col justify-center items-center mt-[22px] "
                                    >
                                        <p
                                            className="text-[18px] mt-[10px] font-light "
                                        >
                                            ¿Realmente desea eliminar estos registros?
                                        </p>
                                        <p
                                            className="text-[18px] font-light "
                                        >
                                            este proceso no se puede deshacer
                                        </p>
                                    </div>

                                    <div className="flex flex-row justify-between mt-[45px]">
                                        <div>
                                            <button
                                                className="w-[85px] h-[45px] border-[1px] border-[#3682F7] rounded-[15px] text-[#3682F7] hover:bg-[#3682F7] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                onClick={() =>
                                                    setShowModal(false)
                                                }
                                            >
                                                Cancel
                                            </button>
                                            <button
                                                className="w-[85px] h-[45px] border-[1px] bg-[#FF0000] rounded-[15px] text-[#FFFF] hover:border-[#FF0000] hover:bg-[#FFFF] hover:text-[#FF0000] text-[14px] mt-[3px]"
                                                // onClick={() =>
                                                //     setShowModal(false)
                                                // }
                                                onClick={() => {
                                                    eliminarDatos(),
                                                    setShowModal(false)
                                                    // "/pantallaProductos",
                                                    // window.location.href = window.location.href;
                                                    // location.reload()
                                                }}
                                            >
                                                Update
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                ) : null}
                {showSecondModal ? (
                    <>
                        <div className="fixed inset-0 z-10 overflow-y-auto">
                            <div
                                className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                                onClick={() => setShowSecondModal(false)}
                            />
                            <div className="flex items-center min-h-screen px-4 py-8">
                                <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">
                                    <div>
                                        <h4 className="text-lg font-medium text-gray-800">
                                            Edit payment Method
                                        </h4>
                                    </div>

                                    <div
                                        className="mt-[20px] grid grid-cols-12 gap-4 mt-[20px]"
                                        onSubmit={handleSubmit(onSubmit)}
                                    >
                                        <div className='col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]'>
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>
                                                Código
                                            </p>
                                            <input
                                                name="codigo"
                                                type="text" {...register('codigo')}
                                                // value={fill Data ? codigo : ''}
                                                onChange={(e) => setCodigo(e.target.value)}
                                                className={`w-full h-[40px] outline-none bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.codigo ? 'is-invalid' : ''}`}
                                            />

                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.codigo?.message}
                                                </p>
                                            </div>
                                        </div>

                                        <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>
                                                Producto
                                            </p>
                                            <input
                                                name="producto"
                                                type="text" {...register('producto')}
                                                onChange={(e) => setProducto(e.target.value)}
                                                className={`w-full outline-none h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.producto ? 'is-invalid' : ''}`}
                                            />

                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.producto?.message}
                                                </p>
                                            </div>
                                        </div>

                                        <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>
                                                Precio
                                            </p>
                                            <input
                                                name="precio"
                                                type="text" {...register('precio')}
                                                onChange={(e) => setPrecio(e.target.value)}
                                                className={`w-full outline-none h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.precio ? 'is-invalid' : ''}`}
                                            />

                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.precio?.message}
                                                </p>
                                            </div>
                                        </div>

                                        <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>
                                                Costo Promedio
                                            </p>
                                            <input
                                                name="costoPromedio"
                                                type="text" {...register('costoPromedio')}
                                                onChange={(e) => setCostoPromedio(e.target.value)}
                                                className={`w-full outline-none h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.costoPromedio ? 'is-invalid' : ''}`}
                                            />

                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.costoPromedio?.message}
                                                </p>
                                            </div>
                                        </div>

                                        <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>
                                                Stock Min.
                                            </p>
                                            <input
                                                name="stockMin"
                                                type="text" {...register('stockMin')}
                                                onChange={(e) => setStockMin(e.target.value)}
                                                className={`w-full outline-none h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.stockMin ? 'is-invalid' : ''}`}
                                            />

                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.stockMin?.message}
                                                </p>
                                            </div>
                                        </div>

                                        <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>
                                                Stock Max.
                                            </p>
                                            <input
                                                name="stockMax"
                                                type="text" {...register('stockMax')}
                                                onChange={(e) => setStockMax(e.target.value)}
                                                className={`w-full h-[40px] outline-none bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.stockMax ? 'is-invalid' : ''}`}
                                            />

                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.stockMax?.message}
                                                </p>
                                            </div>
                                        </div>

                                    </div>
                                    <div className="flex flex-row justify-between mt-[20px]">
                                        <div>
                                            <h1 className="text-[12px] mt-[10px]">
                                                This field is mandatory
                                            </h1>
                                        </div>

                                        <div>
                                            <button
                                                className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                onClick={() =>
                                                    setShowSecondModal(false)
                                                }
                                            >
                                                Cancel
                                            </button>

                                            <button
                                                className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                                type="button"
                                                // id="refresh"
                                                onClick={() => {
                                                    actualizarDatos(),
                                                        setShowSecondModal(false)
                                                }}
                                            >
                                                Update
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                ) : null}
            </form>
        </>
    )
}