import React, { useEffect, useState } from 'react';
import Image from 'next/image'
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'


export default function RequestedProductsFromOtNotCompleted() {
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);

    
    useEffect(() => {

        setIsLoading(true)

        fetch('https://slogan.com.bo/vulcano/products/dashboard')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    var temp = [];
                    Object.values(data.data).map((result) =>{
                        temp.push(result);
                    })
    
                    setData(temp[7])
                    console.log(temp);
                    
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

    }, [])

    return (
        isLoading ?
            <div className='flex justify-center items-center self-center mt-[30px]'  >
                <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div >
                <div style={{maxHeight: '247px', overflowY: 'auto'}}>
                    <Table2 data={data} />
                </div>
                <div className="grid grid-cols-12 ">
                    <button
                    style={{position: 'sticky', top: 0}} className="col-span-12 md:col-span-12 lg:col-span-12 h-[35px] bg-[#3682F7] text-[#FFFFFF] rounded-b-[24px] hover:bg-[#FFFFFF] hover:text-[#3682F7]  hover:border-2 hover:border-[#3682F7]">Ver todo</button>
                </div>  
            </div>
    )
}


///////////////// TABLE DOS //////////

const Table2 = (props) => {

    const { data } = props;

    return (
    <div className="grid grid-cols-12">
        <table className={`col-span-12 md:col-span-12 lg:col-span-12 h-[247px] bg-[#FFFFFF] rounded-t-[24px]`}>
            <tbody>
                <div className="grid grid-cols-1 divide-y pl-3 pr-3">
                    {data.map(row => <TableRow2 key={data.id} row={row} />)}
                </div>
            </tbody>
        </table>
    </div>

    );
};




class TableRow2 extends React.Component {

    render() {

        let row = this.props.row;

        return (
            <div className="flex flex-row">
                <div className="w-[54px] h-[54px] rounded-full bg-[#F6F6FA] items-center self-center">

                        <Image
                            src={row.car.cars_models_version.cars_model.catalogues_record.additional_info}
                            alt="logo"
                            layout="fixed"
                            width={54}
                            height={54}
                            className="rounded-full"
                        />


                </div>
                <div className="pl-[10px] pt-[20px] pb-[15px]">
                    <div className="w-[53px] h-[13px] bg-[#71AD46] rounded-[13px] pl-[5px]">
                        <tr className="text-[8px] text-[#FFFFFF]">{row.status}</tr>
                    </div>
                    <tr className="text-[14px] text-[#000000] font-bold">{row.client.email}</tr>
                    <tr className="text-[8px] text-[#000000]">{row.car.cars_models_version.name} *{row.car.plate} *{row.code}</tr>
                </div>

            </div>
        )

    }
}


