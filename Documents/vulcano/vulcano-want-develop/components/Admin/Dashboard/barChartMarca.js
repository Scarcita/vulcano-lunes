import React, { useEffect, useState } from 'react';
import Chart from "chart.js";

export default function BarChartMarcas() {
  const [isLoading, setIsLoading] = useState(true)
  const [enteredMakes0, setOrderSenteredMakes0] = useState([]);
  const [enteredMakes1, setOrderSenteredMakes1] = useState([]);

  useEffect(() => {

      setIsLoading(true)

      fetch('https://slogan.com.bo/vulcano/orders/adminDashboard')
          .then(response => response.json())
          .then(data => {
              if (data.status) {
                  var temp = [];
                  Object.values(data.data).map((result) =>{
                      temp.push(result);
                  })
  
                  setOrderSenteredMakes0(temp[8].PORSCHE)
                  setOrderSenteredMakes1(temp[8].MERCEDES)
                  
              } else {
                  console.error(data.error)
              }
              setIsLoading(false)
          })

  }, [])
    React.useEffect(() => {
        let config = {
          type: "bar",
          data: {
            labels: [
              "PORSCHE",
              "MERCEDES BENZ",
    
            ],
            datasets: [
              {
                label: new Date().getFullYear(),
                backgroundColor: "#3682F7",
                borderColor: "#3682F7",
                data: [
                  enteredMakes0,
                  enteredMakes1
                ],
                fill: false,
                barThickness: 12,
              },
            ],
          },
          options: {
            
            maintainAspectRatio: false,
            responsive: true,
            title: {
              display: false,
              text: "Orders Chart",
              fontColor: "#000000",
            },
            tooltips: {
              mode: "index",
              intersect: false,
            },
            hover: {
              mode: "nearest",
              intersect: true,
            },
            legend: {
              labels: {
                fontColor: "rgba(0,0,0,.4)",
                color: "#3682F7"
              },
              align: "end",
              position: "bottom",
            },
            scales: {
              xAxes: [
                {
                  
                  display: false,
                  scaleLabel: {
                    display: true,
                    labelString: "Month",
                  },
                  gridLines: {
                    borderDash: [2],
                    borderDashOffset: [2],
                    color: "rgba(33, 37, 41, 0.3)",
                    zeroLineColor: "rgba(33, 37, 41, 0.3)",
                    zeroLineBorderDash: [2],
                    zeroLineBorderDashOffset: [2],
                  },
                },
              ],
              yAxes: [
                {
                  display: true,
                  scaleLabel: {
                    display: false,
                    labelString: "Value",
                  },
                  gridLines: {
                    borderDash: [2],
                    drawBorder: false,
                    borderDashOffset: [2],
                    color: "rgba(33, 37, 41, 0.2)",
                    zeroLineColor: "rgba(33, 37, 41, 0.15)",
                    zeroLineBorderDash: [2],
                    zeroLineBorderDashOffset: [2],
                  },
                },
              ],
            },
          },
        };
    
        let ctx = document.getElementById("bar-chartMarcas").getContext("2d");
        window.myBar = new Chart(ctx, config);
      }, []);
      
      return (
        <>
          <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded bg-[#fff]">
              <div className="p-4 flex-auto">
                <div className="relative h-[230px]">
                  <canvas id="bar-chartMarcas"></canvas>
                </div>
              </div>
          </div>
        </>
      );
    }
    
    
    