import React, { useEffect, useState } from "react";
import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";
import Image from "next/image";
import ImgAnuncio from '../../assets/img/dolar.svg'
import CounterLatestOrder from "./accountants/latestOrder";
import CounterOrder from "./accountants/counterOrder";
import CounterAppointments from "./accountants/counterAppointments";
import CounterDeskSales from "./accountants/counterDeskSales";
import CounterOrdersSales from "./accountants/counterOrdersSales";
import DeskSales from "./accountants/DeskSales";

export default function AccountantsAll() {



    return(
        <div className="grid grid-cols-10 gap-3 mt-[20px]">
            <div className="col-span-5 md:col-span-5 lg:col-span-1 bg-[#3682F7] rounded-[7px] self-center text-center p-[5px]">
                <p className="text-[#fff] text-[12px] font-light">
                    Latest Order #
                </p>
                <div>
                    <CounterLatestOrder/>
                </div>
                <p className="text-[#fff] text-[10px] font-light">
                    On: 11/01/2013
                </p>
            </div>
            <div className="col-span-5 md:col-span-5 lg:col-span-1 bg-[#fff] rounded-[7px] self-center text-center p-[5px]">
                <p className="text-[#000] text-[12px] font-light">
                    Orders
                </p>
                <div>
                    <CounterOrder/>
                </div>
                <p className="text-[#D3D3D3] text-[10px] font-light">
                    Since: 11/01/2023
                </p>
            </div>
            <div className="col-span-5 md:col-span-5 lg:col-span-1 bg-[#fff] rounded-[7px] self-center text-center p-[5px]">
                <p className="text-[#000] text-[12px] font-light">
                    Appointments
                </p>
                <div>
                    <CounterAppointments/>
                </div>
                <p className="text-[#D3D3D3] text-[10px] font-light">
                    Since: 11/01/2023
                </p>
            </div>
            <div className="col-span-5 md:col-span-5 lg:col-span-1 bg-[#fff] rounded-[7px] self-center text-center p-[5px]">
                <p className="text-[#000] text-[12px] font-light">
                    Desk Sales
                </p>
                <div>
                    <CounterDeskSales/>
                </div>
                <p className="text-[#D3D3D3] text-[10px] font-light">
                    Since: 11/01/2023
                </p>
            </div>
            <div className="col-span-10 md:col-span-5 lg:col-span-2 bg-[#fff] rounded-[9px] p-[5px] flex justify-between">
                <div>
                    <p className="text-[#000] text-[12px] font-medium">
                        Last Activity
                    </p>
                    <div className="text-[#3682F7] text-[14px] font-semibold">
                        Call
                    </div>
                    <p className="text-[#3682F7] text-[12px] font-light">
                        Made by: Coordinador
                    </p>
                    <p className="text-[#D3D3D3] text-[10px] font-light">
                        Since: 11/01/2023
                    </p>
                </div>
                <div className="bg-[#E4E7EB] w-[38px] h-[38px] rounded-full self-center items-center text-center  pt-[3px]">
                    <Image
                        className="items-center self-center text-center"
                        src={ImgAnuncio}
                        layout="fixed"
                        alt="media"
                        width={30}
                        height={30}
                    />
                </div>
            </div>
            <div className="col-span-10 md:col-span-5 lg:col-span-2 bg-[#fff] rounded-[9px] p-[5px] flex justify-between">
                <div>
                    <p className="text-[#000] text-[12px] font-medium">
                        Orders Sales
                    </p>
                    <CounterOrdersSales/>
                    <p className="text-[#D3D3D3] text-[10px] font-light">
                        Total
                    </p>
                </div>
                <div className="bg-[#E4E7EB] w-[38px] h-[38px] rounded-full self-center items-center text-center  pt-[3px]">
                    <Image
                        className="items-center self-center text-center"
                        src={ImgAnuncio}
                        layout="fixed"
                        alt="media"
                        width={30}
                        height={30}
                    />
                </div>
            </div>
            <div className="col-span-10 md:col-span-10 lg:col-span-2 bg-[#fff] rounded-[9px] p-[5px] flex justify-between">
                <div>
                    <p className="text-[#000] text-[12px] font-medium">
                        Desk Sales
                    </p>
                    <div>
                        <DeskSales/>
                    </div>
                      
                    <p className="text-[#D3D3D3] text-[10px] font-light">
                        Total
                    </p>
                </div>
                <div className="bg-[#E4E7EB] w-[38px] h-[38px] rounded-full self-center items-center text-center  pt-[3px]">
                    <Image
                        className="items-center self-center text-center"
                        src={ImgAnuncio}
                        layout="fixed"
                        alt="media"
                        width={30}
                        height={30}
                    />
                </div>
            </div>
        </div>
    )

}