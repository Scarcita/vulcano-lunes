import React, { useEffect, useState } from "react";
import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";


export default function CounterDeskSales() {
    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState([]);
    const [postsOnly, setPostsOnly] = useState([]);
    console.log(postsOnly);
    var postsTemp = [];

    const getDatos = () => {
        setIsLoading(true);
        fetch("https://slogan.com.bo/vulcano/clients/getByIdCrm/1")
          .then((response) => response.json())
          .then((data) => {
            if (data.status) {
                setData(Object.values(data.data))

                Object.values(data.data).map((result) =>{
                    postsTemp.push(result);
                })
                
            setPostsOnly(postsTemp[1]);
            } else {
              console.error(data.error);
            }
            setIsLoading(false);
          });
      };
      useEffect(() => {
        getDatos();
      }, []);

    return(
        isLoading ?
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color='#3682F7' size={12} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
        <div>

            { postsOnly[0].discount !== null && postsOnly[0].discount !== undefined ?

                <p className="text-[#3682F7] text-[24px]">
                    {postsOnly[0].discount}
                </p>
            :
            <></>
            }

        </div>
    )
}