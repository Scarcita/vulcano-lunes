import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";


export default function ModalEditOrders(props) {

  const {
    id, 
    title, 
    subtitle, 
    type, 
    planned_datetime, 
    post_copy, 
    social_network, 
    instructions} = props;
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [titleForm, setTitleForm] = useState(title)
  const [subtitleForm, setSubtitleForm] = useState(subtitle)
  const [typeForm, setTypeForm] = useState(type)
  const [plannedDatetimeForm, setPlannedDatetimeForm] = useState (planned_datetime)
  const [postCopyForm, setPostCopyForm] = useState (post_copy);
  //const [imgUrlForm, setImgUrlForm] = useState (imgUrl);
  const [socialNetworkForm, setSocialNetworkForm] = useState (social_network);
  const [instructionsForm, setInstructionsForm] = useState (instructions);


//   const actualizarDatos = async (formData) => {

//       setIsLoading(true)
//       var data = new FormData();

//       //console.log(formData.mediaUrl);
  
//       data.append("client_plan_id", client_plan_id);
//       data.append("social_network", formData.social_network);
//       data.append("type", formData.type);
//       data.append("planned_datetime", plannedDatetimeForm);
//       data.append("title", titleForm);
//       data.append("subtitle", subtitleForm);
//       data.append("instructions", instructionsForm);
//       data.append("post_copy", postCopyForm);
      
//       if(formData.mediaUrl.length !== 0){
//           data.append("media_url", formData.mediaUrl[0]);
//       }
  
//       fetch("http://slogan.com.bo/roadie/clientsPlansExtras/editMobile/" + id, {
//         method: 'POST',
//         body: data,
//       })
//         .then(response => response.json())
//         .then(data => {
//           console.log('VALOR ENDPOINTS EDIT: ', data);
//           setIsLoading(false)
//           if (data.status) { 
//               //setReloadPosts(!reloadPosts)
//               setShowModal(false)
//               console.log('edit endpoint: ' + data.status);
//           } else {
//               console.error(data.error)
//           }
//       })
  
//     }

  
  const validationSchema = Yup.object().shape({
  title: Yup.string()
  .required('title is required'),

  socialNetwork: Yup.string()
      .required('socialNetwork is required')
      .oneOf(['Facebook', 'Instagram', 'Mailing', 'YouTube', 'TikTok', 'LinkedIn', 'Twitter']),


  plannedDateTime: Yup.string()
      .required('plannedDateTime is required'),

  });
  const formOptions = { resolver: yupResolver(validationSchema) };

  // get functions to build form with useForm() hook
  const { register, handleSubmit, reset, formState } = useForm(formOptions);
  const { errors } = formState;

  function onSubmit(data) {
      alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
      console.log('string is NOT empty')
      actualizarDatos(data)
      return false;
      
  }

    return (
        <>
            <div className="flex items-center min-h-screen px-4 py-8">
                <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">
                    <div>
                        <h4 className="text-lg font-medium text-gray-800 text-left">
                            EDIT POST
                        </h4>
                    </div>

                    <div>
                        <form onSubmit={handleSubmit(onSubmit)} >

                            <div className="mt-[20px] grid grid-cols-12 gap-4">

                                <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Name Car</p>
                                    <input name="text"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]'
                                    {...register('title')}
                                    value={titleForm}
                                    onChange={(e) => {
                                        setTitleForm(e.target.value)
                                    }}
                                    />
                                    <div className="text-[14px] text-[#FF0000]">{errors.title?.message}</div>
                                </div>
                                <div className="col-span-6 md:col-span-6 lg:col-span-6 ">
                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>	Service Advisor</p>
                                    <input name="text"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]'
                                    {...register('subtitle')}
                                    value={subtitleForm}
                                    onChange={(e) => {
                                        setSubtitleForm(e.target.value)
                                    }}
                                    />
                                    <div className="text-[14px] text-[#FF0000]">{errors.subtitle?.message}</div>
                                </div>
                                <div className="col-span-6 md:col-span-6 lg:col-span-6">
                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Date Created</p>
                                    <input name="plannedDateTime" type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`}
                                    {...register('plannedDateTime')}
                                    value={plannedDatetimeForm}
                                    onChange={(e) => {
                                        setPlannedDatetimeForm(e.target.value)
                                    }}
                                    />
                                    <div className="text-[14px] text-[#FF0000]">{errors.plannedDateTime?.message}</div>
                                </div>
                                <div className="col-span-6 md:col-span-6 lg:col-span-6 ">
                                    <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Date Delivered</p>
                                    <input name="plannedDateTime" type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`}
                                    {...register('plannedDateTime')}
                                    value={plannedDatetimeForm}
                                    onChange={(e) => {
                                        setPlannedDatetimeForm(e.target.value)
                                    }}
                                    />
                                    <div className="text-[14px] text-[#FF0000]">{errors.plannedDateTime?.message}</div>
                                </div>
                            </div>

                            <div className="flex flex-row justify-between mt-[20px]">

                                <div>
                                    <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                </div>

                                <div>
                                <button
                                    className="w-[72px] h-[30px] text-[#FFFFFF] bg-[#000] text-[12px] rounded-[9px] text-center items-center hover:bg-transparent hover:border-[#000] hover:border hover:text-[#000] justify-center mr-[10px]"
                                    onClick={() =>
                                        setShowModal(false)
                                    }
                                    disabled={isLoading}
                                >
                                    Cancel
                                </button>
                                <button
                                    className="w-[72px] h-[30px] text-[#FFFFFF] bg-[#3682F7] text-[12px] rounded-[9px] text-center items-center hover:bg-transparent hover:border-[#3682F7] hover:border hover:text-[#3682F7] justify-center"
                                    type="submit"
                                    disabled={isLoading}

                                >
                                    {isLoading ? <Dots className='m-auto' size={7} color={'#fff'}></Dots> : 'Update'}
                                    
                                </button>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}