import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Dots } from 'react-activity';
import "react-activity/dist/Dots.css";


export default function ModalInf(props) {

    const {selectedServices, reloadServices, setReloadServices} = props
    console.log('ADD SERVICE: ' + selectedServices);
    const [showModal, setShowModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false)
    const [name, setName] = useState('')
    const [phone, setPhone] = useState('')
    const [email, setEmail] = useState('')
    const [invoiceName, setInvoiceName] = useState('')
    const [invoiceCinit, setInvoiceCinit] = useState('')

    
    const clearForm = () => {
        setCode('')
        setName('')
        setTime('')
        setPrice('')
    }
    
  const getDataOrden = async (formData) => {
    
    setIsLoading(true)
        var data = new FormData();
    data.append("category_id", selectedServices);
    data.append("code", code);
    data.append("name", name);
    data.append("time", time);
    data.append("price", price);

    fetch("http://slogan.com.bo/vulcano/services/addMobile/" + selectedServices, {
      method: 'POST',
      body: data,
    })
      .then(response => response.json())
      .then(data => {
        console.log('VALOR ENDPOINTS: ', data);
        setIsLoading(false)
        if(data.status){
          clearForm()
          setReloadServices(!reloadServices)
          setShowModal(false)
        } else {
          alert(JSON.stringify(data.errors, null, 4))
        }
        
      },
      (error) => {
        console.log(error)
      }
      )
  }
 
  ////////////////// VALIDATION ///////////////////

  
  const validationSchema = Yup.object().shape({

    name: Yup.string()
    .required('name is required'),

    phone: Yup.string()
    .required('phone is required'),

    email: Yup.string()
    .required('email is required'),
    });

    const formOptions = { resolver: yupResolver(validationSchema) };

    // get functions to build form with useForm() hook
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
        console.log('string is NOT empty')
        getDataOrden(data)
        return false;
        
    }


    return (
        <>
            <div>
                <button
                    className="w-[68px] md:w-[80px] lg:w-[85px] h-[25px] text-[#FFFFFF] bg-[#3682F7] text-[12px] rounded-[7px] text-center items-center hover:bg-transparent hover:border-[#3682F7] hover:border hover:text-[#3682F7] justify-center mr-[10px]"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    Edit Inf
                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                <div>
                                    <h4 className="text-lg font-medium text-gray-800 text-left">
                                        Edit Information
                                    </h4>

                                </div>
                                <div>
                                    <form onSubmit={handleSubmit(onSubmit)}>

                                        <div className="mt-[20px] grid grid-cols-12 gap-4">

                                            <div className='col-span-12 md:col-span-12 lg:col-span-12 text-left'>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Name:</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="text"
                                                    name="name"
                                                    {...register('name')}
                                                    value={name}
                                                    onChange={(e) => {
                                                        setName(e.target.value)
                                                    }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.name?.message}</div>
                                            </div>

                                            <div className='col-span-12 md:col-span-12 lg:col-span-6 text-left'>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Phone:</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="number"
                                                    name="phone"
                                                    {...register('phone')}
                                                    value={phone}
                                                    onChange={(e) => {
                                                        setPhone(e.target.value)
                                                    }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.phone?.message}</div>
                                            </div>

                                            <div className='col-span-12 md:col-span-12 lg:col-span-6 text-left'>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Email:</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="email"
                                                    name="email"
                                                    {...register('email')}
                                                    value={email}
                                                    onChange={(e) => {
                                                        setEmail(e.target.value)
                                                    }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.email?.message}</div>
                                            </div>

                                            <div className='col-span-12 md:col-span-12 lg:col-span-6 text-left'>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Invoice Name:</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="text"
                                                    name="invoiceName"
                                                    {...register('invoiceName')}
                                                    value={invoiceName}
                                                    onChange={(e) => {
                                                        setInvoiceName(e.target.value)
                                                    }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.invoiceName?.message}</div>
                                            </div>

                                            <div className='col-span-12 md:col-span-12 lg:col-span-6 text-left'>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Invoice Cinit:</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="number"
                                                    name="invoiceCinit"
                                                    {...register('invoiceCinit')}
                                                    value={invoiceCinit}
                                                    onChange={(e) => {
                                                        setInvoiceCinit(e.target.value)
                                                    }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.invoiceCinit?.message}</div>
                                            </div>
                                        </div>
                                        <div className="flex flex-row justify-between mt-[20px]">

                                            <div>
                                                <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                            </div>

                                            <div>
                                                <button
                                                    className="w-[72px] h-[30px] text-[#FFFFFF] bg-[#000] text-[12px] rounded-[9px] text-center items-center hover:bg-transparent hover:border-[#000] hover:border hover:text-[#000] justify-center mr-[10px]"
                                                    onClick={() =>
                                                        setShowModal(false)
                                                    }
                                                    disabled={isLoading}
                                                >
                                                    Cancel
                                                </button>
                                                <button
                                                    className="w-[72px] h-[30px] text-[#FFFFFF] bg-[#3682F7] text-[12px] rounded-[9px] text-center items-center hover:bg-transparent hover:border-[#3682F7] hover:border hover:text-[#3682F7] justify-center"
                                                   type="submit"
                                                   disabled={isLoading}

                                                >
                                                    {isLoading ? <Dots className='m-auto' size={7} color={'#fff'}></Dots> : 'Update'}
                                                    
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}