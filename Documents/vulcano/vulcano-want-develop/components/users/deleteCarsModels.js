import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Dots } from 'react-activity';
import "react-activity/dist/Dots.css";


export default function DeleteCarsModels(props) {
    const {selectedBrand}= props
    console.log('DELETE ID: ' + selectedBrand);
    const [showModal, setShowModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false)
    const [name, setName] = useState('')
    const [additionalInfo, setAdditionalInfo] = useState('')

  const eliminarDatos = async (formData) => {
    
    setIsLoading(true)
        var data = new FormData();

    fetch("https://slogan.com.bo/vulcano/carsModels/deleteMobile/" + selectedBrand, {
      method: 'POST',
      body: data,
    })
      .then(response => response.json())
      .then(data => {
        console.log('VALOR ENDPOINTS: ', data);
        setIsLoading(false)
        if(data.status){
          setShowModal(false)
        } else {
          alert(JSON.stringify(data.errors, null, 4))
        }
        
      },
      (error) => {
        console.log(error)
      }
      )

  }
 
    return (
        <>
            <div>
                <button
                    className="w-[52px] h-[25px] text-[#FFFFFF] bg-[#3682F7] text-[12px] rounded-[4px] text-center items-center hover:bg-transparent hover:border-[#3682F7] hover:border hover:text-[#3682F7] justify-center"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    Delete
                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                            <div
                                className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                                onClick={() => setShowModal(false)}
                            />
                            <div className="flex items-center min-h-screen px-4 py-8">
                                <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg justify-center items-center flex flex-col ">
                                    <div
                                        className={'text-red hover:text-red'}
                                    >
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-16 h-16 text-[#FF0000]">
                                        <path strokeLinecap="round" strokeLinejoin="round" d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>


                                    </div>

                                    <div>
                                        <h1
                                            className="text-[32px]"
                                        >
                                            Are you sure {selectedBrand}?
                                        </h1>
                                    </div>

                                    <div
                                        className="flex flex-col justify-center items-center mt-[22px] "
                                    >
                                        <p
                                            className="text-[18px]  font-semibold "
                                        >
                                            Do you really want to delete these records?
                                        </p>
                                        <p
                                            className="text-[18px] font-light "
                                        >
                                            This process cannot be undone
                                        </p>
                                    </div>

                                    <div className="flex flex-row justify-between mt-[45px]">
                                        <div>
                                            <button
                                                className="w-[72px] h-[35px] border-[1px] border-[#3682F7] rounded-[7px] text-[#3682F7] hover:bg-[#3682F7] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                onClick={() =>
                                                    setShowModal(false)
                                                }
                                            >
                                                Cancel
                                            </button>
                                            <button
                                                className="w-[72px] h-[35px] border-[1px] bg-[#FF0000] rounded-[7px] text-[#FFFF] hover:border-[#FF0000] hover:bg-[#FFFF] hover:text-[#FF0000] text-[14px] mt-[3px]"
                                                onClick={() => {
                                                    eliminarDatos(),
                                                    setShowModal(false)
                                                }}
                                            >
                                                Delete
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </>
            ) : null}
        </>
    );
}