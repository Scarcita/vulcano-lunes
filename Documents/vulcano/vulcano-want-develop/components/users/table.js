import React from 'react';
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'
import Image from 'next/image'
import { useEffect, useState } from 'react'
import TooltipUsers from './tooltip';
//import Link from 'next/link';
//import TooltipClient from './tooltip';

const TableDatosUsers = (props) => {

    const {row, reloadUsers, setReloadUsers} = props
    const [isLoading, setIsLoading] = useState(true)

    const [data, setData] = useState([]);
    const headlist = [
        'Name', "Username", "Role",  ""
    ];

    useEffect(() => {
        setIsLoading(true)
        fetch('https://slogan.com.bo/vulcano/users/all')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    //console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })
    }, [reloadUsers])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 50 }}>
                    <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
                <div>
                    <TablaProductos data={data} headlist={headlist} reloadUsers={reloadUsers} setReloadUsers={setReloadUsers}/>
                    <TablaResponsive data={data} headlist={headlist} reloadUsers={reloadUsers} setReloadUsers={setReloadUsers}/>
                </div>
            </>
    )
}


const TablaProductos = (props) => {
    const { headlist, data, reloadUsers,  setReloadUsers } = props;

    return (

        <div className="rounded-[17px] bg-[#FFFF] hidden lg:block md:block ">
            <table className="w-full">
                <thead className='bg-[#3682F7] shadow'>
                    <tr>
                        {headlist.map((header, index) => <th key={index} className='text-[12px] font-semibold text-[#fff] text-center pt-[10px] pb-[10px]'>{header}</th>)}
                    </tr>
                </thead>

                <tbody>

                    {data.map(row =>

                        <tr key={row.id}>

                            <td>
                                <div className='text-[12px] self-center ml-[10px]'>
                                    {row.name}
                                </div>
                            </td>
                            <td>
                                <div className='text-[12px] self-center'>
                                    {row.username}
                                </div>
                            </td> 


                            <td >
                                <div className='text-center text-[12px]'>
                                    {row.role}
                                </div>
                            </td>

                            <td className='text-center'>
                                 <TooltipUsers
                                    row={row}
                                    name={row.name}
                                    username={row.username}
                                    role={row.role}
                                    reloadUsers={reloadUsers} 
                                    setReloadUsers={setReloadUsers}
                                 />
                            </td>

                        </tr>
                    )}


                </tbody>
            </table>
        </div>
    );
};


const TablaResponsive = (props) => {
    const { data, reloadUsers,  setReloadUsers  } = props;

    return (
        <div className='grid grid-cols-12'>
            <div className='col-span-12 md:col-span-12 lg:col-span-12 rounded-[10px] md:hidden'>
                
                {data.map(row =>
                    {          
                    return (
                    <div key={row.id} className='grid grid-cols-12 bg-[#fff] rounded-[15px] shadow-md pl-[10px] pb-[10px] mb-[15px]'>
                        <div className='col-span-12 md:col-span-12'>
                            <div className='grid grid-cols-12'>
                                <div className='col-span-10 md:col-span-12 pt-[10px] '>
                                    <div className='flex justify-between'>
                                        <div>
                                            <div className='text-[12px] self-center font-semibold'>
                                                {row.name}
                                            </div>
                                            <div className='text-[12px] self-center font-light'>
                                                {row.username}
                                            </div>
                                        </div>
                                        <div className='text-center text-[13px] font-semibold'>
                                            {row.role}
                                        </div>
                                    </div>
                                </div>
                                <div className='col-span-2 md:col-span-12'>
                                    <div className=' items-center text-center '>
                                        <TooltipUsers
                                            row={row}
                                            name={row.name}
                                            username={row.username}
                                            role={row.role}
                                            reloadUsers={reloadUsers} 
                                            setReloadUsers={setReloadUsers}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    )
                    }
                    )}
            </div>
        </div>

        
    );
};




export default TableDatosUsers;

