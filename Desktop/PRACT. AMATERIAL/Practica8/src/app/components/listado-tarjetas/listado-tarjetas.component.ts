import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { listadoTarjetasI } from '../usuario/usuario-interface';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';


 
  @Component({
    selector: 'app-listado-tarjetas',
    templateUrl: './listado-tarjetas.component.html',
    styleUrls: ['./listado-tarjetas.component.css']
  })
  

  export class ListadoTarjetasComponent implements OnInit, AfterViewInit{

    lisTarjetas: listadoTarjetasI[] = [
      {titulo: 'scarleth guzman', numeroTarjeta: 12548976, fechaExpiracion: 12/22, cvv: 12536},
      {titulo: 'sonia guzman', numeroTarjeta: 12548976, fechaExpiracion: 12/22, cvv: 12536},
      {titulo: 'vidal guzman', numeroTarjeta: 12548976, fechaExpiracion: 12/22, cvv: 12536},
      {titulo: 'noelia guzman', numeroTarjeta: 12548976, fechaExpiracion: 12/22, cvv: 12536},
      {titulo: 'celia guzman', numeroTarjeta: 12548976, fechaExpiracion: 12/22, cvv: 12536},
      {titulo: 'cristina guzman', numeroTarjeta: 12548976, fechaExpiracion: 12/22, cvv: 12536},
      
    ];

    displayedColumns: string[] = ['titulo', 'numeroTarjeta', 'fechaExpiracion', 'cvv', 'acciones'];
    dataSource = new MatTableDataSource (this.lisTarjetas);


    
   
  constructor() { }
  
  


  ngOnInit(): void {
    
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  @ViewChild(MatSort) sort!: MatSort;

  ngAfterViewInit(){
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort
  }
 
}
