import { Injectable } from '@angular/core';
import { listadoTarjetasI } from '../components/usuario/usuario-interface';





@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  lisTarjetas: listadoTarjetasI[] = [
    {titulo: 'scarleth guzman', numeroTarjeta: 12548976, fechaExpiracion: 12/22, cvv: 12536},
    {titulo: 'sonia guzman', numeroTarjeta: 12548976, fechaExpiracion: 12/22, cvv: 12536},
    {titulo: 'vidal guzman', numeroTarjeta: 12548976, fechaExpiracion: 12/22, cvv: 12536},
    {titulo: 'noelia guzman', numeroTarjeta: 12548976, fechaExpiracion: 12/22, cvv: 12536},
    {titulo: 'celia guzman', numeroTarjeta: 12548976, fechaExpiracion: 12/22, cvv: 12536},
    {titulo: 'cristina guzman', numeroTarjeta: 12548976, fechaExpiracion: 12/22, cvv: 12536},
    
  ];

  constructor() { }

  getTarjeta(): listadoTarjetasI[]
  {
    return this.lisTarjetas.slice();
  }

  eliminarTarjeta(titulo:string){
    this.lisTarjetas = this.lisTarjetas.filter(data =>{
      return data.titulo!==titulo;
    })
  }
}
