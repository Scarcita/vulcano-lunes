import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';

import ImagenFacebook from "../../../public/SocialMedia/Facebook.svg";
import logo from "../../../public/logo.svg"
import marca from "../../../public/marca.svg"
import editar from "../../../public/editar.svg"

import { useEffect, useState } from 'react'
import Tooltip from './tooltip';

const Table = (props) => {

    const {reloadAds} = props

    const [isLoading, setIsLoading] = useState(true)

    const [showDots, setShowDots] = useState(true);
    const [total, setTotal] = useState(0);
    const [data, setData] = useState([]);
    const headlist = [
        'Date', "Client", "Social Network", 'Post', 'Duration', "Amount", ""
    ];

    useEffect(() => {
        fetch('https://slogan.com.bo/roadie/clientsPlansPostsAds/all/')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    //console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

    }, [])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
                <div>
                    <TablaProductos data={data} headlist={headlist}/>
                </div>
            </>
    )
}


const TablaProductos = (props) => {
    const { headlist, data  } = props;

    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
            if(date[1] !== undefined){
                element['created_hour'] = date[1].split("+")[0];
            }
        });
        data = obj;
    };
    splitData(data);

    const splitStartDate = (obj) => {
        data.forEach(element => {
            let date = element.start_datetime.split("T");
            element.start_datetime = date[0];
            if(date[1] !== undefined){
                element['start_datetime_hour'] = date[1].split("+")[0];
            }
        });
        data = obj;
    };
    splitStartDate(data);

    const splitEndDate = (obj) => {
        data.forEach(element => {
            let date = element.end_datetime.split("T");
            element.end_datetime= date[0];
            if(date[1] !== undefined){
                element['end_datetime_hour'] = date[1].split("+")[0];
            }
        });
        data = obj;
    };
    splitEndDate(data);

    
    //const map1 = data.map(row => console.log(row));

    return (

        <div className="rounded-lg shadow hidden lg:block md:block mt-[20px]">
            <table className="w-full">
                <thead className='bg-[#FFFF] shadow'>
                    <tr>
                        {headlist.map(header => <th key={headlist.id} className='text-[12px] font-semibold text-[#000000] text-center pt-[10px] pb-[10px]'>{header}</th>)}
                    </tr>
                </thead>

                <tbody>

                    {data.map(row =>
                     {

                        const imgUrl = "";

                        if(row._matchingData.ClientsPlansPosts.social_network !== null){
                            switch (row._matchingData.ClientsPlansPosts.social_network) {
                                case 'Facebook': 
                                    imgUrl = '/SocialMedia/Facebook.svg'
                                    break;
                                case 'TikTok':
                                    imgUrl= '/SocialMedia/TikTok.svg'
                                    break;
                        
                                case 'Instagram':
                                    imgUrl = '/SocialMedia/Instagram.svg'
                                break;
                                case 'YouTube':
                                    imgUrl = '/SocialMedia/Youtube.svg'
                                break;
                                case 'Mailing':
                                    imgUrl = '/Plans/gmail.svg'
                                break;
                                case 'LinkedIn':
                                    imgUrl = '/SocialMedia/messenger.svg'
                                break;
                                case 'Twitter':
                                    imgUrl = '/SocialMedia/Twitter.svg'
                                break;
                                    
                                default:
                                    break;
                            }
                        }
                        return (

                            <tr key={row.id} >
                            <td className=' text-[14px] text-start '>
                                {row.created} {row.created_hour}
                            </td>
                            <td className='font-bold'>
                                <div className='flex flex-row'>
                                    {row._matchingData.ClientsPlansPosts.media_url !== null ?
                                        <Image
                                            className='rounded-full '
                                            src={row._matchingData.ClientsPlansPosts.media_url}
                                            alt=''
                                            layout='fixed'
                                            width={30}
                                            height={30}
                                        />
                                        : <></>
                                    }
                                    <div className='ml-[10px] text-[16px]'>
                                        {row._matchingData.ClientsPlansPosts.client_plan_id}
                                    </div>

                                </div>


                            </td>
                            <td className='text-center text-[14px] font-semibold'>
                                <div  className=''>
                                        {row._matchingData.ClientsPlansPosts.social_network !== null ?
                                            <Image
                                                src={imgUrl}
                                                alt='imagenFaceboock'
                                                layout='fixed'
                                                width={30}
                                                height={30}
                                            />
                                            : <></>
                                        }
                                    </div>

                            </td>
                            <td className=' text-[12px] text-center'>

                                <div className='flex flex-row'>
                                    <div className='mr-[5px]'>
                                        {row._matchingData.ClientsPlansPosts.media_url !== null ?
                                            <Image
                                                className='rounded-lg'
                                                src={row._matchingData.ClientsPlansPosts.media_url}
                                                alt=''
                                                layout='fixed'
                                                width={30}
                                                height={30}
                                            ></Image>
                                            : <></>
                                        }
                                    </div>

                                    {row._matchingData.ClientsPlansPosts.post_copy}
                                </div>
                            </td>
                            <td className=' text-[12px] text-center'>
                                <div>
                                    {row.start_datetime}

                                </div>
                                
                                <div>
                                    {row.end_datetime}
                                </div>

                            </td>
                            <td className=' text-center '>

                            {row.amount !== null ?
                                <div className=''>
                                <span className='text-[#643DCE] text-[20px] font-semibold'>{row.amount}</span>
                                <span className='text-[12px] text-[#643DCE]'> USD</span>
                                </div>
                                : <></>
                            }

                            </td>
                            <td>
                                <div className='text-center'>
                                    <Tooltip 
                                        row={row}
                                        id={row.id}
                                        title={row._matchingData.ClientsPlansPosts.title} 
                                        subtitle={row._matchingData.ClientsPlansPosts.subtitle} 
                                        social_network={row._matchingData.ClientsPlansPosts.social_network} 
                                        type={row._matchingData.ClientsPlansPosts.type} 
                                        start_datetime={row.start_datetime} 
                                        end_datetime={row.end_datetime} 
                                        amount={row.amount}
                                    />
                                </div>

                            </td>


                        </tr>

                        )
                    }

                    )}


                </tbody>
            </table>
        </div>
    );
};





export default Table;
