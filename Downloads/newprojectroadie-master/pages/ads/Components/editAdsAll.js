import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import ReactSelect from 'react-select';
import Image from 'next/image';



export default function ModalEditAdsAll(props) {
    const {
        row,
        id,
        title,
        subtitle,
        social_network, 
        type,
        start_datetime,
        end_datetime,
        amount } = props;
    const [isLoading, setIsLoading] = useState(false)
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    const [showEditModal, setShowEditModal] = useState(false);
    const [clientPlanPosts, setClientPlanPosts] = useState([])
    const [clientPlanPostId, setClientPlanPostId] = useState(null)
    const [titleForm, setTitleForm] = useState(title);
    const [subTitleForm, setSubTitleForm] = useState (subtitle)
    const [typeForm, setTypeForm] = useState (type);
    const [socialNetworkForm, setSocialNetworkForm] = useState (social_network);
    const [startDatetimeForm, setStartDatetimeForm] = useState (start_datetime);
    const [endDatetimeForm, setEndDatetimeForm] = useState (end_datetime);
    const [endtimeForm, setendTimeForm] = useState ();
    const [startTimeForm, setStartTimeForm] = useState();
    const [amountForm, setAmountForm] = useState(amount);

    const formatOptionLabel = ({ value, label, post }) => {
        
        const imgUrl = "";

        if(post.social_network !== null){
            switch (post.social_network) {
                case 'Facebook': 
                    imgUrl = '/SocialMedia/Facebook.svg'
                    break;
                case 'TikTok':
                    imgUrl= '/SocialMedia/TikTok.svg'
                    break;

                case 'Instagram':
                    imgUrl = '/SocialMedia/Instagram.svg'
                break;
                case 'YouTube':
                    imgUrl = '/SocialMedia/Youtube.svg'
                break;
                case 'Mailing':
                    imgUrl = '/Plans/gmail.svg'
                break;
                case 'LinkedIn':
                    imgUrl = '/SocialMedia/messenger.svg'
                break;
                case 'Twitter':
                    imgUrl = '/SocialMedia/Twitter.svg'
                break;
                    
                default:
                    break;
            }
        }

        const hasImage = false;
        if(post.media_url !== null && post.media_url.trim().length !== 0){
            hasImage = true;
        }

        return (
            <div className='grid grid-cols-12 align-center'>
                <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row mt-[5px] mb-[5px]'>
                    {hasImage ?
                        <div className='pr-[10px]'>
                            <Image
                                className='rounded-md'
                                src={post.media_url}
                                alt='media'
                                layout='fixed'
                                width={30}
                                height={30}
                            >
                            </Image>
                        </div>
                        : <></>
                    }
                    {hasImage ?
                        <div className='pr-[10px]'>
                            <Image
                                className='rounded-md'
                                src={imgUrl}
                                alt='media'
                                layout='fixed'
                                width={30}
                                height={30}
                            >
                           
                            </Image>
                        </div>
                        : <></>
                    }
                <div className={ hasImage ? 'col-span-8' : 'col-span-12' + ' text-[14px]'}>
                    {post.title}
                    <div>
                        <small className='text-gray-500 text-[12px]'>{post.subtitle}</small>
                    </div>
                </div>
                </div>
            </div>
        )
    };      
    
    useEffect(() => {

        fetch('https://slogan.com.bo/roadie/clientsPlansPosts/all/' + client_plan_id)
        .then(response => response.json())
        .then(data => {
            if (data.status) {

                var temp = [];
                data.data.map((result) => {
                    temp.push({value: result.id, label: result.title, post: result})
                })

                setClientPlanPosts(temp)
            } else {
                console.error(data.error)
            }
            setIsLoading(false)
        })

    }, [])


    const actualizarDatos = async () => {

        setIsLoading(true)
        var data = new FormData();

        data.append("client_plan_post_id", clientPlanPostId.value);
        data.append("start_datetime", startDatetimeForm + ' ' + startTimeForm);
        data.append("end_datetime", endDatetimeForm + ' ' + startTimeForm);
        data.append("amount", amountForm);
    
        fetch("http://slogan.com.bo/roadie/clientsPlansPostsAds/editMobile/" + row.id , {
          method: 'POST',
          body: data,
        })
          .then(response => response.json())
          .then(data => {
            console.log('VALOR ENDPOINTS EDIT: ', data);
            setIsLoading(false)
            if (data.status) { 
                setReloadAds(!reloadAds)
                setShowEditModal(false)
                console.log('edit endpoint: ' + data.status);
            } else {
                console.error(data.error)
            }
        })
    
      }

    const validationSchema = Yup.object().shape({
        // startDate: Yup.string()
        //     .required('Is required'),
        //     //.matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        // endDate: Yup.string()
        //     .required('Is required'),
        //     //.matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        // amount: Yup.string()
        //     .required('Is required')
        //     //.matches(/^[0-9]*$/, 'Ingrese un numero valido'),
    });

    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        console.log('onSubmit data:');
        console.log(data);
        actualizarDatos(data);
    }

    return (
        <div>
            <form onSubmit={handleSubmit(onSubmit)}>

                <div className="mt-[20px] grid grid-cols-12 gap-4">

                    <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Title</p>
                        <input name="title"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                        {...register('title')}
                        value={titleForm}
                        onChange={(e) => {
                            setTitleForm(e.target.value)
                        }}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.title?.message}</div>
                    </div>
                    <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>SubTitle</p>
                        <input name="subTitle"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                        {...register('subTitle')}
                        value={subTitleForm}
                        onChange={(e) => {
                            setSubTitleForm(e.target.value)
                        }}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.subTitle?.message}</div>
                    </div>

                    <div className='col-span-6 md:col-span-12 lg:col-span-6 '>
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Post</p>
                        <ReactSelect 
                            className="text-start"
                            defaultValue={clientPlanPostId}
                            onChange={setClientPlanPostId}
                            formatOptionLabel={formatOptionLabel}
                            options={clientPlanPosts} 
                        />
                    </div>


                    <div className='col-span-6 md:col-span-6 lg:col-span-6 '>
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Select Red Social</p>
                        <select
                            name="rrss"
                            {...register('rrss')}
                            value={socialNetworkForm}
                            onChange={(e) => {
                                setSocialNetworkForm(e.target.value)
                            }}
                            className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                        >
                        <option value="Facebook">Facebook</option>
                            <option value="Instagram">Instagram</option>
                            <option value="Mailing">Mailing</option>
                            <option value="Youtube">Youtube</option>
                            <option value="Tiktok">Tiktok</option>
                            <option value="LinkedInd">LinkedIn</option>
                            <option value="Twitter">Twitter</option>
                        </select>
                        <div className="text-[14px] text-[#FF0000]">{errors.rrss?.message}</div>
                    </div>

                    <div className='col-span-6 md:col-span-6 lg:col-span-6'>
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Type</p>
                        <select
                            name="type"
                            {...register('type')}
                            value={typeForm}
                            onChange={(e) => {
                                setTypeForm(e.target.value)
                            }}
                            className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                        >
                            <option value="Image">Image</option>
                            <option value="Album">Album</option>
                            <option value="Video">Video</option>
                            <option value="Story">Story</option>
                            <option value="Reel">Reel</option>
                        </select>
                        <div className="text-[14px] text-[#FF0000]">{errors.type?.message}</div>
                    </div>

                    <div className="col-span-6 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>amount</p>
                        <input name='amount' type="number" placeholder="1.0" step="0.01" min="0.01"  className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]'
                        {...register('amount')}
                        value={amountForm}
                        onChange={(e) => {
                            setAmountForm(e.target.value)
                        }}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.amount?.message}</div>
                    </div>

                    <div className="col-span-6 md:col-span-6 lg:col-span-6 ">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Select Date start </p>
                        <input name="startDate" type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]`}
                        {...register('startDate')}
                        value={startDatetimeForm}
                        onChange={(e) => {
                            setStartDatetimeForm(e.target.value)
                        }}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.startDate?.message}</div>
                    </div>
                    
                    <div className="col-span-6 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Select Time Star</p>
                        <input name="startTime"  type={'time'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]'
                        {...register('startTime')}
                        value={startTimeForm}
                        onChange={(e) => {
                            setStartTimeForm(e.target.value)
                        }}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.startTime?.message}</div>
                    </div>

                    <div className="col-span-6 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Select Date End </p>
                        <input name="endDate" type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]`}
                        {...register('endDate')}
                        value={endDatetimeForm}
                        onChange={(e) => {
                            setEndDatetimeForm(e.target.value)
                        }}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.endDate?.message}</div>
                    </div>
                    
                    <div className="col-span-6 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Select Time End</p>
                        <input name="endTime"  type={'time'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]'
                        {...register('endTime')}
                        value={endtimeForm}
                        onChange={(e) => {
                            setendTimeForm(e.target.value)
                        }}
                        />
                        <div className="text-[14px] text-[#FF0000]">{errors.endTime?.message}</div>
                    </div>

                </div>
                <div className="flex flex-row justify-between mt-[20px]">

                    <div>
                        <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                    </div>

                    <div>
                        <button
                            className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#643DCE] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                            onClick={() =>
                                setShowEditModal(false)
                            }
                            disabled={isLoading}
                        >
                            Cancel
                        </button>
                        <button
                            className="w-[100px] h-[35px] border-[1px] bg-[#643DCE] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                            type="submit"
                            disabled={isLoading}
                        >
                            {isLoading ? <Dots className='m-auto' size={7} color={'#fff'}></Dots> : 'Create'}
                        </button>
                    </div>
                </div>
                </form>
        </div>
    );
}