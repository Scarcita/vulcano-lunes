import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import TooltipPost from './tooltipPost';

const TablePost = (props) => {

    const {reloadPosts, setReloadPosts, client_plan_id} = props;

    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState([]);
    const headlist = [
        'Date', "SN", "Post", 'Status', ""
    ];

    useEffect(() => {
        setIsLoading(true)
        fetch('https://slogan.com.bo/roadie/clientsPlansPosts/all/' + client_plan_id)
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    setData(data.data)
                    console.log(data.data);
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

            console.warn('reloaddd' + reloadPosts);

    }, [reloadPosts])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
                <div>
                    <TablaProductos headlist={headlist} data={data} client_plan_id={client_plan_id } reloadPosts={reloadPosts} setReloadPosts={setReloadPosts}/>
                    <TablaResponsive headlist={headlist} data={data} client_plan_id={client_plan_id} reloadPosts={reloadPosts} setReloadPosts={setReloadPosts}/>
                </div>
            </>
    )
}


const TablaProductos = (props) => {
    const { headlist, data, reloadPosts, setReloadPosts, client_plan_id  } = props;

    return (

        <div className="rounded-lg shadow hidden lg:block md:block">
            <table className="w-full">
                <thead className='bg-[#FFFF] shadow'>
                    <tr>
                        {headlist.map((header, index) => <th key={index} className='text-[12px] font-semibold text-[#000000] text-center pt-[10px] pb-[10px]'>{header}</th>)}
                    </tr>
                </thead>

                <tbody>

                    {data.map(row =>
                        {

                            const imgUrl = "";

                            if(row.social_network !== null){
                                switch (row.social_network) {
                                    case 'Facebook': 
                                        imgUrl = '/SocialMedia/Facebook.svg'
                                        break;
                                    case 'TikTok':
                                        imgUrl= '/SocialMedia/TikTok.svg'
                                        break;
                                    case 'Instagram':
                                        imgUrl = '/SocialMedia/Instagram.svg'
                                    break;
                                    case 'YouTube':
                                        imgUrl = '/SocialMedia/Youtube.svg'
                                    break;
                                    case 'Mailing':
                                        imgUrl = '/Plans/gmail.svg'
                                    break;
                                    case 'LinkedIn':
                                        imgUrl = '/SocialMedia/messenger.svg'
                                    break;
                                    case 'Twitter':
                                        imgUrl = '/SocialMedia/Twitter.svg'
                                    break;
                                        
                                    default:
                                        break;
                                }
                            }
                            
                        return (
                            <tr key={row.id} className=''>
                                <td className='text-[12px] whitespace-nowrap text-center'>
                                    {row.planned_datetime}
                                </td>
                                <td className='text-[12px] text-center '>
                                    {row.social_network !== null ?
                                        <Image
                                            src={imgUrl}
                                            alt=''
                                            layout='fixed'
                                            width={30}
                                            height={30}
                                        />
                                        : <></>
                                    }

                                </td>

                                <td className=' text-center'>


                                    <div className='flex flex-row text-center '>
                                        <div className='mr-[7px] items-center self-center'>
                                            {row.media_url !== null && row.media_url.trim().length !== 0 ?
                                                <Image
                                                    className='rounded-md'
                                                    src={row.media_url}
                                                    alt=''
                                                    layout='fixed'
                                                    width={48}
                                                    height={48}
                                                />
                                                : <></>
                                            }
                                        </div>
                                        <div className='self-center'>
                                            <p className='text-[12px] text-left font-medium leading-2 whitespace-normal'>
                                                {row.title}
                                            </p>
                                            
                                            <p className='text-gray font-light leading-2 whitespace-normal text-[12px] text-left'>
                                                {row.subtitle}
                                            </p>
                                            <p className='text-gray font-light leading-2 whitespace-normal text-[12px] text-left'>
                                                {row.post_copy}
                                            </p>
                                            
                                        </div>
                                    </div>
                                </td>


                                <td className=''>
                                    <p className='bg-[#D9D9D9] rounded-[6px] text-[12px] text-center items-center self-center pt-[2px] pb-[2px] pl-[3px] pr-[3px]'>
                                        {row.status}
                                    </p>
                                </td>
                                <td className=' items-center text-center '>
                                    <TooltipPost
                                    row={row}
                                    client_plan_id={client_plan_id}
                                    id={row.id}
                                    title={row.title}
                                    subtitle={row.subtitle}
                                    planned_datetime={row.planned_datetime}
                                    type={row.type}
                                    post_copy={row.post_copy}
                                    social_network={row.social_network}
                                    instructions={row.instructions}
                                    reloadPosts={reloadPosts}
                                    setReloadPosts={setReloadPosts}
                                    />

                                </td>
                            


                            </tr>
                            )

                        }
                    )}
                </tbody>
            </table>
        </div>
    );
};



const TablaResponsive = (props) => {
    const { headlist, data, reloadPosts, setReloadPosts, client_plan_id  } = props;

    return (
        <div className='grid grid-cols-12 '>
            <div className='col-span-12 md:col-span-12 lg:col-span-12 rounded-[10px] md:hidden'>
                
                {data.map(row =>
                        {

                            const imgUrl = "";

                            if(row.social_network !== null){
                                switch (row.social_network) {
                                    case 'Facebook': 
                                        imgUrl = '/SocialMedia/Facebook.svg'
                                        break;
                                    case 'TikTok':
                                        imgUrl= '/SocialMedia/TikTok.svg'
                                        break;
                                    case 'Instagram':
                                        imgUrl = '/SocialMedia/Instagram.svg'
                                    break;
                                    case 'YouTube':
                                        imgUrl = '/SocialMedia/Youtube.svg'
                                    break;
                                    case 'Mailing':
                                        imgUrl = '/Plans/gmail.svg'
                                    break;
                                    case 'LinkedIn':
                                        imgUrl = '/SocialMedia/messenger.svg'
                                    break;
                                    case 'Twitter':
                                        imgUrl = '/SocialMedia/Twitter.svg'
                                    break;
                                        
                                    default:
                                        break;
                                }
                            }

                            if(row.planned_datetime !== null && row.planned_datetime !== undefined){
                                var eventdate = row.planned_datetime;
                                var splitdate = eventdate.split('-');
                                //console.log(splitdate);
                                var day = splitdate[2];
                                var year = splitdate[0];
                            }


                            if(row.planned_datetime !== null && row.planned_datetime !== undefined){
                                var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                                var d = new Date(row.planned_datetime);
                                var dayName = days[d.getDay()];
                            }
                    
                    
                        
                            if(row.planned_datetime !== null && row.planned_datetime !== undefined){
                                var months = ['Jan.', 'Feb. ', 'Mar.', 'Apr.', 'May', 'Jun.', 'Jul.','Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.'];
                                var m = new Date(row.planned_datetime);
                                var monthName = months[m.getMonth()];
                            }
                              
                            
                        return (
                        <div key={row.id} className='grid grid-cols-12 bg-[#fff] rounded-[15px] shadow-md pl-[10px] pb-[10px] mb-[15px]'>
                            <div className='col-span-12 md:col-span-12'>
                                <div className='grid grid-cols-12'>
                                    <div className='col-span-10 md:col-span-12 pt-[10px]'>
                                        <div className='grid grid-cols-12 gap-3'>
                                            <div className='col-span-6 md:col-span-6 flex flex-row justify-between'>
                                                <div className='text-left'>
                                                    <p className='text-[26px] font-bold text-center'>{day}</p>
                                                    
                                                </div>
                                                <div className='text-end'>
                                                    <p className='text-[14px] text-[#582BE7]  leading-4 whitespace-normal'>{dayName}</p>
                                                    <p className='text-[16px] font-semibold leading-4 whitespace-normal'>{monthName} {year}</p>
                                                    
                                                </div>

                                            </div>
                                            <div className='col-span-6 md:col-span-6'>
                                            
                                                <div className=' text-[12px] text-end self-center'>
                                                    {row.status}
                                                </div>
                                        
                                                <div className='text-end'>
                                                    {row.social_network !== null ?
                                                        <Image
                                                            src={imgUrl}
                                                            alt=''
                                                            layout='fixed'
                                                            width={30}
                                                            height={30}
                                                        />
                                                        : <></>
                                                    }
                                                </div>

                                            </div>

                                            <div className='col-span-12 md:col-span-12 mr-[10px]'>
                                                <div className='flex flex-row text-center '>
                                                    <div className='mr-[7px]'>
                                                        {row.media_url !== null && row.media_url.trim().length !== 0 ?
                                                            <Image
                                                                className='rounded-md'
                                                                src={row.media_url}
                                                                alt=''
                                                                layout='fixed'
                                                                width={30}
                                                                height={30}
                                                            />
                                                            : <></>
                                                        }
                                                    </div>
                                                    <div>
                                                        <p className='text-[10px] text-left font-medium leading-2 whitespace-normal'>
                                                            {row.title}
                                                        </p>
                                                        
                                                        <p className='text-gray font-light leading-2 whitespace-normal text-[8px] text-left'>
                                                            {row.subtitle}
                                                        </p>
                                                        <p className='text-gray font-light leading-2 whitespace-normal text-[8px] text-left'>
                                                            {row.post_copy}
                                                        </p>
                                                        
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-span-2 md:col-span-12'>
                                        <div className=' items-center text-center '>
                                            <TooltipPost
                                            row={row}
                                            client_plan_id={client_plan_id}
                                            id={row.id}
                                            title={row.title}
                                            subtitle={row.subtitle}
                                            type={row.type}
                                            planned_datetime={row.planned_datetime}
                                            post_copy={row.post_copy}
                                            social_network={row.social_network}
                                            instructions={row.instructions}
                                            reloadPosts={reloadPosts}
                                            setReloadPosts={setReloadPosts}
                                            />

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            )
                        }
                    )}
            </div>
        </div>

        
    );
};




export default TablePost;