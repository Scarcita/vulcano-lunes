import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { useRouter } from 'next/router'
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";



export default function ModalPost(props) {


    const { reloadPosts, setReloadPosts, client_plan_id} = props;

    const [showModal, setShowModal] = useState(false);

    const [isLoading, setIsLoading] = useState(false);

    const [plannedDateTime, setPlannedDateTime] = useState('')
    const [time, setTime] = useState('')
    const [title, setTitle] = useState('')
    const [subtitle, setSubTitle] = useState('')
    const [instructions, setInstructions] = useState('')
    const [postCopy, setPostCopy] = useState('')

    const clearForm = () => {
        setPlannedDateTime('')
        setTitle('')
        setSubTitle('')
        setInstructions('')
        setPostCopy('')
        // setMediaUrl('')
    }
   
    const registerPost = async (formData) => {

        setIsLoading(true)
        var data = new FormData();

        //console.log(formData.mediaUrl);
    
        data.append("client_plan_id", client_plan_id);
        data.append("social_network", formData.socialNetwork);
        data.append("type", formData.type);
        data.append("planned_datetime", plannedDateTime);
        data.append("title", title);
        data.append("subtitle", subtitle);
        data.append("instructions", instructions);
        data.append("post_copy", postCopy);
        
        if(formData.mediaUrl.length !== 0){
            data.append("media_url", formData.mediaUrl[0]);
        }
        // data.append("media_url", mediaUrl);
    
        fetch("https://slogan.com.bo/roadie/clientsPlansPosts/addMobile", {
          method: 'POST',
          body: data,
        })
          .then(response => response.json())
          .then(data => {
              //console.log('VALOR ENDPOINTS: ', data);

              setIsLoading(false)
              if(data.status){
                clearForm()
                setReloadPosts(!reloadPosts)
                setShowModal(false)
                //IMPLEMENTAR RECARGA DE DATOS (COMPARTIR FUNCION DE PANTALLA AL MODAL)
              } else {
                alert(JSON.stringify(data.errors, null, 4))
              }
              
            },
            (error) => {
              console.log(error)
            }
          )
    
      }
    
    
    const validationSchema = Yup.object().shape({
        title: Yup.string()
        .required('title is required'),

        subtitle: Yup.string()
            .required("subtitle is required"),
        // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        // mediaUrl: Yup.string()
        //     .required('mediaUrl is required'),
        //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        socialNetwork: Yup.string()
            .required('socialNetwork is required')
            .oneOf(['Facebook', 'Instagram', 'Mailing', 'YouTube', 'TikTok', 'LinkedIn', 'Twitter']),
        // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        type: Yup.string()
            .required('type is required'),
        //   //.min(6, 'minimo 6 caracteres'),
        // //.matches(/[a-zA-Z0-9]/, 'solo numeros y letras'),
    
        plannedDateTime: Yup.string()
            .required('plannedDateTime is required'),
    
        // instructions: Yup.string()
        //     .required('instructions is required'),
    
        // postCopy: Yup.string()
        //     .required('postCopy is required'),

    
        });
        const formOptions = { resolver: yupResolver(validationSchema) };
    
        // get functions to build form with useForm() hook
        const { register, handleSubmit, reset, formState } = useForm(formOptions);
        const { errors } = formState;
    
        function onSubmit(data) {
            alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
            console.log('string is NOT empty')
            registerPost(data)
            return false;
            
        }
    

    



    return (
        <>
            <div>
                <button
                    className="w-[112px] h-[26px] md:w-[200px] md:h-[48px] lg:w-[160px] lg:h-[35px] bg-[#582BE7] text-[#FFFFFF] font-semibold text-[14px] rounded-[25px] text-center items-center hover:bg-[#fff] hover:border-[#582BE7] hover:border-[2px] hover:text-[#582BE7] justify-center"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    Create post
                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                <div>
                                    <h4 className="text-lg font-medium text-gray-800">
                                        CREATE POST
                                    </h4>

                                </div>

                                <div>
                                    {/* {client_plan_id } */}
                                    <form onSubmit={handleSubmit(onSubmit)} >

                                        <div className="mt-[20px] grid grid-cols-12 gap-4">

                                            <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Title</p>
                                                <input name="text"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                                                {...register('title')}
                                                value={title}
                                                onChange={(e) => {
                                                    setTitle(e.target.value)
                                                }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.title?.message}</div>
                                            </div>
                                            <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>SubTitle</p>
                                                <input name="text"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                                                {...register('subtitle')}
                                                value={subtitle}
                                                onChange={(e) => {
                                                    setSubTitle(e.target.value)
                                                }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.subtitle?.message}</div>
                                            </div>

                                            <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Image</p>
                                                <input 
                                                    // name="mediaUrl"  
                                                    type={'file'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pt-[7px] text-[12px]'
                                                {...register('mediaUrl')}
                                                // value={mediaUrl}
                                                // onChange={(e) => {
                                                //     setMediaUrl(e.target.value)
                                                // }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.mediaUrl?.message}</div>
                                            </div>
                                            
                                            <div className='col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]'>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Red Social</p>
                                                <select
                                                    {...register('socialNetwork')}
                                                    name="socialNetwork"
                                                    className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                >
                                                    <option value="Facebook">Facebook</option>
                                                    <option value="Instagram">Instagram</option>
                                                    <option value="Mailing">Mailing</option>
                                                    <option value="YouTube">YouTube</option>
                                                    <option value="TikTok">TikTok</option>
                                                    <option value="LinkedIn">LinkedIn</option>
                                                    <option value="Twitter">Twitter</option>
                                                </select>
                                                <div className="text-[14px] text-[#FF0000]">{errors.socialNetwork?.message}</div>
                                            </div>

                                            <div className='col-span-12 md:col-span-6 lg:col-span-12'>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Type</p>
                                                <select
                                                    {...register('type')}
                                                    name="type"
                                                    className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                >
                                                    <option value="Image">Image</option>
                                                    <option value="Album">Album</option>
                                                    <option value="Video">Video</option>
                                                    <option value="Story">Story</option>
                                                    <option value="Reel">Reel</option>
                                                </select>
                                                <div className="text-[14px] text-[#FF0000]">{errors.type?.message}</div>
                                            </div>
                                            <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Date</p>
                                                <input name="plannedDateTime" type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`}
                                                {...register('plannedDateTime')}
                                                value={plannedDateTime}
                                                onChange={(e) => {
                                                    setPlannedDateTime(e.target.value)
                                                }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.plannedDateTime?.message}</div>
                                            </div>
                                            <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Time</p>
                                                <input name="time" type="time" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`}
                                                {...register('time')}
                                                value={time}
                                                onChange={(e) => {
                                                    setTime(e.target.value)
                                                }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.time?.message}</div>
                                            </div>
                                            
                                            <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Indications</p>
                                                <input name="instructions"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                                                {...register('instructions')}
                                                value={instructions}
                                                onChange={(e) => {
                                                    setInstructions(e.target.value)
                                                }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.instructions?.message}</div>
                                            </div>
                                            <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Post_copy</p>
                                                <input name="postCopy"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                                                {...register('postCopy')}
                                                value={postCopy}
                                                onChange={(e) => {
                                                    setPostCopy(e.target.value)
                                                }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.postCopy?.message}</div>
                                            </div>




                                        </div>

                                        <div className="flex flex-row justify-between mt-[20px]">

                                            <div>
                                                <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                            </div>

                                            <div>
                                                <button
                                                    className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                    onClick={() =>
                                                        setShowModal(false)
                                                    }
                                                    disabled={isLoading}
                                                >
                                                    Cancel
                                                </button>
                                                <button
                                                    className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                                    type={'submit'}
                                                    disabled={isLoading}
                                                >
                                                    {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Create'}
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}