import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import { useRouter } from 'next/router'
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';
import { useEffect, useState } from 'react'
import TooltipAds from './tooltipAds';

import ImgFacebook from "../../../public/SocialMedia/Facebook.svg";
import logo from "../../../public/ClientPlan/logo.svg"
import marca from "../../../public/ClientPlan/marca.svg"
import editar from "../../../public/ClientPlan/editar.svg"


const TableAds = (props) => {

    const {reloadAds, setReloadAds, client_plan_id} = props
    
    const [isLoading, setIsLoading] = useState(true);
    const [total, setTotal] = useState(0);
    const [data, setData] = useState([]);
    const headlist = [
        'Date', "SN", "Post", 'Status', 'Amount', ""
    ];


    useEffect(() => {

        setIsLoading(true)

        fetch('https://slogan.com.bo/roadie/clientsPlansPostsAds/all/' + client_plan_id)
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    setData(data.data)
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

        console.warn('reloaddd' + reloadAds);

    }, [reloadAds])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
                <div>
                    <TablaProductos data={data} headlist={headlist} client_plan_id={client_plan_id } reloadAds={reloadAds} setReloadAds={setReloadAds}/>
                    <TablaResponsive headlist={headlist} data={data} client_plan_id={client_plan_id } reloadAds={reloadAds} setReloadAds={setReloadAds} />
                </div>
            </>
    )
}


const TablaProductos = (props) => {
    const { headlist, data, reloadAds, setReloadAds, client_plan_id } = props;

    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
        });
        data = obj;
    };
    splitData(data);


    const splitStartDate = (obj) => {
        data.forEach(element => {
            let date = element.start_datetime.split("T");
            element.start_datetime= date[0];
            if(date[1] !== undefined){
                element['end_datetime_hour'] = date[1].split("+")[0];
            }
        });
        data = obj;
    };
    splitStartDate(data);

    const splitEndDate = (obj) => {
        data.forEach(element => {
            let date = element.end_datetime.split("T");
            element.end_datetime= date[0];
            if(date[1] !== undefined){
                element['end_datetime_hour'] = date[1].split("+")[0];
            }
        });
        data = obj;
    };
    splitEndDate(data);

    return (

        <div className="rounded-lg shadow hidden lg:block md:block mt-[20px]">
            <table className="w-full">
                <thead className='bg-[#FFFF] shadow'>
                    <tr>
                        {headlist.map(header => <th key={headlist.id} className='text-[12px] font-semibold text-[#000000] text-center pt-[10px] pb-[10px]'>{header}</th>)}
                    </tr>
                </thead>

                <tbody>

                    {data.map(row =>
                     {

                        const imgUrl = "";

                        if(row._matchingData.ClientsPlansPosts.social_network !== null){
                            switch (row._matchingData.ClientsPlansPosts.social_network) {
                                case 'Facebook': 
                                    imgUrl = '/SocialMedia/Facebook.svg'
                                    break;
                                case 'TikTok':
                                    imgUrl= '/SocialMedia/TikTok.svg'
                                    break;
                        
                                case 'Instagram':
                                    imgUrl = '/SocialMedia/Instagram.svg'
                                break;
                                case 'YouTube':
                                    imgUrl = '/SocialMedia/Youtube.svg'
                                break;
                                case 'Mailing':
                                    imgUrl = '/Plans/gmail.svg'
                                break;
                                case 'LinkedIn':
                                    imgUrl = '/SocialMedia/messenger.svg'
                                break;
                                case 'Twitter':
                                    imgUrl = '/SocialMedia/Twitter.svg'
                                break;
                                    
                                default:
                                    break;
                            }
                        }

                        return (
                            <tr key={row.id} >
                            <td className='text-center text-[10px]'>
                                <p>
                                    {row.start_datetime}
                                </p>
                                <p>
                                    {row.end_datetime}
                                </p>
                            </td>
                           
                            <td className=' text-center '>
                                    {row._matchingData.ClientsPlansPosts.social_network !== null ?
                                        <Image
                                            src={imgUrl}
                                            alt='ImgFacebook'
                                            layout='fixed'
                                            width={30}
                                            height={30}
                                        />
                                        : <></>
                                    }

                            </td>
                            <td className='text-center '>

                                <div className='flex flex-row text-center '>
                                    <div className='mr-[7px]'>
                                        {row._matchingData.ClientsPlansPosts.media_url !== null && row._matchingData.ClientsPlansPosts.media_url.trim().length !== 0 ?
                                            <Image
                                                className='rounded-md bg-[#fff]'
                                                src={row._matchingData.ClientsPlansPosts.media_url}
                                                alt='media'
                                                layout='fixed'
                                                width={30}
                                                height={30}
                                            >
                                            </Image>
                                            : <></>
                                        }
                                    </div>
                                    <div className='self-center'>
                                        <p className='text-[10px] text-left font-medium leading-2 whitespace-normal'>
                                            {row._matchingData.ClientsPlansPosts.title}
                                        </p>
                                        <p className='text-gray font-light leading-2 whitespace-normal text-[8px] text-left'>
                                            {row._matchingData.ClientsPlansPosts.subtitle}
                                        </p>
                                        <p className='text-gray font-light leading-2 whitespace-normal text-[8px] text-left'>
                                            {row._matchingData.ClientsPlansPosts.post_copy}
                                        </p>
                                    </div>
                                </div>


                            </td>


                            <td>
                                <p className='bg-[#D9D9D9] rounded-[6px] text-[12px] text-center items-center self-center pt-[2px] pb-[2px] pl-[3px] pr-[3px]'>
                                    {row._matchingData.ClientsPlansPosts.status}
                                </p>

                            </td>
                            <td className='text-center text-[12px] font-semibold text-[#643DCE]'>
                                {row.amount !== null ?

                                    <div>
                                        {row.amount} USD
                                    </div>
                                    : <></>

                                }
                            </td>

                            <td className='text-center'>
                                <TooltipAds
                                    row={row}
                                    client_plan_id={client_plan_id}
                                    id={row.id}
                                    startDatetime={row.start_datetime}
                                    endDatetime={row.end_datetime}
                                    amount={row.amount}
                                    reloadAds={reloadAds} 
                                    setReloadAds={setReloadAds}
                                />
                            </td>
                            

                        </tr>

                        )
                    }

                        
                    )}


                </tbody>
            </table>
        </div>
    );
};


const TablaResponsive = (props) => {
    const { headlist, data, reloadAds, setReloadAds, client_plan_id } = props;

    return (
        <div className='grid grid-cols-12 mt-[20px]'>
            <div className='col-span-12 md:col-span-12 lg:col-span-12 rounded-[10px] md:hidden'>
                
                {data.map(row =>
                        {

                            const imgUrl = "";

                            if(row._matchingData.ClientsPlansPosts.social_network !== null){
                                switch (row._matchingData.ClientsPlansPosts.social_network) {
                                    case 'Facebook': 
                                        imgUrl = '/SocialMedia/Facebook.svg'
                                        break;
                                    case 'TikTok':
                                        imgUrl= '/SocialMedia/TikTok.svg'
                                        break;
                                    case 'Instagram':
                                        imgUrl = '/SocialMedia/Instagram.svg'
                                    break;
                                    case 'YouTube':
                                        imgUrl = '/SocialMedia/Youtube.svg'
                                    break;
                                    case 'Mailing':
                                        imgUrl = '/Plans/gmail.svg'
                                    break;
                                    case 'LinkedIn':
                                        imgUrl = '/SocialMedia/messenger.svg'
                                    break;
                                    case 'Twitter':
                                        imgUrl = '/SocialMedia/Twitter.svg'
                                    break;
                                        
                                    default:
                                        break;
                                }
                            }

                            if(row.start_datetime !== null && row.start_datetime !== undefined){
                                var eventdate = row.start_datetime;
                                var splitdate = eventdate.split('-');
                                //console.log(splitdate);
                                var day = splitdate[2];
                                var year = splitdate[0];
                            }


                            if(row.start_datetime !== null && row.start_datetime !== undefined){
                                var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                                var d = new Date(row.start_datetime);
                                var dayName = days[d.getDay()];
                            }
                    
                    
                        
                            if(row.start_datetime !== null && row.start_datetime !== undefined){
                                var months = ['Jan.', 'Feb. ', 'Mar.', 'Apr.', 'May', 'Jun.', 'Jul.','Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.'];
                                var m = new Date(row.start_datetime);
                                var monthName = months[m.getMonth()];
                            }

                            if(row.end_datetime !== null && row.end_datetime !== undefined){
                                var eventdate = row.end_datetime;
                                var splitdate = eventdate.split('-');
                                //console.log(splitdate);
                                var dayEnd = splitdate[2];
                                var yearEnd = splitdate[0];
                            }


                            if(row.end_datetime !== null && row.end_datetime !== undefined){
                                var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                                var d = new Date(row.end_datetime);
                                var dayNameEnd = days[d.getDay()];
                            }
                    
                    
                        
                            if(row.end_datetime !== null && row.end_datetime !== undefined){
                                var months = ['Jan.', 'Feb. ', 'Mar.', 'Apr.', 'May', 'Jun.', 'Jul.','Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.'];
                                var m = new Date(row.end_datetime);
                                var monthNameEnd = months[m.getMonth()];
                            }
                              
                            
                        return (
                        <div key={row.id} className='grid grid-cols-12 bg-[#fff] rounded-[15px] shadow-md pl-[10px] pb-[10px] mb-[15px]'>
                            <div className='col-span-12 md:col-span-12'>
                                <div className='grid grid-cols-12'>
                                    <div className='col-span-10 md:col-span-12 pt-[10px]'>
                                        <div className='grid grid-cols-12 gap-1'>
                                            <div className='col-span-12 md:col-span-12'>
                                                <div className='grid grid-cols-12 gap-4 mt-[5px] mb-[5px]'>
                                                    <div className='col-span-6 md:col-span-6 lg:col-span-6 flex flex-row justify-between'>
                                                        <div>
                                                            <p className='text-[30px] font-bold pr-[5px]'>{day}</p> 
                                                        </div>
                                                        <div className='self-center items-center'>
                                                            <p className='text-[14px] text-[#582BE7] text-right leading-4 whitespace-normal'>{dayName}</p>
                                                            <p className='text-[16px] font-semibold text-right leading-4 whitespace-normal'>{monthName} {year}</p>
                                                            
                                                        </div>
                                                    </div>
                                                    <div className='col-span-6 md:col-span-6 lg:col-span-6 flex flex-row justify-between'>
                                                        <div>
                                                            <p className='text-[30px] font-bold text-left pr-[5px]'>{dayEnd}</p>
                                                            
                                                        </div>
                                                        <div className='self-center items-center'>
                                                            <p className='text-[14px] text-[#582BE7] text-right leading-4 whitespace-normal'>{dayNameEnd}</p>
                                                            <p className='text-[16px] font-semibold text-right leading-4 whitespace-normal'>{monthNameEnd} {yearEnd}</p>
                                                            
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                                <div className='grid grid-cols-12 gap-3'>
                                                    <div className='col-span-7 md:col-span-7'>
                                                        <div className='flex flex-row text-center '>
                                                            <div className='mr-[7px]'>
                                                                {row._matchingData.ClientsPlansPosts.media_url !== null && row._matchingData.ClientsPlansPosts.media_url.trim().length !== 0 ?
                                                                    <Image
                                                                        className='rounded-md'
                                                                        src={row._matchingData.ClientsPlansPosts.media_url}
                                                                        alt=''
                                                                        layout='fixed'
                                                                        width={30}
                                                                        height={30}
                                                                    />
                                                                    : <></>
                                                                }
                                                            </div>
                                                            <div>
                                                                <p className='text-[10px] text-left font-medium leading-2 whitespace-normal'>
                                                                    {row._matchingData.ClientsPlansPosts.title}
                                                                </p>
                                                                
                                                                <p className='text-gray font-light leading-2 whitespace-normal text-[8px] text-left'>
                                                                    {row._matchingData.ClientsPlansPosts.subtitle}
                                                                </p>
                                                                <p className='text-gray font-light leading-2 whitespace-normal text-[8px] text-left'>
                                                                    {row._matchingData.ClientsPlansPosts.post_copy}
                                                                </p>
                                                                
                                                            </div>
                                                    </div>

                                                    </div>
                                    
                                                    <div className='col-span-5 md:col-span-5'>
                                                            
                                                        <div className=' text-[12px] text-end self-center'>
                                                            {row._matchingData.ClientsPlansPosts.status}
                                                        </div>
                                                    
                                                        <div className='text-end mt-[5px]'>
                                                            {row._matchingData.ClientsPlansPosts.social_network !== null ?
                                                                <Image
                                                                    src={imgUrl}
                                                                    alt=''
                                                                    layout='fixed'
                                                                    width={30}
                                                                    height={30}
                                                                />
                                                                : <></>
                                                            }
                                                        </div>
                    
                                                    </div>

                                                    
                                                </div>

                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div className='col-span-2 md:col-span-12'>
                                        <div className=' items-center text-center '>
                                            <TooltipAds
                                                row={row}
                                                client_plan_id={client_plan_id}
                                                id={row.id}
                                                startDatetime={row.start_datetime}
                                                endDatetime={row.end_datetime}
                                                amount={row.amount}
                                                reloadAds={reloadAds} 
                                                setReloadAds={setReloadAds}
                                            />

                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                            )
                        }
                    )}
            </div>
        </div>

        
    );
};


export default TableAds;