import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import ReactSelect from 'react-select';
import imagenFaceboock from "../../../public/Dashhoard/facebook.svg";


export default function ModalAllPost(props) {

  const {id, title, subtitle, type, planned_datetime, post_copy, social_network, instructions} = props;
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [titleForm, setTitleForm] = useState(title)
  const [subtitleForm, setSubtitleForm] = useState(subtitle)
  const [typeForm, setTypeForm] = useState(type)
  const [plannedDatetimeForm, setPlannedDatetimeForm] = useState (planned_datetime)
  const [postCopyForm, setPostCopyForm] = useState (post_copy);
  //const [imgUrlForm, setImgUrlForm] = useState (imgUrl);
  const [socialNetworkForm, setSocialNetworkForm] = useState (social_network);
  const [instructionsForm, setInstructionsForm] = useState (instructions);


  const actualizarDatos = async (formData) => {

      setIsLoading(true)
      var data = new FormData();

      //console.log(formData.mediaUrl);
  
      data.append("client_plan_id", client_plan_id);
      data.append("social_network", formData.social_network);
      data.append("type", formData.type);
      data.append("planned_datetime", plannedDatetimeForm);
      data.append("title", titleForm);
      data.append("subtitle", subtitleForm);
      data.append("instructions", instructionsForm);
      data.append("post_copy", postCopyForm);
      
      if(formData.mediaUrl.length !== 0){
          data.append("media_url", formData.mediaUrl[0]);
      }
  
      fetch("http://slogan.com.bo/roadie/clientsPlansExtras/editMobile/" + id, {
        method: 'POST',
        body: data,
      })
        .then(response => response.json())
        .then(data => {
          console.log('VALOR ENDPOINTS EDIT: ', data);
          setIsLoading(false)
          if (data.status) { 
              setReloadPosts(!reloadPosts)
              setShowModal(false)
              console.log('edit endpoint: ' + data.status);
          } else {
              console.error(data.error)
          }
      })
  
    }

  
  const validationSchema = Yup.object().shape({
  title: Yup.string()
  .required('title is required'),

  subtitle: Yup.string()
      .required("subtitle is required"),
  // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

  // mediaUrl: Yup.string()
  //     .required('mediaUrl is required'),
  //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

  socialNetwork: Yup.string()
      .required('socialNetwork is required')
      .oneOf(['Facebook', 'Instagram', 'Mailing', 'YouTube', 'TikTok', 'LinkedIn', 'Twitter']),
  // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

  type: Yup.string()
      .required('type is required'),
  //   //.min(6, 'minimo 6 caracteres'),
  // //.matches(/[a-zA-Z0-9]/, 'solo numeros y letras'),

  plannedDateTime: Yup.string()
      .required('plannedDateTime is required'),

  // instructions: Yup.string()
  //     .required('instructions is required'),

  // postCopy: Yup.string()
  //     .required('postCopy is required'),


  });
  const formOptions = { resolver: yupResolver(validationSchema) };

  // get functions to build form with useForm() hook
  const { register, handleSubmit, reset, formState } = useForm(formOptions);
  const { errors } = formState;

  function onSubmit(data) {
      alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
      console.log('string is NOT empty')
      actualizarDatos(data)
      return false;
      
  }

    return (
        <>
            <div>
                <button
                    className="w-full h-[48px] bg-[#582BE7] text-[#FFFFFF] font-semibold text-[18px] rounded-[25px] text-center items-center hover:bg-[#fff] hover:border-[#582BE7] hover:border-[2px] hover:text-[#582BE7]"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    Edit Post
                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                <div>
                                    <h4 className="text-lg font-medium text-gray-800">
                                        EDIT POST
                                    </h4>

                                </div>

                                <div>
                                <form onSubmit={handleSubmit(onSubmit)} >

                                  <div className="mt-[20px] grid grid-cols-12 gap-4">

                                      <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                          <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Title</p>
                                          <input name="text"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]'
                                          {...register('title')}
                                          value={titleForm}
                                          onChange={(e) => {
                                              setTitleForm(e.target.value)
                                          }}
                                          />
                                          <div className="text-[14px] text-[#FF0000]">{errors.title?.message}</div>
                                      </div>
                                      <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                          <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>SubTitle</p>
                                          <input name="text"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]'
                                          {...register('subtitle')}
                                          value={subtitleForm}
                                          onChange={(e) => {
                                              setSubtitleForm(e.target.value)
                                          }}
                                          />
                                          <div className="text-[14px] text-[#FF0000]">{errors.subtitle?.message}</div>
                                      </div>

                                      <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                          <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Select Image</p>
                                          <input 
                                              type={'file'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pt-[7px] text-[12px] pr-[10px] text-ellipsis'
                                          {...register('mediaUrl')}
                                          />
                                          <div className="text-[14px] text-[#FF0000]">{errors.mediaUrl?.message}</div>
                                      </div>

                                      <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                          <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Post_copy</p>
                                          <input name="postCopy"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]'
                                          {...register('postCopy')}
                                          value={postCopyForm}
                                          onChange={(e) => {
                                              setPostCopyForm(e.target.value)
                                          }}
                                          />
                                          <div className="text-[14px] text-[#FF0000]">{errors.postCopy?.message}</div>
                                      </div>
                                      
                                      <div className='col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]'>
                                          <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Select Red Social</p>
                                          <select
                                              {...register('socialNetwork')}
                                              name="socialNetwork"
                                              value={socialNetworkForm}
                                              onChange={(e) => {
                                                  setSocialNetworkForm(e.target.value)
                                              }}
                                              className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB]  rounded-[10px] pl-[5px] text-[12px]"
                                          >
                                              <option value="Facebook">Facebook</option>
                                              <option value="Instagram">Instagram</option>
                                              <option value="Mailing">Mailing</option>
                                              <option value="YouTube">YouTube</option>
                                              <option value="TikTok">TikTok</option>
                                              <option value="LinkedIn">LinkedIn</option>
                                              <option value="Twitter">Twitter</option>
                                          </select>
                                          <div className="text-[14px] text-[#FF0000]">{errors.socialNetwork?.message}</div>
                                      </div>

                                      <div className='col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]'>
                                          <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Type</p>
                                          <select
                                              {...register('type')}
                                              name="type"
                                              value={typeForm}
                                              onChange={(e) => {
                                                  setTypeForm(e.target.value)
                                              }}
                                              className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                          >
                                              <option value="Image">Image</option>
                                              <option value="Album">Album</option>
                                              <option value="Video">Video</option>
                                              <option value="Story">Story</option>
                                              <option value="Reel">Reel</option>
                                          </select>
                                          <div className="text-[14px] text-[#FF0000]">{errors.type?.message}</div>
                                      </div>
                                      <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                          <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Select Date</p>
                                          <input name="plannedDateTime" type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`}
                                          {...register('plannedDateTime')}
                                          value={plannedDatetimeForm}
                                          onChange={(e) => {
                                              setPlannedDatetimeForm(e.target.value)
                                          }}
                                          />
                                          <div className="text-[14px] text-[#FF0000]">{errors.plannedDateTime?.message}</div>
                                      </div>
                                      <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                          <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Select Time</p>
                                          <input name="time" type="time" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`}
                                          {...register('time')}
                                          // value={time}
                                          // onChange={(e) => {
                                          //     setTime(e.target.value)
                                          // }}
                                          />
                                          <div className="text-[14px] text-[#FF0000]">{errors.time?.message}</div>
                                      </div>
                                      
                                      <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                          <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Indications</p>
                                          <input name="instructions"  type={'text'} className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] pr-[10px] text-ellipsis text-[12px]'
                                          {...register('instructions')}
                                          value={instructionsForm}
                                          onChange={(e) => {
                                              setInstructionsForm(e.target.value)
                                          }}
                                          />
                                          <div className="text-[14px] text-[#FF0000]">{errors.instructions?.message}</div>
                                      </div>
                                      




                                  </div>

                                  <div className="flex flex-row justify-between mt-[20px]">

                                      <div>
                                          <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                      </div>

                                      <div>
                                          <button
                                              className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                              onClick={() =>
                                                  setShowModal(false)
                                              }
                                              disabled={isLoading}
                                          >
                                              Cancel
                                          </button>
                                          <button
                                              className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                              type={'submit'}
                                              disabled={isLoading}
                                          >
                                              {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Create'}
                                          </button>
                                      </div>
                                  </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}