import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';
import { useEffect, useState } from 'react'
import TooltipPlans from './tooltip';


///////////// IMAGES //////////////

import ImgFacebook from '../../../public/Plans/facebook.svg'
import ImgIg from '../../../public/Plans/instagram.svg'
import ImgTiktok from '../../../public/Plans/tikTok.svg'
import ImgYoutube from '../../../public/Plans/youtube.svg'
import ImgTelegram from '../../../public/Plans/telegram.svg'
import ImgTwitter from '../../../public/Plans/twitter.svg'
import editar from "../../../public/editar.svg"


const TablePlans = (props) => {

    const {reloadPlansAll, setReloadPlansAll} = props
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);
    const headlist = [
        'Name', "Post", "Has included ads", 'Price', ""
    ];

    useEffect(() => {

        setIsLoading(true)

        fetch('https://slogan.com.bo/roadie/plans/all/')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    //console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

    }, [reloadPlansAll])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
                <div>
                    <TablaProductos data={data} headlist={headlist} reloadPlansAll={reloadPlansAll} setReloadPlansAll={setReloadPlansAll}/>
                    <TablaResponsive data={data} headlist={headlist} reloadPlansAll={reloadPlansAll} setReloadPlansAll={setReloadPlansAll}/>
                </div>
            </>
    )
}


const TablaProductos = (props) => {
    const { headlist, data, reloadPlansAll, setReloadPlansAll } = props;

    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
        });
        data = obj;
    };
    splitData(data);

    return (

        <div className="rounded-lg shadow hidden lg:block md:block">
            <table className="w-full">
                <thead className='bg-[#FFFF] shadow'>
                    <tr>
                        {headlist.map(header => <th key={headlist.id} className='text-[12px] font-semibold text-[#000000] text-center pt-[10px] pb-[10px]'>{header}</th>)}
                    </tr>
                </thead>

                <tbody>

                    {data.map(row =>

                        <tr key={row.id} >
                            <td>
                               <div className='text-[12px] pl-[15px]'>
                                    {row.name}
                               </div>
                            </td>
                            <td className='font-semibold '>
                                <div className='flex flex-row'>
                                    <div className='flex flex-row mr-[5px]'>
                                        <div className='text-[12px] self-center items-center'>
                                            {row.facebook_posts_qty}
                                        </div>

                                        <div className=' self-center items-center'>
                                            {row.facebook_posts_qty !== null ?
                                                <Image
                                                    src={ImgFacebook}
                                                    alt='ImgIg'
                                                    layout='fixed'
                                                    width={30}
                                                    height={30}
                                                />
                                                : <></>
                                            }
                                        </div>
                                    </div>
                                    <div className='flex flex-row mr-[5px]'>
                                        <div className='text-[12px] self-center items-center'>
                                        {row.instagram_posts_qty}
                                        </div>

                                        <div className=' self-center items-center'>
                                            {row.instagram_posts_qty !== null ?
                                                <Image
                                                    src={ImgIg}
                                                    alt='ImgIg'
                                                    layout='fixed'
                                                    width={30}
                                                    height={30}
                                                />
                                                : <></>
                                            }
                                        </div>
                                    </div>

                                    <div className='flex flex-row mr-[5px]'>
                                        <div className='text-[12px] self-center items-center'>
                                            {row.mailing_posts_qty}
                                        </div>

                                        <div className=' self-center items-center'>
                                            {row.mailing_posts_qty !== null ?
                                                <Image
                                                    src={ImgTelegram}
                                                    alt='ImgIg'
                                                    layout='fixed'
                                                    width={30}
                                                    height={30}
                                                />
                                                : <></>
                                            }
                                        </div>
                                    </div>

                                    <div className='flex flex-row mr-[5px]'>
                                        <div className='text-[12px] self-center items-center'>
                                            {row.youtube_posts_qty}
                                        </div>

                                        <div className=' self-center items-center'>
                                            {row.youtube_posts_qty !== null ?
                                                <Image
                                                    src={ImgYoutube}
                                                    alt='ImgYoutube'
                                                    layout='fixed'
                                                    width={30}
                                                    height={30}
                                                />
                                                : <></>
                                            }
                                        </div>
                                    </div>
                                    <div className='flex flex-row mr-[5px]'>
                                        <div className='text-[12px] self-center items-center'>
                                            {row.tiktok_posts_qty}
                                        </div>

                                        <div className=' self-center items-center'>
                                            {row.tiktok_posts_qty !== null ?
                                                <Image
                                                    src={ImgTiktok}
                                                    alt='ImgYoutube'
                                                    layout='fixed'
                                                    width={30}
                                                    height={30}
                                                />
                                                : <></>
                                            }
                                        </div>
                                    </div>

                                    <div className='flex flex-row mr-[5px]'>
                                        <div className='text-[12px] self-center items-center'>
                                            {row.linkedin_posts_qty}
                                        </div>

                                        <div className=' self-center items-center'>
                                            {row.linkedin_posts_qty !== null ?
                                                <Image
                                                    src={ImgTelegram}
                                                    alt='ImgYoutube'
                                                    layout='fixed'
                                                    width={30}
                                                    height={30}
                                                />
                                                : <></>
                                            }
                                        </div>
                                    </div>

                                    <div className='flex flex-row'>
                                        <div className='text-[12px] self-center items-center'>
                                            {row.twitter_posts_qty}
                                        </div>

                                        <div className=' self-center items-center'>
                                            {row.twitter_posts_qty !== null ?
                                                <Image
                                                    src={ImgTwitter}
                                                    alt='ImgYoutube'
                                                    layout='fixed'
                                                    width={30}
                                                    height={30}
                                                />
                                                : <></>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </td>
                            
                            <td className=' text-center]'>
                                {row.included_ads_qty !== null ?
                                <div className='text-[12px] text-[#000000] text-center'>
                                    {row.included_ads_qty} USD

                                </div>
                                : <></>
                                }

                            </td>
                            
                            <td className='text-center'>
                                {row.price !== null ?
                                    <div className='text-[12px] text-[#000000] text-center'>
                                        {row.price} USD
                                    </div>
                                    : <></>
                                }

                            </td>

                            <td className='text-center'>
                                  <TooltipPlans
                                    id={row.id}
                                    row={row}
                                    name={row.name}
                                    facebook_posts_qty={row.facebook_posts_qty}
                                    instagram_posts_qty={row.instagram_posts_qty}
                                    mailing_posts_qty={row.mailing_posts_qty}
                                    youtube_posts_qty={row.youtube_posts_qty}
                                    tiktok_posts_qty={row.tiktok_posts_qty}
                                    linkedin_posts_qty={row.linkedin_posts_qty}
                                    twitter_posts_qty={row.twitter_posts_qty}
                                    has_included_ads={row.has_included_ads}
                                    included_ads_qty={row.included_ads_qty}
                                    price={row.price}
                                    reloadPlansAll={reloadPlansAll} 
                                    setReloadPlansAll={setReloadPlansAll}
                                  />
                            </td>

                        </tr>
                    )}


                </tbody>
            </table>

        </div>
    );
};

const TablaResponsive = (props) => {
    const { headlist, data, reloadPlansAll, setReloadPlansAll  } = props;

    return (
        <div className='grid grid-cols-12 '>
            <div className='col-span-12 md:col-span-12 lg:col-span-12 rounded-[10px] md:hidden'>
                
                {data.map(row =>
                        { 
                        return (
                        <div key={row.id} className='grid grid-cols-12 bg-[#fff] rounded-[15px] shadow-md pl-[10px] pb-[10px] mb-[15px]'>
                            <div className='col-span-12 md:col-span-12'>
                                <div className='grid grid-cols-12'>
                                    <div className='col-span-10 md:col-span-12 pt-[10px]'>
                                        <div className='grid grid-cols-12 gap-3'>
                                            <div className='col-span-12 md:col-span-12'>
                                                <div className='text-[14px] font-semibold'>
                                                    {row.name}
                                                </div>

                                            </div>
                                            <div className='col-span-12 md:col-span-12'>
                                                <div className='flex flex-row'>
                                                    <div className='flex flex-row mr-[5px]'>
                                                        <div className='text-[12px] self-center items-center'>
                                                         {row.facebook_posts_qty}
                                                        </div>

                                                        <div className=' self-center items-center'>
                                                            {row.facebook_posts_qty !== null ?
                                                                <Image
                                                                    src={ImgFacebook}
                                                                    alt='ImgIg'
                                                                    layout='fixed'
                                                                    width={30}
                                                                    height={30}
                                                                />
                                                                : <></>
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className='flex flex-row mr-[5px]'>
                                                        <div className='text-[12px] self-center items-center'>
                                                        {row.instagram_posts_qty}
                                                        </div>

                                                        <div className=' self-center items-center'>
                                                            {row.instagram_posts_qty !== null ?
                                                                <Image
                                                                    src={ImgIg}
                                                                    alt='ImgIg'
                                                                    layout='fixed'
                                                                    width={30}
                                                                    height={30}
                                                                />
                                                                : <></>
                                                            }
                                                        </div>
                                                    </div>

                                                    <div className='flex flex-row mr-[5px]'>
                                                        <div className='text-[12px] self-center items-center'>
                                                            {row.mailing_posts_qty}
                                                        </div>

                                                        <div className=' self-center items-center'>
                                                            {row.mailing_posts_qty !== null ?
                                                                <Image
                                                                    src={ImgTelegram}
                                                                    alt='ImgIg'
                                                                    layout='fixed'
                                                                    width={30}
                                                                    height={30}
                                                                />
                                                                : <></>
                                                            }
                                                        </div>
                                                    </div>

                                                    <div className='flex flex-row mr-[5px]'>
                                                        <div className='text-[12px] self-center items-center'>
                                                            {row.youtube_posts_qty}
                                                        </div>

                                                        <div className=' self-center items-center'>
                                                            {row.youtube_posts_qty !== null ?
                                                                <Image
                                                                    src={ImgYoutube}
                                                                    alt='ImgYoutube'
                                                                    layout='fixed'
                                                                    width={30}
                                                                    height={30}
                                                                />
                                                                : <></>
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className='flex flex-row mr-[5px]'>
                                                        <div className='text-[12px] self-center items-center'>
                                                            {row.tiktok_posts_qty}
                                                        </div>

                                                        <div className=' self-center items-center'>
                                                            {row.tiktok_posts_qty !== null ?
                                                                <Image
                                                                    src={ImgTiktok}
                                                                    alt='ImgYoutube'
                                                                    layout='fixed'
                                                                    width={30}
                                                                    height={30}
                                                                />
                                                                : <></>
                                                            }
                                                        </div>
                                                    </div>

                                                    <div className='flex flex-row mr-[5px]'>
                                                        <div className='text-[12px] self-center items-center'>
                                                            {row.linkedin_posts_qty}
                                                        </div>

                                                        <div className=' self-center items-center'>
                                                            {row.linkedin_posts_qty !== null ?
                                                                <Image
                                                                    src={ImgTelegram}
                                                                    alt='ImgYoutube'
                                                                    layout='fixed'
                                                                    width={30}
                                                                    height={30}
                                                                />
                                                                : <></>
                                                            }
                                                        </div>
                                                    </div>

                                                    <div className='flex flex-row'>
                                                        <div className='text-[12px] self-center items-center'>
                                                            {row.twitter_posts_qty}
                                                        </div>

                                                        <div className=' self-center items-center'>
                                                            {row.twitter_posts_qty !== null ?
                                                                <Image
                                                                    src={ImgTwitter}
                                                                    alt='ImgYoutube'
                                                                    layout='fixed'
                                                                    width={30}
                                                                    height={30}
                                                                />
                                                                : <></>
                                                            }
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        
                                        <div className='grid grid-cols-12'>
                                                <div className='col-span-6 md:col-span-6'>
                                                    
                                                    {row.included_ads_qty !== null ?
                                                        <div className='text-[16px] font-semibold text-[#000000] text-center'>
                                                            <div className='text-[#000] text-[12px]'>Has included ads:</div>
                                                             {row.included_ads_qty} USD

                                                        </div>
                                                    : <></>
                                                    }
                                                    
                                                </div>
                                                <div className='col-span-6 md:col-span-6'>
                                                    
                                                    {row.price !== null ?
                                                        <div className='text-[16px] font-semibold text-[#000000] text-center'>
                                                            <div className='text-[#000] text-[12px]'>Price:</div>
                                                            {row.price} USD
                                                        </div>
                                                    : <></>
                                                    }
                                                </div>
                                            </div>
                                    </div>
                                    <div className='col-span-2 md:col-span-12'>
                                        <div className=' items-center text-center '>
                                        <TooltipPlans
                                            id={row.id}
                                            name={row.name}
                                            facebook_posts_qty={row.facebook_posts_qty}
                                            instagram_posts_qty={row.instagram_posts_qty}
                                            mailing_posts_qty={row.mailing_posts_qty}
                                            youtube_posts_qty={row.youtube_posts_qty}
                                            tiktok_posts_qty={row.tiktok_posts_qty}
                                            linkedin_posts_qty={row.linkedin_posts_qty}
                                            twitter_posts_qty={row.twitter_posts_qty}
                                            has_included_ads={row.has_included_ads}
                                            included_ads_qty={row.included_ads_qty}
                                            price={row.price}
                                            reloadPlansAll={reloadPlansAll} 
                                            setReloadPlansAll={setReloadPlansAll}
                                            />

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            )
                        }
                    )}
            </div>
        </div>

        
    );
};




export default TablePlans;
