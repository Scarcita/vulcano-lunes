import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';

import ImgFacebook from '../../../public/Plans/facebook.svg'
import ImgGmail from '../../../public/Plans/gmail.svg'
import ImgIg from '../../../public/Plans/instagram.svg'
import ImgMessenger from '../../../public/Plans/messenger.svg'
import ImgTelegram from '../../../public/Plans/telegram.svg'
import ImgTiktok from '../../../public/Plans/tikTok.svg'
import ImgTwitter from '../../../public/Plans/twitter.svg'
import ImgWhatsapp from '../../../public/Plans/whatsapp.svg'
import ImgYoutube from '../../../public/Plans/youtube.svg'
import { status } from 'nprogress';

export default function ModalPlans(props) {

    const [isLoading, setIsLoading] = useState(false)
    const [showModal, setShowModal] = useState(false);
    const {
        reloadPlansAll,
        setReloadPlansAll
        } = props;

    const [dataAdd, setAddData] = useState('')
    const [name, setName] = useState('')
    const [facebook, setFacebook] = useState('')
    const [facebookPostsGty, setFacebookPostsGty] = useState('')
    const [instagram, setInstagram] = useState('')
    const [instagramPostsGty, setInstagramPostsGty] = useState('')
    const [mailing, setMailing] = useState('')
    const [mailingPostsGty, setMailingPostsGty] = useState('')
    const [youtube, setYoutube] = useState('')
    const [youtubePostsGty, setYoutubePostsGty] = useState('')
    const [tiktok, setTiktok] = useState('')
    const [tiktokPostsGty, setTiktokPostsGty] = useState('')
    const [linkedin, setLinkedin] = useState('')
    const [linkedinPostsGty, setLinkedinPostsGty] = useState('')
    const [twitter, setTwitter] = useState('')
    const [twitterPostsGty, setTwitterPostsGty] = useState('')
    const [hasIncludedAds, setHasIncludedAds] = useState('')
    const [includedAdsQty, setIncludedAdsQty] = useState('')
    const [price, setPrice] = useState('')


    const clearForm = () => {
        setName('a')
        setFacebook('facebook')
        setFacebookPostsGty('')
        setInstagram('')
        setInstagramPostsGty('')
        setMailing('')
        setMailingPostsGty('')
        setYoutube('')
        setYoutubePostsGty('')
        setTiktok('')
        setTiktokPostsGty('')
        setLinkedin('')
        setLinkedinPostsGty('')
        setTwitter('')
        setTwitterPostsGty('')
        setHasIncludedAds('')
        setIncludedAdsQty('')
        setPrice('')

    }

    
  const getDataOrden = async () => {
    var data = new FormData();
    console.warn(name)
    console.warn(facebookPostsGty)
    console.warn(instagram)

    data.append("name", name);
    data.append("facebook", facebook ? '1' : '0');
    data.append("facebook_posts_qty", facebookPostsGty);
    data.append("instagram", instagram ? '1' : '0');
    data.append("instagram_posts_qty", instagramPostsGty);
    data.append("mailing", mailing ? '1' : '0');
    data.append("mailing_posts_qty", mailingPostsGty);
    data.append("youtube", youtube ? '1' : '0');
    data.append("youtube_posts_qt", youtubePostsGty);
    data.append("tiktok", tiktok ? '1' : '0');
    data.append("tiktok_posts_qty", tiktokPostsGty);
    data.append("linkedin", linkedin ? '1' : '0');
    data.append("linkedin_posts_qty", linkedinPostsGty);
    data.append("twitter", twitter ? '1' : '0');
    data.append("twitter_posts_qty", twitterPostsGty);
    data.append("has_included_ads", hasIncludedAds ? '1' : '0');
    data.append("included_ads_qty", includedAdsQty);
    data.append("price", price);

    fetch("https://slogan.com.bo/roadie/plans/addMobile", {
      method: 'POST',
      body: data,
    })
      .then(response => response.json())
      .then(data => {
        //console.log('VALOR ENDPOINTS: ', data);
        setIsLoading(false)
        if(data.status){
          clearForm()
          setShowModal(false)
          setReloadPlansAll(!reloadPlansAll)
          //IMPLEMENTAR RECARGA DE DATOS (COMPARTIR FUNCION DE PANTALLA AL MODAL)
        }
        
      },
      (error) => {
        console.log(error)
      }
      )

  }

  useEffect(() => {
    console.warn('facebookPostsGty : '+ facebookPostsGty);
  }, [ facebookPostsGty])

  useEffect(() => {
    console.warn('ahora name: '+ name);
  }, [ name])
  useEffect(() => {
    console.warn('ahora TIKTOK : '+ tiktok);
  }, [ tiktok])

  useEffect(() => {
    console.warn('PRICE : '+ price);
  }, [ price])

  useEffect(() => {
    console.warn('ahora twitterPostsGty: '+ twitterPostsGty);
  }, [ twitterPostsGty])
  useEffect(() => {
    console.warn('ahora linkedinPostsGty : '+ linkedinPostsGty);
  }, [ linkedinPostsGty])

  useEffect(() => {
    console.warn('mailingPostsGty : '+ mailingPostsGty);
  }, [ mailingPostsGty])



    const validationSchema = Yup.object().shape({
        name: Yup.string()
        .required('name is required'),

        // facebook: Yup.string()
        //   .required("facebook is required"),
        // // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        // facebookPostsGty: Yup.string()
        //   .required('facebookPostsGty is required'),
        // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        // instagram: Yup.string()
        //   .required('instagram is required'),
        // // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        // instagramPostsGty: Yup.number()
        //   .required('instagramPostsGty is required'),
        // //   //.min(6, 'minimo 6 caracteres'),
        // // //.matches(/[a-zA-Z0-9]/, 'solo numeros y letras'),
    
        // mailing: Yup.string()
        //   .required('mailing is required'),
    
        // mailingPostsGty: Yup.string()
        //   .required('mailingPostsGty is required'),
    
        // youtube: Yup.string()
        //   .required('youtube is required'),
    
        // youtubePostsGty: Yup.string()
        // .required('youtubePostsGty is required'),
        // // //.matches(/^[\(]?[\+]?(\d{2}|\d{3})[\)]?[\s]?((\d{6}|\d{8})|(\d{3}[\*\.\-\s]){3}|(\d{2}[\*\.\-\s]){4}|(\d{4}[\*\.\-\s]){2})|\d{8}|\d{10}|\d{12}$/, 'Ingrese un numero valido'),
    
        // tiktok: Yup.string()
        // .required('tiktok is required'),
    
        // tiktokPostsGty: Yup.string()
        // .required('tiktokPostsGty is required'),
        // // //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        // linkedin: Yup.string()
        // .required('linkedin is required'),
        // // // //.matches(/[A-za-z0-9]/, 'solo se permite numeros'),
    
        // linkedinPostsGty: Yup.string()
        // .required('linkedinPostsGty is required'),
        // // //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        // twitter: Yup.string()
        // .required('twitter is required'),
        // // //.matches(/^[\(]?[\+]?(\d{2}|\d{3})[\)]?[\s]?((\d{6}|\d{8})|(\d{3}[\*\.\-\s]){3}|(\d{2}[\*\.\-\s]){4}|(\d{4}[\*\.\-\s]){2})|\d{8}|\d{10}|\d{12}$/, 'Ingrese un numero valido'),
    
        // twitterPostsGty: Yup.string()
        // .required('twitterPostsGty is required'),
        // // //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
    
        hasIncludedAds: Yup.string()
          .required('hasIncludedAds is required'),
        //   //.matches(/^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/, 'Date must be a valid date in the format YYYY-MM-DD'),
        
        includedAdsQty: Yup.string()
        .required('includedAdsQty is required'),

        price: Yup.string()
        .required('price is required'),
    
    
        });
        const formOptions = { resolver: yupResolver(validationSchema) };
    
        // get functions to build form with useForm() hook
        const { register, handleSubmit, reset, formState } = useForm(formOptions);
        const { errors } = formState;
    
        function onSubmit(data) {
            alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
            console.log('string is NOT empty')
            getDataOrden()
            return false;
            
        }



    return (
        <>
            <div>
                <button
                    className="w-[112px] h-[22px] md:w-[200px] md:h-[48px] lg:w-[240px] lg:h-[46px] bg-[#582BE7] text-[#FFFFFF] font-semibold text-[12px] md:text-[15px] lg:text-[15px] rounded-[25px] text-center items-center hover:bg-[#fff] hover:border-[#582BE7] hover:border-[2px] hover:text-[#582BE7] justify-center"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    Create Plan
                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                <div>
                                    <h4 className="text-lg font-medium text-gray-800">
                                        CREATE PLANS
                                    </h4>

                                </div>

                                <div>
                                <form onSubmit={handleSubmit(onSubmit)}>

                                    <div className="mt-[20px] grid grid-cols-12 gap-4">

                                        <div className='col-span-12 md:col-span-12 lg:col-span-12 '>
            
                                                <p className='text-[12px] text-[#C1C1C1] mr-[10px] '>Name</p>
                                                <input name="name" type="text" id="name" className={`w-full h-[30px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`}
                                                placeholder='Ingrese su nombre...'
                                                {...register('name')}
                                                value={name}
                                                onChange={(e) => {
                                                    setName(e.target.value)
                                                }}/>
                                                <div className="text-[14px] text-[#FF0000]">{errors.name?.message}</div>

                                        </div>

                                    
                                    </div>
                                

                                    <div className="mt-[20px] grid grid-cols-12 gap-3">

                                        <div className='col-span-6 md:col-span-6 lg:col-span-4 '>
                                            <div className="flex flex-row">
                                                <input name="facebook"   type="checkbox" id="" className="mr-[5px]" 
                                                {...register('facebook')}
                                                value={facebook}
                                                onChange={(e) => {
                                                    setFacebook(e.target.value)
                                                }}/>
                                                <Image
                                                src={ImgFacebook}
                                                alt='ImgFacebook'
                                                layout='fixed'
                                                width={25}
                                                height={25}
                                            />
                                            <input name="facebookPostsGty" type="number" {...register('facebookPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                                            placeholder='Posts Qty'
                                            value={facebookPostsGty}
                                            onChange={(e) => {
                                                setFacebookPostsGty(e.target.value)
                                            }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.facebookPostsGty?.message}</div>
                                                
                                            </div>
                                        </div>

                                        <div className='col-span-6 md:col-span-6 lg:col-span-4 '>
                                            <div className="flex flex-row">
                                                <input name="instagram" type="checkbox" id="acceptTermsandConditions"  className="mr-[5px]" 
                                                {...register('instagram')}
                                                value={instagram}
                                                onChange={(e) => {
                                                    setInstagram(e.target.value)
                                                }}/>
                                                <Image
                                                src={ImgIg}
                                                alt='ImgIg'
                                                layout='fixed'
                                                width={25}
                                                height={25}
                                            />
                                            <input name="instagramPostsGty" type="number" {...register('instagramPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                                            placeholder='Posts Qty'
                                            value={instagramPostsGty}
                                            onChange={(e) => {
                                                setInstagramPostsGty(e.target.value)
                                            }}
                                            />

                                            <div className="text-[14px] text-[#FF0000]">{errors.instagramPostsGty?.message}</div>
                                                
                                            </div>
                                        </div>

                                        <div className='col-span-6 md:col-span-6 lg:col-span-4 '>
                                            <div className="flex flex-row">
                                                <input name="mailing" type="checkbox" id="acceptTermsandConditions" className="mr-[5px]"
                                                {...register('mailing')}
                                                value={mailing}
                                                onChange={(e) => {
                                                    setMailing(e.target.value)
                                                }} />
                                                <Image
                                                src={ImgGmail}
                                                alt='ImgGmail'
                                                layout='fixed'
                                                width={25}
                                                height={25}
                                            />
                                            <input name="mailingPostsGty" type="number" {...register('mailingPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                                            placeholder='Posts Qty'
                                            value={mailingPostsGty}
                                            onChange={(e) => {
                                                setMailingPostsGty(e.target.value)
                                            }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.mailingPostsGty?.message}</div>
                                                
                                            </div>
                                        </div>
                                        

                                        <div className='col-span-6 md:col-span-6 lg:col-span-3 '>
                                            <div className="flex flex-row">
                                                <input name="youtube" type="checkbox" id="acceptTermsandConditions" className="mr-[5px]"
                                                {...register('youtube')} 
                                                value={youtube}
                                                onChange={(e) => {
                                                    setYoutube(e.target.value)
                                                }}/>
                                                <Image
                                                src={ImgYoutube}
                                                alt='ImgYoutube'
                                                layout='fixed'
                                                width={25}
                                                height={25}
                                            />
                                            <input name="youtubePostsGty" type="number" {...register('youtubePostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                                            placeholder='Posts Qty'
                                            value={youtubePostsGty}
                                            onChange={(e) => {
                                                setYoutubePostsGty(e.target.value)
                                            }}
                                            />

                                            <div className="text-[14px] text-[#FF0000]">{errors.youtubePostsGty?.message}</div>
                                                
                                            </div>
                                        </div>

                                        <div className='col-span-6 md:col-span-6 lg:col-span-3 '>
                                            <div className="flex flex-row">
                                                <input name="tiktok" type="checkbox" id="acceptTermsandConditions" className="mr-[5px]"
                                                {...register('tiktok')}
                                                value={tiktok}
                                                onChange={(e) => {
                                                    setTiktok(e.target.value)
                                                }}/>
                                                <Image
                                                src={ImgTiktok}
                                                alt='ImgTiktok'
                                                layout='fixed'
                                                width={25}
                                                height={25}
                                            />
                                            <input name="tiktokPostsGty" type="number" {...register('tiktokPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                                            placeholder='Posts Qty'
                                            value={tiktokPostsGty}
                                            onChange={(e) => {
                                                setTiktokPostsGty(e.target.value)
                                            }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.tiktokPostsGty?.message}</div>
                                                
                                            </div>
                                        </div>

                                        <div className='col-span-6 md:col-span-6 lg:col-span-3'>
                                            <div className="flex flex-row">
                                                <input name="linkedin" type="checkbox" id="acceptTermsandConditions" className="mr-[5px]"
                                                {...register('linkedin')}
                                                value={linkedin}
                                                onChange={(e) => {
                                                    setLinkedin(e.target.value)
                                                }}/>
                                                <Image
                                                src={ImgTelegram}
                                                alt='ImgTelegram'
                                                layout='fixed'
                                                width={25}
                                                height={25}
                                            />
                                            <input name="linkedinPostsGty" type="number" {...register('linkedinPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                                            placeholder='Posts Qty'
                                            value={linkedinPostsGty}
                                            onChange={(e) => {
                                                setLinkedinPostsGty(e.target.value)
                                            }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.linkedinPostsGty?.message}</div>
                                                
                                            </div>
                                        </div>

                                        <div className='col-span-6 md:col-span-6 lg:col-span-3'>
                                            <div className="flex flex-row">
                                                <input name="twitter" type="checkbox" id="acceptTermsandConditions" {...register('twitter')} className="mr-[5px]"
                                                {...register('twitter')}
                                                value={twitter}
                                                onChange={(e) => {
                                                    setTwitter(e.target.value)
                                                }}/>
                                                <Image
                                                src={ImgTwitter}
                                                alt='ImgTwitter'
                                                layout='fixed'
                                                width={25}
                                                height={25}
                                            />
                                            <input name="twitterPostsGty" type="number" {...register('twitterPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                                            placeholder='Posts Qty'
                                            value={twitterPostsGty}
                                            onChange={(e) => {
                                                setTwitterPostsGty(e.target.value)
                                            }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.twitterPostsGty?.message}</div>
                                                
                                            </div>
                                        </div>
                                        
                                    
                                    </div>

                                    <div className="mt-[15px] grid grid-cols-12 gap-3">

                                        <div className='col-span-6 md:col-span-6 lg:col-span-6 '>
                                            <div className="flex flex-row">
                                                <input name="acceptTermsandConditions" type="checkbox" id="acceptTermsandConditions"
                                                {...register('hasIncludedAds')}
                                                value={hasIncludedAds}
                                                onChange={(e) => {
                                                    setHasIncludedAds(e.target.value)
                                                }}/>
                                                <p className='text-[12px] text-[#C1C1C1] ml-[10px] '>Has included ads </p>
                                                
                                                
                                            </div>
                                            <input name="includedAdsQty" type="number" {...register('includedAdsQty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]`}
                                                placeholder='Posts Qty'
                                                value={includedAdsQty}
                                                onChange={(e) => {
                                                    setIncludedAdsQty(e.target.value)
                                                }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.includedAdsQty?.message}</div>
                                        </div>

                                        <div className='col-span-6 md:col-span-6 lg:col-span-6 '>
                                            <p className='text-[12px] text-[#C1C1C1] mr-[10px] '>Price</p>
                                            <input name="price" type="text" {...register('price')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`}
                                            placeholder='Price'
                                            value={price}
                                            onChange={(e) => {
                                                setPrice(e.target.value)
                                            }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.price?.message}</div>
                                                
                                        </div>
                                        

                                    </div>


                                    <div className="flex flex-row justify-between mt-[20px]">

                                        <div>
                                            <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                        </div>

                                        <div>
                                            <button
                                                className="w-[75px] h-[35px] border-[1px] border-[#582BE7] rounded-[30px] text-[#582BE7] hover:bg-[#582BE7] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                onClick={() =>
                                                    setShowModal(false)
                                                    
                                                }
                                                disabled={isLoading}
                                            >
                                                Cancel
                                            </button>
                                            <button
                                                className="w-[100px] h-[35px] border-[1px] bg-[#582BE7] rounded-[30px] text-[#FFFFFF] hover:border-[#582BE7] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#582BE7] text-[14px] mt-[3px]"
                                                type='submit'
                                                disabled={isLoading}
                                            >
                                                {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Create'}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}