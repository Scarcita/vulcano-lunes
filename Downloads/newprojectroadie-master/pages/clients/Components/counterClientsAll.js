import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import { useEffect, useState } from 'react';


export default function CounterClientsAll() {
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([ ]);
    const [clientPlan, setClientPlan] = useState([])
    const [counterPosted, setCounterPosted] = useState(0);


    useEffect(() => {
        fetch('https://slogan.com.bo/roadie/clients/all')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    var temp = [];
                    data.data.map((result) => {
                        temp.push(result)
                    })

                    let temporal = 0;
                    for (let i = 0; i < temp.length; i++) {
                        temp[i].name !== null ?
                            (
                                temporal++,
                                setCounterPosted(temporal)
                            )
                            :
                            null

                    }
    

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })
    }, [])

    return (

        isLoading ?
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#582BE7" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div>{counterPosted}</div>
    )
}