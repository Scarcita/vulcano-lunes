import React, { useEffect, useState } from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import { useRouter } from 'next/router';


import { Bar } from 'react-chartjs-2';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

export default function CharDashoard() {
  const [isLoading, setIsLoading] = useState(true)
  const [data, setData] = useState([]);([]);
  const [dataGraph, setDataGraph] = useState(null);

  const router = useRouter()
  const {client_id} = router.query

    const options = {
        responsive: true,
        plugins: {
          legend: {
            display: false,
            position: "top",
          },
          title: {
            display: false,
            text: 'Chart.js Bar Chart',
          },
        },
      };

    const getDatos = () => {

      fetch('https://slogan.com.bo/vulcano/orders/yearlyGraph')
          .then(response => response.json())
          .then(data => {
              if (data) {
                  console.log(data.data);
                  setData(data.data)     
                  console.log(data.data.labels);

                  const labels = data.data.labels;

                  setDataGraph(
                    {
                      labels,
                      datasets: [
                        {
                          label: 'Dataset 1',
                          data: data.data.data,
                          backgroundColor: '#643DCE',
                        },
                      ],
                    }
                  );


              } else {
                  console.error(data.error)
              }
            setIsLoading(false)
          })
          
        }
    
        useEffect(() => {
        getDatos();
    }, []);
  

    return (
      isLoading ?
        <div className='flex justify-center items-center mt-[30px]'  > 
          <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{marginLeft: 'auto', marginRight: 'auto'}} />
        </div> 
        : 
        <div>


          {dataGraph != null ?  
            
              <Bar options={options} data={dataGraph} width={200} height={100} /> 
               : <></>}
         
        </div>

    )
}
