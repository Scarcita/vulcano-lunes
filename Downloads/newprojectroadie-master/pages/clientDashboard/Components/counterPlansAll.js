import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import { useEffect, useState } from 'react';


export default function CounterPlansAll(props) {
    const { client_id} = props
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([ ]);
    const [clientPlan, setClientPlan] = useState([])
    const [counterPlans, setCounterPlans] = useState(0);


    useEffect(() => {
        fetch('https://slogan.com.bo/roadie/clientsPlans/all/' + client_id)
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    var temp = [];
                    data.data.map((result) => {
                        temp.push(result)
                    })

                    let temporal = 0;
                    for (let i = 0; i < temp.length; i++) {
                        temp[i].plan.name !== null ?
                            (
                                temporal++,
                                setCounterPlans(temporal)
                            )
                            :
                            null

                    }
    

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })
    }, [])

    return (

        isLoading ?
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#582BE7" size={14} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div>{counterPlans}</div>
    )
}