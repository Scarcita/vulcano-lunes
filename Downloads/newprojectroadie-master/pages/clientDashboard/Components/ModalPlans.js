import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useRouter } from 'next/router'
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import ReactSelect from 'react-select';
import Image from 'next/image';


export default function ModalPlans(props) {

    const { reloadPlans, setReloadPlans, client_id} = props;
    const [showModal, setShowModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false)
    

    const [name, setName] = useState("");
    const [facebbok, setFacebbok] = useState('')
    const [instagram, setInstagram] = useState('')
    const [mailing, setMailing] = useState('')
    const [youTube, setYouTube] = useState('')
    const [tikTok, setTikTok] = useState('')
    const [linkedIn, setLinkedIn] = useState('')
    const [twitter, setTwitter] = useState('')
    const [included, setIncluded] = useState('')
    const [price, setPrice] = useState('')

   // const [clientId, setClientId] = useState(null)
    const [clientPlan, setClientPlan] = useState([])
    const [planId, setPlanId] = useState([])
    const [startDate, setStartDate] = useState('')
    const [endDate, setEndDate] = useState('')


    const clearForm = () => {
        setPlanId('')
        setStartDate('')
        setEndDate('')
    }


    const formatOptionLabel = ({ value, label, post }) => {

        return (
            <div className='grid grid-cols-12 align-center'>
                <div>
                        <small className='text-gray-500 text-xs'>{post.name}</small>
                </div>
            </div>
        )
    };      


    useEffect(() => {

        fetch('https://slogan.com.bo/roadie/plans/all')
        .then(response => response.json())
        .then(data => {
            if (data.status) {

                var temp = [];
                data.data.map((result) => {
                    temp.push({value: result.id, label: result.name, post: result})
                })

                setClientPlan(temp)
            } else {
                console.error(data.error)
            }
            setIsLoading(false)
        })

    }, [])


    const getClientDashboard = async () => {
        var data = new FormData();
    
        data.append("client_id", client_id );
        data.append("plan_id", planId.value);
        data.append("start_date", startDate );
        data.append("end_date", endDate);
    
        fetch("https://slogan.com.bo/roadie/clientsPlans/addMobile", {
          method: 'POST',
          body: data,
        })
          .then(response => response.json())
          .then(data => {
            //console.log('VALOR DASHBOARD CLIENT: ', data);

            setIsLoading(false)
            if(data.status){
              clearForm()
              setReloadPlans(!reloadPlans)
              setShowModal(false)
            } else {
              alert(JSON.stringify(data.errors, null, 4))
            }
            
          },
          (error) => {
            //console.log(error)
          }
          )
    
      }

      useEffect(() => {
        console.warn('client_id : '+ client_id);
      }, [ client_id])

      useEffect(() => {
        console.warn('plan_id : '+ planId);
      }, [ planId])
    

    
      const validationSchema = Yup.object().shape({
            
        startDate: Yup.string()
            .required('startDate is required'),
        endDate: Yup.string()
            .required('startDate is required'),
    
        });
        const formOptions = { resolver: yupResolver(validationSchema) };
    
        // get functions to build form with useForm() hook
        const { register, handleSubmit, reset, formState } = useForm(formOptions);
        const { errors } = formState;
    
        function onSubmit(data) {
            alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
            console.log('string is NOT empty')
            getClientDashboard()
            return false;
            
        }
    

    return (
        <>
            <div>
                <button
                    className="w-[112px] h-[22px] md:w-[200px] md:h-[48px] lg:w-[240px] lg:h-[46px] bg-[#582BE7] text-[#FFFFFF] font-semibold text-[12px] md:text-[15px] lg:text-[15px] rounded-[25px] text-center items-center hover:bg-[#fff] hover:border-[#582BE7] hover:border-[2px] hover:text-[#582BE7] justify-center"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    Create Plan
                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">

                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">
                                <form onSubmit={handleSubmit(onSubmit)}>
                                    <div>
                                        <h4 className="text-lg font-medium text-gray-800">
                                            CREATE PLANS
                                        </h4>

                                    </div>

                                    <div className="mt-[20px] grid grid-cols-12 gap-4">

                                        <div className='col-span-12 md:col-span-12 lg:col-span-12 '>
                                            <p className='text-[12px] text-[#C1C1C1] mr-[10px] '>Select Plan</p>
                                            <ReactSelect 
                                                defaultValue={planId}
                                                onChange={setPlanId}
                                                formatOptionLabel={formatOptionLabel}
                                                options={clientPlan}
                                                />

                                        </div>

                                        <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Date start </p>
                                            <input name="startDate" type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]`}
                                            {...register('startDate')}
                                            value={startDate}
                                            onChange={(e) => {
                                                setStartDate(e.target.value)
                                            }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.startDate?.message}</div>
                                        </div>

                                        <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Select Date End </p>
                                            <input name="endDate" type="date" className={`w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]`}
                                            {...register('endDate')}
                                            value={endDate}
                                            onChange={(e) => {
                                                setEndDate(e.target.value)
                                            }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.endDate?.message}</div>
                                        </div>

                                    
                                    </div>


                                    <div className="flex flex-row justify-between mt-[20px]">

                                        <div>
                                            <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                        </div>

                                        <div>
                                            <button
                                                className="w-[75px] h-[35px] border-[1px] border-[#582BE7] rounded-[30px] text-[#582BE7] hover:bg-[#582BE7] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                onClick={() =>
                                                    setShowModal(false)
                                                }
                                                disabled={isLoading}
                                            >
                                                Cancel
                                            </button>
                                            <button
                                                className="w-[100px] h-[35px] border-[1px] bg-[#582BE7] rounded-[30px] text-[#FFFFFF] hover:border-[#582BE7] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#582BE7] text-[14px] mt-[3px]"
                                                type="submit"
                                                disabled={isLoading}
                                            >
                                                {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Create'}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}
