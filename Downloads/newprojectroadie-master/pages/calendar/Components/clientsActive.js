import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';
import { useEffect, useState } from 'react'

const Client = (props) => {

    const {reloadAd} = props

    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);

    useEffect(() => {
        fetch('https://slogan.com.bo/roadie/clients/all')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    //console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

            //console.warn('reloaddd' + reloadAd);

    }, [reloadAd])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
                <div className=''>
                    <ClientsActive data={data} />
                </div>
            </>
    )
}


const ClientsActive = (props) => {
    const { data } = props;

    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
        });
        data = obj;
    };
    splitData(data);

    return (

                <div className='grid grid-cols-12 '>

                    {data.map((row, index) =>
                     {

                        if(index > 6){
                            return <></>
                        }

                        return (

                            <div key={row.id} >
                                <div >
                                    {row.img_url !== null ?
                                        <Image
                                            className='rounded-full w-[35px] h-[35px] border-2 border-[#FFFFFF]'
                                            src={row.img_url}
                                            alt='imagenFaceboock'
                                            layout='fixed'
                                            width={35}
                                            height={35}

                                        />
                                        : <></>
                                    }
                                </div>

                            </div>

                        )
                    }
                    )}
                </div>

    );
};





export default Client;