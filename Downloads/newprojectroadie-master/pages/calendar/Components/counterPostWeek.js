import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import { useEffect, useState } from 'react';


export default function CounterPostDay() {
    const [isLoading, setIsLoading] = useState(true);

    const [weekPostedPosts, setWeekPostedPosts] = useState(0);

    function getFirstDayOfWeek(d) {
        // 👇️ clone date object, so we don't mutate it
        const date = new Date(d);
        const day = date.getDay(); // 👉️ get day of week

        // 👇️ day of month - day of week (-6 if Sunday), otherwise +1
        const diff = date.getDate() - day + (day === 0 ? -6 : 1);

        return new Date(date.setDate(diff));
    }

    

    useEffect(() => {

        fetch('https://slogan.com.bo/roadie/clientsPlansPosts/monthCalendar')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    let postsTemp = [];
                    // setData(Object.values(data.data))

                    Object.values(data.data).map((date) => {

                        date.map((result) => {
                            postsTemp.push(result);
                        })
                    })

                    const today = new Date();
                    const firstDay = getFirstDayOfWeek(today);
                    firstDay.setHours(0,0,0);
                    const lastDay = new Date(firstDay);
                    lastDay.setDate(lastDay.getDate() + 6);
                    lastDay.setHours(23, 59, 59);

                    console.log(firstDay); // 👉️ Monday August 8 2022
                    console.log(lastDay); // 👉️ Sunday August 14 2022

                    var weekPostedPostsCounter = 0;

                    postsTemp.map((post) => {
                        var plannedDateTime = new Date(post.planned_datetime);

                        if(post.status == "POSTED"){
                            if(plannedDateTime >= firstDay && plannedDateTime <= lastDay){
                                weekPostedPostsCounter++;
                                console.error(post);
                            }
                        }
                    })

                    setWeekPostedPosts(weekPostedPostsCounter);

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })
    }, [])


    return (

        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#582BE7" size={12} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>
            </>
            :

            <div>
                {weekPostedPosts}
                <div>
                    
                </div>
            </div>


    )
}