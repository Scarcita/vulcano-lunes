import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import ReactSelect from 'react-select';

export default function SelectClient(props) {

    const {clientId, setClientId} = props;
    const [selected, setSelected] = useState(null);
    const [isLoadingClients, setIsLoadingClients] = useState(true);
    const [clients, setClients] = useState([]);


    const formatOptionLabel1 = ({ value, label, post }) => {
        const hasImage = false;
        if(post.img_url !== null && post.img_url.trim().length !== 0){
            hasImage = true;
        }
        return (
            <div className='flex flex-row self-center items-center'>
                {hasImage ?
                <div className='pr-[10px] pt-[3px]'>
                    <Image
                        className='rounded-full'
                        src={post.img_url}
                        alt='media'
                        layout='fixed'
                        width={30}
                        height={30}
                    >
                    </Image>
                </div>
                : <></>
            }
            <small className='text-black text-[12px] leading-2 whitespace-normal text-ellipsis ...'>{post.name}</small>   
            </div> 
        )
    }; 

    useEffect(() => {

        fetch('https://slogan.com.bo/roadie/clients/all')
        .then(response => response.json())
        .then(data => {
            if (data.status) {
                var temp = [];
                data.data.map((result) => {
                    temp.push({value: result.id, label: result.name, post: result})
                })
                setClients(temp)
            } else {
                console.error(data.error)
            }
            setIsLoadingClients(false)
        })

    }, [])


    useEffect(() => {
        console.log(clientId);
    }, [clientId])

    return (
        <div>
            <div className=" grid grid-cols-12">
                <div className='col-span-12 md:col-span-12 lg:col-span-12 '>
                    {isLoadingClients ?
                    <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                        <Spinner color="#582BE7" size={12} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                    </div>
                    :
                    <div>
                        <p className='text-[12px] text-[#C1C1C1] '>Select Client</p>
                        <ReactSelect
                            defaultValue={null} 
                            // value={clientId == 0 ? null : clientId} 
                            value={clientId == 0 ? null : selected}
                            onChange={ (newValue) => {
                                setClientId(newValue.value);
                                setSelected(newValue);
                            }}
                            formatOptionLabel={formatOptionLabel1}
                            options={clients}
                            />
                    </div>
                    }
                </div>
                                    
            </div>
        </div>
    );
}