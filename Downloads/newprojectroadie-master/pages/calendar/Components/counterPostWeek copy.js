import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import { useEffect, useState } from 'react';


export default function CounterPostWeek() {
    const [isLoading, setIsLoading] = useState(true)
    const [counterPosted, setCounterPosted] = useState(0);

    useEffect(() => {

        fetch('https://slogan.com.bo/roadie/clientsPlansPosts/monthCalendar')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    let postsTemp = [];
                    // setData(Object.values(data.data))

                    Object.values(data.data).map((date) => {

                        date.map((result) => {
                            postsTemp.push(result);
                        })

                    })
                    let temporal = 0;
                    for (let i = 0; i < postsTemp.length; i++) {
                        postsTemp[i].status == "PLANNED" ?
                            (
                                temporal++,
                                setCounterPosted(temporal)
                            )
                            :
                            null

                    }

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })
    }, [])


    // const today = new Date();

    // function getFirstDayOfWeek(d) {
    //     const date = new Date(d);
    //     const day = date.getDay(); // 👉️ get day of week

    //     const diff = date.getDate() - day + (day === 0 ? -6 : 1);

    //     return new Date(date.setDate(diff));
    // }

    // const firstDay = getFirstDayOfWeek(today);

    // const lastDay = new Date(firstDay);
    // lastDay.setDate(lastDay.getDate() + 6);

    // console.log(lastDay);



    return (

        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#582BE7" size={12} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>
            </>
            :

            <div>


                <div>
                    {counterPosted}
                </div>


            </div>

    )
}