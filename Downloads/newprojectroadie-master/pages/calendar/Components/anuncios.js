import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import ImgPublic from '../../../public/Calendar/public.svg'
import Image from 'next/image'
import Link from "next/link";
import { useEffect, useState } from 'react'
import ModalEditCalendar from './modalEditCalendar';
import ModalEditLateral from './EditPostLateral';


export default function Anuncios() {
  const [isLoading, setIsLoading] = useState(true)
  const [data, setData] = useState([]);
  //const [post, setPost] = useState([]);
  const [singlePost, setSinglePost] = useState([]);

  const [postsOnly, setPostsOnly] = useState([]);
  var postsTemp = [];
  
  useEffect(() => {
    fetch('https://slogan.com.bo/roadie/clientsPlansPosts/monthCalendar')
    .then(response => response.json())
    .then(data => {
        if (data.status) {
            setData(Object.values(data.data))

            Object.values(data.data).map((date) => {

              date.map((result) =>{
                postsTemp.push(result);
                 console.log('VIEW RESULT: ' + result);
              })

            })
            setPostsOnly(postsTemp);

        } else {
            console.error(data.error)
        }
        setIsLoading(false)
    })
}, [])



  return (
    isLoading ?
    <>
        <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
            <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
        </div>

    </>
    :
    <>
    <div className='bg-[#F7F7F7] pb-[30px]'>
      <div className='grid grid-cols-12 ml-[25px] gap-4 mr-[25px] '>
        <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between mt-[45px] mb-[25px]'>
            <div className='text-[14px] md:text-[18px] lg:text-[14px] text-[#000000] font-semibold'>
              May 7, 2022
            </div>
            <div className=''>
            <Link href='/posts'>
              <a className='text-[14px] md:text-[18px] lg:text-[14px] text-[#582BE7] font-semibold hover:text-[#000000]'>View All</a>
          </Link>
          
          </div>
        </div >
          {
            postsOnly.map((post, index) =>
             {

                if(index > 2){
                  return <></>
              }

              if(post.planned_datetime !== null && post.planned_datetime !== undefined){
                var eventdate = post.planned_datetime;
                var splitdate = eventdate.split('-');
                //console.log(splitdate);
                var day = splitdate[2];
                var year = splitdate[0];
              }
        
              if(post.planned_datetime !== null && post.planned_datetime !== undefined){
                var months = ['Jan.', 'Feb.', 'Mar. ', 'Apr.', 'May.', 'Jun.', 'Jul.','Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.'];
                var m = new Date(post.planned_datetime);
                var monthName = months[m.getMonth()];
              }

              // if(post.clients_plan.status !== 'POSTED') {
              //   return <></>
              // }
              return(
                <div className='col-span-12 md:col-span-6 lg:col-span-12  bg-[#fff] shadow-md rounded-[20px] p-[20px] mb-[15px]'
                key={post.id}>
                  <div className="flex flex-row">
                      <div className="w-[48px] h-[48px] rounded-full bg-[#643DCE]">
                      {post.clients_plan.client.img_url != null && post.clients_plan.client.img_url.length > 0 ?
                          
                          <Image 
                            className='rounded-full'
                            src={post.clients_plan.client.img_url}
                            alt='media'
                            layout='responsive'
                            height={48}
                            width={48}
                            >

                          </Image> 
                          : <></>  
                        } 
          
                      </div>
                    <div className="pl-[10px]">
        
                      {post.clients_plan.client.name !== null ?
                        <p className='text-[14px] font-semibold text-left leading-2 whitespace-normal '>
                          {post.clients_plan.client.name}
                        </p>
                        : <></>
                      }
                      <p className='text-[12px]'>{monthName}{day}, {year}</p>
        
                    </div>
        
                  </div>
                <div className='mt-[15px] items-center text-center'>
                {post.media_url != null && post.media_url.length > 0 ?
                    
                    <Image
                    className=''
                    src={post.media_url}
                    alt='media'
                    layout='responsive'
                    height={260}
                    width={260}
                    >
                </Image>
                    : <></>  
                } 
                </div>
                {post.post_copy !== null && post.post_copy !== undefined ?
                  <div className='mt-[5px] '>
                    <p className='text-[12px] leading-4 whitespace-normal'>{post.post_copy}</p>
                    
                  </div>
                : <></>
                }
                <div className='col-span-12 md:col-span-12 lg:col-span-12 mt-[15px]'> 

                  <ModalEditLateral
                  id={post.id}
                  title={post.title}
                  subtitle={post.subtitle}
                  post_copy={post.post_copy}
                  social_network={post.social_network}
                  planned_datetime={post.planned_datetime}
                  type={post.type}
                  instructions={post.instructions}
                  />
                      
                </div>
      
              </div>
              )
            }
            )
          }

      </div>
    </div>
    </>
  )
}