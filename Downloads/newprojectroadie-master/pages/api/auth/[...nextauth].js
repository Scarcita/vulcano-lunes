
import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials"

export default NextAuth({
  pages: {
    signIn: "/login"
  },
  providers: [
    CredentialsProvider({
      name: "Credentials",
      // credentials: {
      //   username: { label: "Username", type: "text", placeholder: "jsmith" },
      //   password: { label: "Password", type: "password" }
      // },
      async authorize(credentials, req) {
        if(credentials){
          return { 
              user: {
                _id: credentials.id,
                name: credentials.username,
                //role: credentials.role,
                roles: credentials.role
              }
          }
        } else {
          return null;
        }

      }, 
      
    })
  ],
  callbacks: {
    async jwt({token, user}){
      return {...token, ...user}
    },
    async session({session, user, token}){
      return token
    }
  }
  // session: {
  //   // jwt: true,
  //   strategy: "jwt",
  //   maxAge: 30 * 24 * 60 * 60,
  // },
  // jwt: {
  //   signingKey: process.env.JWT_SIGNING_PRIVATE_KEY,
  // },
  // callbacks: {
  //   // async session({ session, token }) {
  //   //   session.user = token.user;
  //   //   // session.role = 'ROLE';
  //   //   console.log(token);
  //   //   return session;
  //   // },
  //   async session({ session, user, token }) {
  //     return token
  //    },
  //   async jwt({ token, user }) {
  //     if (user) {
  //       token.user = user;
  //     }
  //     return token;
  //   },
  // },
  // callbacks: {
  //   async jwt(token, user ) {
  //     // if (account?.accessToken) {
  //     //   token.accessToken = account.accessToken
  //     // }
  //     if (user?.role) {
  //       token.roles = user.role
  //     }
  //     return token
  //   },
  //   async session(session, token) {
  //     // if(token?.accessToken) {
  //     //   session.accessToken = token.accessToken
  //     // }
  //     if (token?.role) {
  //       session.user.role = token.role
  //       session.user.roles = token.roles
  //     }
  //     return session
  //   }
  // }
  }
)
