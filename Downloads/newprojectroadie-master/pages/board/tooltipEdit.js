import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import ModalEditCalendar from '../calendar/Components/modalEditCalendar';


import ImgVideo from '../../public/Board/video.png'
import ImgImagen from '../../public/Board/image.png'
import ImgFacebook from '../../public/SocialMedia/Facebook.svg'
import ImgTikTok from '../../public/SocialMedia/TikTok.svg'
import ImgIg from '../../public/SocialMedia/Instagram.svg'
import ImgYoutube from '../../public/SocialMedia/Youtube.svg'
import ImgMailing from '../../public/SocialMedia/messenger.svg'
import ImgLinkendIn from '../../public/SocialMedia/messenger.svg'
import ImgTwitter from '../../public/SocialMedia/Twitter.svg'
import ModalEditPost from './ModalEditPost';




export default function TooltipEditItem(props) {
  const {showModal, setShowModal, selectedPost } = props;


  if(selectedPost !== undefined){
    if (selectedPost !== null){
      if(selectedPost.planned_datetime !== null && selectedPost.planned_datetime !== undefined){
        var eventdate = selectedPost.planned_datetime;
        var splitdate = eventdate.split('-');
        //console.log(splitdate);
        var day = splitdate[2];
        var year = splitdate[0];
      }
    }
  }
  if(selectedPost !== undefined){
    if (selectedPost !== null){
      if(selectedPost.planned_datetime !== null && selectedPost.planned_datetime !== undefined){
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var d = new Date(selectedPost.planned_datetime);
        var dayName = days[d.getDay()];
      }
    }
  }

  if(selectedPost !== undefined){
    if (selectedPost !== null){
      if(selectedPost.planned_datetime !== null && selectedPost.planned_datetime !== undefined){
        var months = ['January', 'February ', 'March ', 'April', 'May', 'June', 'July','August', 'September', 'Octuber', 'November', 'December'];
        var m = new Date(selectedPost.planned_datetime);
        var monthName = months[m.getMonth()];
      }
    }
  }
    return (
        <>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[450px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">
                                <div>Posts Day</div>

                                <div className='col-span-12 md:col-span-12 lg:col-span-12  bg-[#fff] shadow-md rounded-[20px] p-[20px] mb-[15px]'
                                    key={selectedPost.id}>
                                    <div className='flex flex-row justify-between'>
                                    <div className='flex flex-row justify-between'>
                                        <div className='mr-[15px]'>
                                            <div className='text-[36px] font-bold text-center self-center'>{day}</div>

                                        </div>
                                        <div>
                                            <p className='text-[14px] text-[#582BE7] text-right leading-4 whitespace-normal'>{dayName}</p>
                                            <p className='text-[16px] font-semibold text-right leading-4 whitespace-normal'>{monthName} {year}</p>
                                            <p className='text-[12px] font-semibold text-right leading-4 whitespace-normal'>7:00 AM</p>

                                        </div>
                                    </div>
                                    <div className="">

                                        {selectedPost.status !== null ?
                                            <div className='text-[12px] font-semibold self-center bg-[#582BE7] rounded-md  pl-[10px] pr-[10px] text-[#fff]'>
                                            {selectedPost.status}
                                            </div>
                                            : <></>
                                        }

                                        <div className='flex flex-row mt-[5px]'>
                                            <div className=''>
                                                {selectedPost.social_network === 'Facebook' ?
                                                    <Image 
                                                        
                                                        src={ImgFacebook}
                                                        alt='media'
                                                        layout='fixed'
                                                        height={40}
                                                        width={40}
                                                        >

                                                    </Image> 
                                                : <></>  
                                                } 
                                                {selectedPost.social_network === 'TikTok' ?
                                                    <Image 
                                                        
                                                        src={ImgTikTok}
                                                        alt='media'
                                                        layout='fixed'
                                                        height={40}
                                                        width={40}
                                                        >

                                                    </Image> 
                                                : <></>  
                                                } 
                                                {selectedPost.social_network === 'Instagram' ?
                                                    <Image 
                                                        
                                                        src={ImgIg}
                                                        alt='media'
                                                        layout='fixed'
                                                        height={40}
                                                        width={40}
                                                        >

                                                    </Image> 
                                                : <></>  
                                                }
                                                {selectedPost.social_network === 'YouTube' ?
                                                    <Image 
                                                        
                                                        src={ImgYoutube}
                                                        alt='media'
                                                        layout='fixed'
                                                        height={40}
                                                        width={40}
                                                        >

                                                    </Image> 
                                                : <></>  
                                                }
                                                {selectedPost.social_network === 'Mailing' ?
                                                    <Image 
                                                        
                                                        src={ImgMailing}
                                                        alt='media'
                                                        layout='fixed'
                                                        height={40}
                                                        width={40}
                                                        >

                                                    </Image> 
                                                : <></>  
                                                }
                                                {selectedPost.social_network === 'LinkedIn' ?
                                                    <Image 
                                                        
                                                        src={ImgLinkendIn}
                                                        alt='media'
                                                        layout='fixed'
                                                        height={40}
                                                        width={40}
                                                        >

                                                    </Image> 
                                                : <></>  
                                                }
                                                {selectedPost.social_network === 'Twitter' ?
                                                    <Image 
                                                        
                                                        src={ImgTwitter}
                                                        alt='media'
                                                        layout='fixed'
                                                        height={40}
                                                        width={40}
                                                        >

                                                    </Image> 
                                                : <></>  
                                                }
                                            
                                            </div>

                                            <div className=''>
                                                {selectedPost.type === 'Video' ?
                                                    <Image 
                                                        
                                                        src={ImgVideo}
                                                        alt='media'
                                                        layout='fixed'
                                                        height={40}
                                                        width={40}
                                                        >

                                                    </Image> 
                                                : <></>  
                                                } 
                                                {selectedPost.type === 'Image' ?
                                                    <Image 
                                                        
                                                        src={ImgImagen}
                                                        alt='media'
                                                        layout='fixed'
                                                        height={40}
                                                        width={40}
                                                        >

                                                    </Image> 
                                                : <></>  
                                                } 
                                                {selectedPost.type === 'Album' ?
                                                    <Image 
                                                        
                                                        src={ImgImagen}
                                                        alt='media'
                                                        layout='fixed'
                                                        height={40}
                                                        width={40}
                                                        >

                                                    </Image> 
                                                : <></>  
                                                }
                                                {selectedPost.type === 'Story' ?
                                                    <Image 
                                                        
                                                        src={ImgVideo}
                                                        alt='media'
                                                        layout='fixed'
                                                        height={40}
                                                        width={40}
                                                        >

                                                    </Image> 
                                                : <></>  
                                                }
                                                {selectedPost.type === 'Reel' ?
                                                    <Image 
                                                        
                                                        src={ImgVideo}
                                                        alt='media'
                                                        layout='fixed'
                                                        height={40}
                                                        width={40}
                                                        >

                                                    </Image> 
                                                : <></>  
                                                }
                                            
                                            </div>
                                        </div>
                            
                                    </div>
                                    </div>
                                    <div className='mt-[15px] items-center text-center'>
                                        {selectedPost.media_url != null && selectedPost.media_url.length > 0 ?
                        
                                                <Image
                                                className=''
                                                src={selectedPost.media_url}
                                                alt='media'
                                                layout='responsive'
                                                height={260}
                                                width={260}
                                                >
                                            </Image>
                                                : <></>  
                                            }
                                    </div>

                                    <div className='mt-[10px]'>
                                        {selectedPost.post_copy !== null && selectedPost.post_copy !== undefined ?
                                            <div className='mb-[5px]'>
                                                <p className='text-[10px] font-light leading-4 whitespace-normal'>{selectedPost.post_copy}</p>
                                                
                                            </div>
                                        : <></>
                                        }
                                        <hr></hr>
                                        {selectedPost.title !== null ?
                                            <div className='mt-[5px]'>
                                                <p className='text-[12px] font-semibold text-left leading-2 whitespace-normal text-ellipsis ...'>
                                                    {selectedPost.title}
                                                </p>
                                            </div>
                                        : <></>
                                        }
                                        {selectedPost.suttitle !== null ?
                                            <p className='text-[10px] text-left leading-2 whitespace-normal text-ellipsis ...'>
                                            {selectedPost.subtitle}
                                            </p>
                                            : <></>
                                        }
                                        {selectedPost.instructions !== null && selectedPost.instructions !== undefined ?
                                        <div className='mt-[5px] mb-[5px]'>
                                            <div className='font-semibold text-[12px] text-[#582BE7]'>Instructions: </div>
                                            <p className='text-[12px] font-light leading-4 whitespace-normal'>{selectedPost.instructions}</p>
                                        </div>
                                        : <></>
                                        }
                                    </div>

                                    
                                    <div className='col-span-12 md:col-span-12 lg:col-span-12 mt-[15px]'> 
                                    <ModalEditPost
                                    id={selectedPost.id}
                                    title={selectedPost.title}
                                    subtitle={selectedPost.subtitle}
                                    post_copy={selectedPost.post_copy}
                                    social_network={selectedPost.social_network}
                                    planned_datetime={selectedPost.planned_datetime}
                                    type={selectedPost.type}
                                    instructions={selectedPost.instructions}
                                    />
                                        
                                    </div>
                        
                                </div>
                                <div className='text-end'>
                                    <button
                                        className="w-[100px] h-[35px] border-[1px] border-[#582BE7] rounded-[30px] text-[#582BE7] hover:bg-[#582BE7] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                        onClick={() =>
                                            setShowModal(false)
                                        }
                                    >
                                        Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}
