import React, { useEffect, useState } from 'react';
import { Spinner} from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";
import Layout from "../components/layout"
import Kanban from '../../boardComponents/Kanban';
import SelectClient from './selectClients';
import { resetServerContext } from "react-beautiful-dnd";
import { v4 as uuidv4 } from 'uuid';


export default function Board() {
  const [columns, setColumns] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  
  const getPostsByClientPlan = (client_plan_id) => {

    let todo_tasks = [];
    let progress_tasks = [];
    let done_tasks = [];

    fetch('https://slogan.com.bo/roadie/clientsPlansPosts/all/' + client_plan_id)
    .then(response => response.json())
    .then( data => {
        if(data.status){
          data.data.map((item, index) => {
            switch (item.status) {
              case "PLANNED":
                todo_tasks.push(item)
                break;
              case "DONE":
                progress_tasks.push(item)
                break;
              case "POSTED":
                done_tasks.push(item)
                break;        
              default:
                break;
            }

          })

          setColumns(
              {[uuidv4()]: {
                id: 1,
                title: "PLANNED",
                items: todo_tasks,
              },
              [uuidv4()]: {
                id: 2,
                title: "DONE",
                items: progress_tasks,
              },
              [uuidv4()]: {
                id:3,
                title: "POSTED",
                items: done_tasks,
              }
            }
          )
        }
        else {
          console.error(data.error)
      }
      }
    )
  }

  useEffect(() => {
    fetch('https://slogan.com.bo/roadie/clientsPlansPosts/all/')
        .then(response => response.json())
        .then(data => {
            if (data.status) {
                setData(data.data)
            } else {
                console.error(data.error)
            }
            setIsLoading(false)
        })

}, [])
  return (
      <div className='grid grid-cols-12 ml-[20px] mt-[20px] mr-[20px]'>

        <div className='col-span-12 md:col-span-6 lg:col-span-7'>
            <div className='text-[24px] md:text-[32px] lg:text-[37px] text-[#000000] font-bold'>Tracking Board</div>
        </div>

        <div className='col-span-12 md:col-span-6 lg:col-span-5'>
            <SelectClient getPostsByClientPlan={getPostsByClientPlan} />
        </div>

        <div className='col-span-12 md:col-span-12 lg:col-span-12 '>
          <Kanban
          columns={columns} 
          setColumns={setColumns}/>
        </div>
      </div>
  )
}
Board.getLayout = function getLayout(page){
  return (
    <Layout>{page}</Layout>
  )
}

export async function getServerSideProps(context) {
  resetServerContext()
  return {
    props: {},
  }
}